// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ActionRPG_MY2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ACTIONRPG_MY2_API AActionRPG_MY2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
