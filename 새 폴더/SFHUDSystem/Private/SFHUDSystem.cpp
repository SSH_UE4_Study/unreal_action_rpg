﻿#include "SFHUDSystem.h"

DEFINE_LOG_CATEGORY(LogSFHUDSystem);

#define LOCTEXT_NAMESPACE "FSFHUDSystem"

void FSFHUDSystem::StartupModule()
{
	SF_LOG(Warning, TEXT("SFHUDSystem module has been loaded"));
}

void FSFHUDSystem::ShutdownModule()
{
	SF_LOG(Warning, TEXT("SFHUDSystem module has been unloaded"));
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFHUDSystem, SFHUDSystem)
