﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SFMainHUD.h"


ASFMainHUD::ASFMainHUD()
{
	m_bGamepad = false;
}

void ASFMainHUD::OnChangeCharacter()
{
	//UE_LOG(LogTemp, Display, TEXT("ASFHUD::OnChangeCharacter"));
	if (m_OnChangeCharacter.IsBound())
	{
		m_OnChangeCharacter.Broadcast();
	}
}

void ASFMainHUD::OnChangeSkillCooldown()
{
	//UE_LOG(LogTemp, Display, TEXT("ASFHUD::OnChangeCooldown"));
	if (m_OnChangeSkillCooldown.IsBound())
	{
		m_OnChangeSkillCooldown.Broadcast();
	}
}

void ASFMainHUD::OnDisableChanged(bool bDisable)
{
	//UE_LOG(LogTemp, Display, TEXT("ASFHUD::OnDisableChanged"));
	if (m_OnChangeDisable.IsBound())
	{
		m_OnChangeDisable.Broadcast(bDisable);
	}
}

void ASFMainHUD::OnActionInputChanged(FGameplayTag InputGameTag, bool bStart, bool bSwim)
{
	//UE_LOG(LogTemp, Display, TEXT("ASFHUD::OnActionInputChanged"));
	if (m_OnActionInputChanged.IsBound())
	{
		m_OnActionInputChanged.Broadcast(InputGameTag, bStart, bSwim);
	}
}

void ASFMainHUD::OnRidingChanged(bool bRiding)
{
	if (m_OnRideChanged.IsBound())
	{
		m_OnRideChanged.Broadcast(bRiding);
	}
}

void ASFMainHUD::OnInputDeviceChanged_HUD(bool bGamePad)
{
	m_bGamepad = bGamePad;

	if (m_OnchangeInputDevice_HUD.IsBound())
	{
		m_OnchangeInputDevice_HUD.Broadcast(bGamePad);
	}
}

void ASFMainHUD::OnInputDeviceChanged(bool bGamePad)
{
	//UE_LOG(LogTemp, Display, TEXT("ASFHUD::OnInputDeviceChanged"));
	if (m_OnchangeInputDevice.IsBound())
	{
		m_OnchangeInputDevice.Broadcast(bGamePad);
	}
}

void ASFMainHUD::OnRunnersSpawnStateChanged(bool bPossible)
{
	if (m_OnSpawnPossible.IsBound())
	{
		m_OnSpawnPossible.Broadcast(bPossible);
	}
}

void ASFMainHUD::OnChangeStamina(float fCurrentStamina, float fMaxStamina)
{
	//UE_LOG(LogTemp, Display, TEXT("ASFHUD::OnChangeStamina"));
	if (m_OnChangeStamina.IsBound())
	{
		m_OnChangeStamina.Broadcast(fCurrentStamina, fMaxStamina);
	}
}

void ASFMainHUD::OnInputKey(const FKey InputKey, const EInputEvent InputEvent)
{
	if (m_OnInputKey.IsBound())
	{
		m_OnInputKey.Broadcast(InputKey, InputEvent);
	}

	KeyInputProcess(InputKey, InputEvent);
}

float ASFMainHUD::GetCameraDistancePawn()
{
	return GetOwningPlayerController()->PlayerCameraManager->GetDistanceTo(GetOwningPawn());
}

float ASFMainHUD::GetCameraDistanceTarget(FVector TargetLocation)
{
	return (GetOwningPlayerController()->PlayerCameraManager->GetCameraLocation() - TargetLocation).Size();
}