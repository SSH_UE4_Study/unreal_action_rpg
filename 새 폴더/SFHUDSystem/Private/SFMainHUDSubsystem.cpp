﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SFMainHUDSubsystem.h"

#include "SFTicket.h"
#include "SFTicketExecutor.h"
#include "SFTicketGenerator.h"
#include "SFTicketProcessor.h"

#include "SFMainHUD.h"

#include "Components/PanelWidget.h"

void USFMainHUDSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	UE_LOG(LogTemp, Warning, TEXT("SFC-009 SFMainHUD Init"));
}

void USFMainHUDSubsystem::Deinitialize()
{
	UE_LOG(LogTemp, Warning, TEXT("SFC-009 SFMainHUD Deinit"));
}

void USFMainHUDSubsystem::SetMainHUD(ASFMainHUD* MainHUD)
{
	_MainHUD = MainHUD;
}

ASFMainHUD* USFMainHUDSubsystem::GetMainHUD()
{
	return _MainHUD;
}

void USFMainHUDSubsystem::AddWidget(TSubclassOf<UUserWidget> NewWidgetClass, EUILayerType eType, UUserWidget*& ResultWidget)
{
	if (_MainHUD.IsNull())
	{
		return;
	}

	_MainHUD->AddWidget(NewWidgetClass, eType, ResultWidget);
}

void USFMainHUDSubsystem::AddBattleText(FVector vLocation, int nOwnerType, int nDamageType, int nAdvType, bool bCritcal, FString strDamage)
{
	if (_MainHUD.IsNull())
	{
		return;
	}

	_MainHUD->AddBattleText(vLocation, nOwnerType, nDamageType, nAdvType, bCritcal, strDamage);
}