﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "SFMainHUD.h"

#include "SFTicketExecutor.h"
#include "SFMainHUDSubsystem.generated.h"

/**
 * 
 */
UCLASS(DisplayName = "SFC-009 SFMainHUD")
class SFHUDSYSTEM_API USFMainHUDSubsystem 
	: public UGameInstanceSubsystem
	, public ISFTicketExecutor
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	FName ExecutorName;

	UPROPERTY()
	TSet<FName>	_processableTicketNames;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDele_OnSFHUD_ADDWidget, const EUILayerType, eType, UClass*, WidgetClass);

	UPROPERTY(BlueprintAssignable, Category = "SFC-009 SFMainHUD")
	FDele_OnSFHUD_ADDWidget Func_OnSFHUD_ADDWidget;

	UPROPERTY()
	TObjectPtr<ASFMainHUD> _MainHUD;

public:

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	
	UFUNCTION(BlueprintCallable, Category = "SFC-009 SFMainHUD")
	void SetMainHUD(ASFMainHUD* MainHUD);

	UFUNCTION(BlueprintCallable, Category = "SFC-009 SFMainHUD")
	ASFMainHUD* GetMainHUD();

	UFUNCTION(BlueprintCallable, Category = "SFC-009 SFMainHUD")
	void AddWidget(TSubclassOf<UUserWidget> NewWidgetClass, EUILayerType eType, UUserWidget*& ResultWidget);

	UFUNCTION(BlueprintCallable, Category = "SFC-009 SFMainHUD")
	void AddBattleText(FVector vLocation, int nOwnerType, int nDamageType, int nAdvType, bool bCritcal, FString strDamage = "0");
};
