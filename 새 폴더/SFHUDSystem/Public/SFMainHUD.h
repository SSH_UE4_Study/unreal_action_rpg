﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "GameplayTagContainer.h"
#include "SFMainHUD.generated.h"

UENUM(BlueprintType, meta = (DisplayName = "UILayerType"))
enum class EUILayerType : uint8
{
	E_None = 0 UMETA(DisplayName = "None"),
	E_MainLayer UMETA(DisplayName = "MainLayer"),
	E_ControlLayer UMETA(DisplayName = "ControlLayer"),
	E_BattleInfoLayer UMETA(DisplayName = "BattleInfoLayer"),
	E_OverlayLayer UMETA(DisplayName = "OverlayLayer"),
	E_Max UMETA(Hidden),
};

DECLARE_MULTICAST_DELEGATE(FSFBaseWidgetDelegate);
DECLARE_MULTICAST_DELEGATE_OneParam(FSFMultiDelegateOne, bool);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeStamina, float, fCurrentStamina, float, fMaxStamina);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeInputDevice, bool, bGamepad);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeDisable, bool, bDisable);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnChangeAction, FGameplayTag, stNameTag, bool, bStart, bool, bSwim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangeSkillCooldown);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnInputKey, FKey, InputKey, EInputEvent, InputEvent);

/**
 * 
 */
UCLASS()
class SFHUDSYSTEM_API ASFMainHUD : public AHUD
{
	GENERATED_BODY()

public:
	ASFMainHUD();

	FSFBaseWidgetDelegate m_OnChangeCharacter;
	FOnChangeSkillCooldown m_OnChangeSkillCooldown;

	//FOnChangeAction m_OnActionInputChanged_HUD;
	FOnChangeAction m_OnActionInputChanged;
	FSFMultiDelegateOne m_OnRideChanged;
	FOnChangeDisable m_OnChangeDisable;
	FOnChangeStamina m_OnChangeStamina;
	FOnChangeInputDevice m_OnchangeInputDevice_HUD;
	FOnChangeInputDevice m_OnchangeInputDevice;
	FOnInputKey m_OnInputKey;

	FSFMultiDelegateOne m_OnSpawnPossible;

	UFUNCTION(BlueprintCallable)
	void OnChangeCharacter();

	UFUNCTION(BlueprintCallable)
	void OnChangeSkillCooldown();

	UFUNCTION(BlueprintCallable)
	void OnDisableChanged(bool bDisable);

	UFUNCTION(BlueprintCallable)
	void OnActionInputChanged(FGameplayTag InputGameTag, bool bStart, bool bSwim);

	UFUNCTION(BlueprintCallable)
	void OnRidingChanged(bool bRiding);

	UFUNCTION(BlueprintCallable)
	void OnInputDeviceChanged_HUD(bool bGamePad);

	UFUNCTION(BlueprintCallable)
	void OnInputDeviceChanged(bool bGamePad);

	UFUNCTION(BlueprintCallable)
	void OnRunnersSpawnStateChanged(bool bPossible);

	UFUNCTION(BlueprintCallable)
	void OnChangeStamina(float fCurrentStamina, float fMaxStamina);

	UFUNCTION(BlueprintCallable)
	void OnInputKey(const FKey InputKey, const EInputEvent InputEvent);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	FVector GetPlayerCharacterSocketLocation(const FName SocketName);

	UFUNCTION(BlueprintCallable)
	float GetCameraDistancePawn();

	UFUNCTION(BlueprintCallable)
	float GetCameraDistanceTarget(FVector TargetLocation);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Initialize")
	void InitializeLayer();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Initialize")
	void InitializeHUD();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Initialize")
	void InitializeStaminaUI();

 	UFUNCTION(BlueprintImplementableEvent)
 	void KeyInputProcess(const FKey InputKey, const EInputEvent InputEvent);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void AddWidget(TSubclassOf<UUserWidget> NewWidgetClass, EUILayerType eType, UUserWidget*& ResultWidget);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void AddBattleText(FVector vLocation, int nOwnerType, int nDamageType, int nAdvType, bool bCritcal, const FString& strDamage);

	UPROPERTY(BlueprintReadOnly, meta = (DisplayName = "Is Gamepad"))
	bool m_bGamepad;

	
};
