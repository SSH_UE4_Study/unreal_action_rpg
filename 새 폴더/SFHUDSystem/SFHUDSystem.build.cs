﻿using UnrealBuildTool;
 
public class SFHUDSystem : ModuleRules
{
	public SFHUDSystem(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"
			, "GameplayTags"
			, "SFCTicketSystem"
			, "InputCore"
			, "GameplayTags"
			, "UMG"});
 
		PublicIncludePaths.AddRange(new string[] {"SFHUDSystem/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFHUDSystem/Private"});
	}
}
