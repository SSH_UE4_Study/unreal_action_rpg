#include "SFPartySystem.h"

DEFINE_LOG_CATEGORY(SFPartySystem);

#define LOCTEXT_NAMESPACE "FSFPartySystem"

void FSFPartySystem::StartupModule()
{
	UE_LOG(SFPartySystem, Warning, TEXT("SFPartySystem module has been loaded"));
}

void FSFPartySystem::ShutdownModule()
{
	UE_LOG(SFPartySystem, Warning, TEXT("SFPartySystem module has been unloaded"));
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFPartySystem, SFPartySystem)