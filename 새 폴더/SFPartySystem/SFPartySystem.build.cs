using UnrealBuildTool;
 
public class SFPartySystem : ModuleRules
{
	public SFPartySystem(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"});
 
		PublicIncludePaths.AddRange(new string[] {"SFPartySystem/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFPartySystem/Private"});
	}
}