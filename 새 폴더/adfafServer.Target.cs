// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class ProjectSFServerTarget : TargetRules
{
	public ProjectSFServerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Server;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "ProjectSF" } );
        ExtraModuleNames.AddRange(new string[] { "SFPartySystem" });
        ExtraModuleNames.AddRange(new string[] { "SFSpawnableSystem" });
        ExtraModuleNames.AddRange(new string[] { "SFEnvironmentSystem" });
        ExtraModuleNames.AddRange(new string[] { "SFBattleSystem" });
        ExtraModuleNames.AddRange(new string[] { "SFCameraSystem" });
        ExtraModuleNames.AddRange(new string[] { "SFInputSystem" });
        ExtraModuleNames.AddRange(new string[] { "SFCCharacterSystem" });
        ExtraModuleNames.AddRange(new string[] { "SFCTicketSystem" });
        ExtraModuleNames.AddRange(new string[] { "SFHUDSystem" });
    }
}
