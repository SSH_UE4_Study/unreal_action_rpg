﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SFCameraSystemFunction.h"

// --------------------------------------------------------------------------------------------------------------
// Add Header List
#include "Math/Matrix.h"

// --------------------------------------------------------------------------------------------------------------

FMatrix USFCameraSystemFunction::CreateViewMatrix(const FVector& EyeLocation, const FVector& LookAtLocation, FVector UpVector)
{
	return FLookAtMatrix(EyeLocation, LookAtLocation, UpVector);
}

void USFCameraSystemFunction::GetDirectionVector(const FMatrix& Matrix, FVector& Forward, FVector& Right, FVector& Up)
{
	Forward = Matrix.GetUnitAxis(EAxis::X);
	Right = Matrix.GetUnitAxis(EAxis::Y);
	Up = Matrix.GetUnitAxis(EAxis::Z);
}

FVector2D USFCameraSystemFunction::GetWorldToNDC(const FMatrix& ViewProjectionMatrix, const FVector& TargetLocation)
{
	FVector4 Location = FVector4(TargetLocation, 1);
	Location = ViewProjectionMatrix.TransformFVector4(Location);

	return FVector2D(Location.X / Location.W, Location.Y / Location.W);
}

FVector2D USFCameraSystemFunction::GetWorldToScreenLocation(const FMatrix& ViewProjectionMatrix, const FVector& TargetLocation, const FVector2D& ViewPortSize)
{
	FVector2D NDC = GetWorldToNDC(ViewProjectionMatrix, TargetLocation);

	return NDC * (ViewPortSize * 0.5);
}

FVector2D USFCameraSystemFunction::GetWorldToUnitNDC(const FMatrix& ViewProjectionMatrix, const FVector& TargetLocation)
{
	return ConvertNDCToUnitNDC(GetWorldToNDC(ViewProjectionMatrix, TargetLocation));
}

FVector2D USFCameraSystemFunction::ConvertNDCToUnitNDC(const FVector2D& NDC)
{
	FVector2D Result = (NDC + 1.0) / 2.0;
	Result.Y = 1.0 - Result.Y;

	return Result;
}
