﻿#include "SFCameraSubsystem.h"

#include "Subsystems/SubsystemCollection.h"

#include "SFTicket.h"
#include "SFTicketExecutor.h"
#include "SFMainCamera.h"


void USFCCameraSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	UE_LOG(LogTemp, Warning, TEXT("SFC-002 CameraSystem Init"));

	// 할당용 handle 초기화
	_assigend_handle = 0;
}

void USFCCameraSubsystem::Deinitialize()
{
	UE_LOG(LogTemp, Warning, TEXT("SFC-002 CameraSystem Deinit"));
}


void USFCCameraSubsystem::SetExecutorName_Implementation(FName name)
{
	//ExecutorName = name;
}

void USFCCameraSubsystem::GetExecutorName_Implementation(FName& name)
{
	name = NAME_CAMERASYSTEM;
}

void USFCCameraSubsystem::RegisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Emplace(_elem);
	}
}

void USFCCameraSubsystem::UnregisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Remove(_elem);
	}
}

int USFCCameraSubsystem::GetRegisteredTickets_Implementation(TArray<FName>& registerEvents)
{
	registerEvents = _processableTicketNames.Array();
	return registerEvents.Num();
}

void USFCCameraSubsystem::ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket)
{
	ensureMsgf(iTicket, TEXT("Ticket is null or none"));

	int cam_uid = ISFTicket::Execute_GetOwnerUID(iTicket.GetObject());

	if (Func_OnExecuteSFTicketMng.IsBound())
		Func_OnExecuteSFTicketMng.Broadcast(iTicket, cam_uid);

	/// Cam ID가 0 이하면, 전역 카메라(메인 카메라)로 인지
	if (cam_uid <= 0 && m_ISFMainCamera)
	{
		if (m_ISFMainCamera.GetObject()->GetClass()->ImplementsInterface(USFTicketExecutor::StaticClass()))
		{
			// 실행기가 있다면.
			ISFTicketExecutor::Execute_ExecuteTicket(m_ISFMainCamera.GetObject(), iTicket);
		}
	}
	else
	{
		FName* pAliasName = m_camHandleNamePairMap.Find(cam_uid);

		if (pAliasName) {
			auto* pISubCamera = m_SubCameras.Find(*pAliasName);

			if (pISubCamera && *pISubCamera
				&& pISubCamera->GetObject()->GetClass()->ImplementsInterface(USFTicketExecutor::StaticClass()))
			{
				ISFTicketExecutor::Execute_ExecuteTicket(pISubCamera->GetObject(), iTicket);
			}
		}
	}
}




void USFCCameraSubsystem::SetSFMainCameraInterface(const TScriptInterface<ISFMainCamera>& iSFMainCamera)
{
	m_ISFMainCamera = iSFMainCamera;

	ISFMainCamera::Execute_SetHandle(iSFMainCamera.GetObject(), 0);
	ISFMainCamera::Execute_SetAliasName(iSFMainCamera.GetObject(), NAME_MAINCAMERA);
}


bool USFCCameraSubsystem::GetSFMainCameraInterface(TScriptInterface<ISFMainCamera>& iSFMainCamera)
{
	iSFMainCamera = m_ISFMainCamera;
	return true;
}


void USFCCameraSubsystem::AddSubCameraInterface(FName camAliasName, const TScriptInterface<ISFMainCamera>& iSFCamera, int& AssignedHandle)
{
	// 핸들 할당	
	AssignedHandle =  ( _assigend_handle + 1 >= INT_MAX ) ? 0 : ++_assigend_handle;

	ISFMainCamera::Execute_SetHandle(iSFCamera.GetObject(), AssignedHandle);
	ISFMainCamera::Execute_SetAliasName(iSFCamera.GetObject(), camAliasName);

	m_SubCameras.Add(camAliasName, iSFCamera);
}


bool USFCCameraSubsystem::GetSubCameraInterface(FName camAliasName, TScriptInterface<ISFMainCamera>& iSFCamera)
{
	auto* pIt = m_SubCameras.Find(camAliasName);
	if (pIt) {
		iSFCamera = *pIt;
		return true;
	}

	return false;
}

void USFCCameraSubsystem::RemoveSubCameraInterface(FName camAliasName)
{
	m_SubCameras.Remove(camAliasName);
}



void USFCCameraSubsystem::Tick(float DeltaTime)
{
	if (GetWorld() && GetWorld()->IsGameWorld())
	{
		if (LastFrameNumberWeTicked == GFrameCounter)
			return;
		LastFrameNumberWeTicked = GFrameCounter;

		Update(DeltaTime);
	}
}




void USFCCameraSubsystem::Update(float dt)
{

}
