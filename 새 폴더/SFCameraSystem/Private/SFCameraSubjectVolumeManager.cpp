﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SFCameraSubjectVolumeManager.h"

// Add default functionality here for any ISFCameraSubjectVolumeManager functions that are not pure virtual.

void ISFCameraSubjectVolumeManager::SetSFCamera_Implementation(const TScriptInterface<ISFMainCamera>& iSFCamera)
{
	//SetSFCameraObject(iSFCamera.GetObject());
}
bool ISFCameraSubjectVolumeManager::GetSFCamera_Implementation(TScriptInterface<ISFMainCamera>& iSFCamera)
{
	//if (GetSFCameraObject() != nullptr) {
	//	iSFCamera = GetSFCameraObject();
	//	return true;
	//}
	return false;
}

void ISFCameraSubjectVolumeManager::AddSubjectVolumeAsset_Implementation(const USFSubjectVolumeAsset* volumeAsset)
{
	if (::IsValid(volumeAsset))
	{
		SubjectVolumeAssets.Add(volumeAsset->Key, volumeAsset->Data);
	}
}
bool ISFCameraSubjectVolumeManager::GetSubjectVolumeAsset_Implementation(FName Key, FSFSubjectVolume& subjectVolume)
{
	auto* pVolume = SubjectVolumeAssets.Find(Key);
	if (pVolume)
	{
		subjectVolume = *pVolume;
		return true;
	}

	return false;
}
void ISFCameraSubjectVolumeManager::RemoveSubjectVolumeAsset_Implementation(FName Key)
{
	SubjectVolumeAssets.Remove(Key);
}

UObject* ISFCameraSubjectVolumeManager::SpawnSubjectVolume_Implementation(UClass* Class, FName Key, bool bUseCustomLocation, FVector Location)
{
	return nullptr;
	/*UWorld* World = GetWorld();

	if (IsValid(World) == false)
		return nullptr;

	FSFSubjectVolume volumeAsset;

	if (Execute_GetSubjectVolumeAsset(this, Key, volumeAsset) == false)
		return nullptr;

	if (bUseCustomLocation == true)
		volumeAsset.Transform.SetLocation(Location);

	AActor* Result = World->SpawnActor(Class);
	Result->SetActorTransform(volumeAsset.Transform);

	if (IsValid(Result) == true)
	{
		return Result;
	}
	else
	{
		return nullptr;
	}*/
}
