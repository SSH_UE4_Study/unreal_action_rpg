﻿#include "SFCameraSystem.h"

DEFINE_LOG_CATEGORY(LogSFCameraSystem);

#define LOCTEXT_NAMESPACE "FSFCameraSystem"

void FSFCameraSystem::StartupModule()
{
	SF_LOG(Warning, TEXT("SFCameraSystem module has been loaded"));
}

void FSFCameraSystem::ShutdownModule()
{
	SF_LOG(Warning, TEXT("SFCameraSystem module has been unloaded"));
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFCameraSystem, SFCameraSystem)
