﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SFCameraSubjectVolume.h"

#include "Engine/Engine.h"
#include "Engine/GameEngine.h"
#include "Engine/GameInstance.h"
#include "Engine/World.h"

#include "Kismet/KismetMathLibrary.h"

// Add default functionality here for any ISFCameraSubjectVolume functions that are not pure virtual.

FTransform ISFCameraSubjectVolume::GetSubjectActorTransform_Implementation()
{
	return FTransform();
}

float ISFCameraSubjectVolume::GetPlanArea_Implementation(ESubjectVertex TargetVertex)
{
	FVector DirectionTemp, FUL_Location, FUR_Location, FDR_Location, BUR_Location;
	GetVertexInfo_Implementation(ESubjectVertex::FUL, DirectionTemp, FUL_Location);
	GetVertexInfo_Implementation(ESubjectVertex::FUR, DirectionTemp, FUR_Location);
	GetVertexInfo_Implementation(ESubjectVertex::FDR, DirectionTemp, FDR_Location);
	GetVertexInfo_Implementation(ESubjectVertex::BUR, DirectionTemp, BUR_Location);

	switch (TargetVertex)
	{
	case ESubjectVertex::Right:
	case ESubjectVertex::Left:
		return abs(FUR_Location.Z - FDR_Location.Z) * abs(FUR_Location.X - BUR_Location.X);

	case ESubjectVertex::Forward:
	case ESubjectVertex::Backward:
		return abs(FUL_Location.Y - FUR_Location.Y) * abs(FUR_Location.Z - FDR_Location.Z);

	case ESubjectVertex::Up:
	case ESubjectVertex::Down:
		return abs(FUL_Location.Y - FUR_Location.Y) * abs(FUR_Location.X - BUR_Location.X);
	}

	return 0.0f;
}


// ----------------------------------------------------------------------------------------------------------------------------------------------------

FTransform ASFCameraSubjectVolumeActor::GetSubjectActorTransform_Implementation()
{
	return this->GetActorTransform();
}

void ASFCameraSubjectVolumeActor::SetSubjectActorLocation_Implementation(FVector Location)
{
	this->SetActorLocation(Location);
}

void ASFCameraSubjectVolumeActor::SetSubjectActorRotation_Implementation(FRotator Rotator)
{
	this->SetActorRotation(Rotator);
}

void ASFCameraSubjectVolumeActor::SetSubjectActorScale_Implementation(FVector Scale)
{
	this->SetActorScale3D(Scale);
}


void ASFCameraSubjectVolumeActor::GetVertexInfo_Implementation(ESubjectVertex TargetVertex, FVector& Directional, FVector& Location)
{
	FVector ActorLocation = GetActorLocation();
	FVector ActorScale = GetActorScale3D() * 0.5f;

	FVector Forward = (GetActorForwardVector() * ActorScale.X);
	FVector Up = (GetActorUpVector() * ActorScale.Z);
	FVector Right = (GetActorRightVector() * ActorScale.Y);
	FVector Backward = Forward * -1.0f;
	FVector Down = Up * -1.0f;
	FVector Left = Right * -1.0f;

	float FUSize = FVector2D(ActorScale.X, ActorScale.Z).Length();
	float FRSize = FVector2D(ActorScale.X, ActorScale.Y).Length();
	float URSize = FVector2D(ActorScale.Y, ActorScale.Z).Length();

	float DiagonalSize = ActorScale.Length();

	switch (TargetVertex)
	{
		// One Param
	case ESubjectVertex::Center:
	{
		Directional = FVector::Zero();
		Location = ActorLocation;
		return;
	}
	case ESubjectVertex::Left:
	{
		Directional = UKismetMathLibrary::Normal(Left);
		Location = Directional * ActorScale.Y + ActorLocation;
		return;
	}
	case ESubjectVertex::Right:
	{
		Directional = UKismetMathLibrary::Normal(Right);
		Location = Directional * ActorScale.Y + ActorLocation;
		return;
	}
	case ESubjectVertex::Up:
	{
		Directional = UKismetMathLibrary::Normal(Up);
		Location = Directional * ActorScale.Z + ActorLocation;
		return;
	}
	case ESubjectVertex::Down:
	{
		Directional = UKismetMathLibrary::Normal(Down);
		Location = Directional * ActorScale.Z + ActorLocation;
		return;
	}
	case ESubjectVertex::Backward:
	{
		Directional = UKismetMathLibrary::Normal(Backward);
		Location = Directional * ActorScale.X + ActorLocation;
		return;
	}
	case ESubjectVertex::Forward:
	{
		Directional = UKismetMathLibrary::Normal(Forward);
		Location = Directional * ActorScale.X + ActorLocation;
		return;
	}

	// Two Vertex
	case ESubjectVertex::FU:
	{
		Directional = UKismetMathLibrary::Normal(Forward + Up);
		Location = Directional * FUSize + ActorLocation;
		return;
	}
	case ESubjectVertex::FD:
	{
		Directional = UKismetMathLibrary::Normal(Forward + Down);
		Location = Directional * FUSize + ActorLocation;
		return;
	}
	case ESubjectVertex::FL:
	{
		Directional = UKismetMathLibrary::Normal(Forward + Left);
		Location = Directional * FRSize + ActorLocation;
		return;
	}
	case ESubjectVertex::FR:
	{
		Directional = UKismetMathLibrary::Normal(Forward + Right);
		Location = Directional * FRSize + ActorLocation;
		return;
	}
	case ESubjectVertex::LU:
	{
		Directional = UKismetMathLibrary::Normal(Left + Up);
		Location = Directional * URSize + ActorLocation;
		return;
	}
	case ESubjectVertex::LD:
	{
		Directional = UKismetMathLibrary::Normal(Left + Down);
		Location = Directional * URSize + ActorLocation;
		return;
	}
	case ESubjectVertex::RU:
	{
		Directional = UKismetMathLibrary::Normal(Right + Up);
		Location = Directional * URSize + ActorLocation;
		return;
	}
	case ESubjectVertex::RD:
	{
		Directional = UKismetMathLibrary::Normal(Right + Down);
		Location = Directional * URSize + ActorLocation;
		return;
	}
	case ESubjectVertex::BU:
	{
		Directional = UKismetMathLibrary::Normal(Backward + Up);
		Location = Directional * FUSize + ActorLocation;
		return;
	}
	case ESubjectVertex::BD:
	{
		Directional = UKismetMathLibrary::Normal(Backward + Down);
		Location = Directional * FUSize + ActorLocation;
		return;
	}
	case ESubjectVertex::BL:
	{
		Directional = UKismetMathLibrary::Normal(Backward + Left);
		Location = Directional * FRSize + ActorLocation;
		return;
	}
	case ESubjectVertex::BR:
	{
		Directional = UKismetMathLibrary::Normal(Backward + Right);
		Location = Directional * FRSize + ActorLocation;
		return;
	}

	// Three Vertex
	case ESubjectVertex::FUL:
	{
		Directional = UKismetMathLibrary::Normal(Forward + Up + Left);
		Location = Directional * DiagonalSize + ActorLocation;
		return;
	}
	case ESubjectVertex::FUR:
	{
		Directional = UKismetMathLibrary::Normal(Forward + Up + Right);
		Location = Directional * DiagonalSize + ActorLocation;
		return;
	}
	case ESubjectVertex::FDL:
	{
		Directional = UKismetMathLibrary::Normal(Forward + Down + Left);
		Location = Directional * DiagonalSize + ActorLocation;
		return;
	}
	case ESubjectVertex::FDR:
	{
		Directional = UKismetMathLibrary::Normal(Forward + Down + Right);
		Location = Directional * DiagonalSize + ActorLocation;
		return;
	}
	case ESubjectVertex::BUL:
	{
		Directional = UKismetMathLibrary::Normal(Backward + Up + Left);
		Location = Directional * DiagonalSize + ActorLocation;
		return;
	}
	case ESubjectVertex::BUR:
	{
		Directional = UKismetMathLibrary::Normal(Backward + Up + Right);
		Location = Directional * DiagonalSize + ActorLocation;
		return;
	}
	case ESubjectVertex::BDL:
	{
		Directional = UKismetMathLibrary::Normal(Backward + Down + Left);
		Location = Directional * DiagonalSize + ActorLocation;
		return;
	}
	case ESubjectVertex::BDR:
	{
		Directional = UKismetMathLibrary::Normal(Backward + Down + Right);
		Location = Directional * DiagonalSize + ActorLocation;
		return;
	}

	}
}
