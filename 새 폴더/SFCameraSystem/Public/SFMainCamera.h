﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

//#include "SFTicketExecutor.h"
//#include "SFTicket.h"

#include "SFMainCamera.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFMainCamera : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SFCAMERASYSTEM_API ISFMainCamera
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-002.02 ISFMainCamera Interface")
	void SetHandle(int AssignedHandle);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-002.02 ISFMainCamera Interface")
	void GetHandle(int& AssignedHandle);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-002.02 ISFMainCamera Interface")
	void SetAliasName(FName AliasName);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-002.02 ISFMainCamera Interface")
	void GetAliasName(FName& AliasName);
};
