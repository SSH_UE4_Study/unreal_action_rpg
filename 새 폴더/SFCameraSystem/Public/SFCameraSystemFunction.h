﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SFCameraSystemFunction.generated.h"

/**
 * 
 */
UCLASS()
class SFCAMERASYSTEM_API USFCameraSystemFunction : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "SF|Camera|Matrix")
	static FMatrix CreateViewMatrix(const FVector& EyeLocation, const FVector& LookAtLocation, FVector UpVector = FVector(0.0, 0.0, 1.0));

	UFUNCTION(BlueprintCallable, Category = "SF|Camera|Matrix")
	static void GetDirectionVector(const FMatrix& Matrix, FVector& Forward, FVector& Right, FVector& Up);

public:
	UFUNCTION(BlueprintCallable, Category = "SF|Camera|NDC")
	static FVector2D GetWorldToNDC(const FMatrix& ViewProjectionMatrix, const FVector& TargetLocation);

	UFUNCTION(BlueprintCallable, Category = "SF|Camera|NDC")
	static FVector2D GetWorldToScreenLocation(const FMatrix& ViewProjectionMatrix, const FVector& TargetLocation, const FVector2D& ViewPortSize);

	UFUNCTION(BlueprintCallable, Category = "SF|Camera|NDC")
	static FVector2D GetWorldToUnitNDC(const FMatrix& ViewProjectionMatrix, const FVector& TargetLocation);
	
	UFUNCTION(BlueprintCallable, Category = "SF|Camera|NDC")
	static FVector2D ConvertNDCToUnitNDC(const FVector2D& NDC);
};
