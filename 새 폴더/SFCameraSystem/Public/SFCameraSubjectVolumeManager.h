﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "SFMainCamera.h"
#include "SFCameraSubjectVolume.h"

#include "GameFramework/Actor.h"

#include "SFCameraSubjectVolumeManager.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class USFCameraSubjectVolumeManager : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class SFCAMERASYSTEM_API ISFCameraSubjectVolumeManager
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.

public:
	//virtual void SetSFCameraObject(UObject* pCamObject) = 0;
	//virtual UObject* GetSFCameraObject() = 0;

/**/
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolumeManager")
	void SetSFCamera(const TScriptInterface<ISFMainCamera>& iSFCamera);
	virtual void SetSFCamera_Implementation(const TScriptInterface<ISFMainCamera>& iSFCamera);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolumeManager")
	bool GetSFCamera(TScriptInterface<ISFMainCamera>& iSFCamera);
	virtual bool GetSFCamera_Implementation(TScriptInterface<ISFMainCamera>& iSFCamera);

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolumeManager")
	void AddSubjectVolumeAsset(const USFSubjectVolumeAsset* volumeAsset);
	virtual void AddSubjectVolumeAsset_Implementation(const USFSubjectVolumeAsset* volumeAsset);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolumeManager")
	bool GetSubjectVolumeAsset(FName Key, FSFSubjectVolume& subjectVolume);
	virtual bool GetSubjectVolumeAsset_Implementation(FName Key, FSFSubjectVolume& subjectVolume);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolumeManager")
	void RemoveSubjectVolumeAsset(FName Key);
	virtual void RemoveSubjectVolumeAsset_Implementation(FName Key);

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolumeManager")
	UObject* SpawnSubjectVolume(UClass* Class, FName Key, bool bUseCustomLocation = false, FVector Location = FVector(0.0f, 0.0f, 0.0f));
	virtual UObject* SpawnSubjectVolume_Implementation(UClass* Class, FName Key, bool bUseCustomLocation = false, FVector Location = FVector(0.0f, 0.0f, 0.0f));

public:
	TMap<FName, FSFSubjectVolume> SubjectVolumeAssets;
};

UCLASS(Blueprintable)
class SFCAMERASYSTEM_API ASFCameraSubjectVolumeManagerActor : public AActor, public ISFCameraSubjectVolumeManager
{
	GENERATED_BODY()

public:

protected:
	//UPROPERTY(BlueprintReadWrite, Category = "Rect")
	//FVector2D NDCMinMax_X;
	//UPROPERTY(BlueprintReadWrite, Category = "Rect")
	//FVector2D NDCMinMax_Y;
	//UPROPERTY(BlueprintReadWrite, Category = "Rect")
	//FVector2D NDCRatio;
	//UPROPERTY(BlueprintReadWrite, Category = "Rect")
	//FVector2D NDCOffset;
};
