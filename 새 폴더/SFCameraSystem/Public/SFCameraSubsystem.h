﻿#pragma once

#include "CoreMinimal.h"
//#include "Subsystems/GameInstanceSubsystem.h"
#include "Subsystems/LocalPlayerSubsystem.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"

#include "SFMainCamera.h"
#include "SFCameraSubjectVolume.h"

#include "SFTicketExecutor.h"
#include "SFTicket.h"
#include "Tickable.h"

#include "SFCameraSubsystem.generated.h"

#define NAME_CAMERASYSTEM	(FName(TEXT("SFCameraSystem")))
#define NAME_MAINCAMERA		(FName(TEXT("SFMainCamera")))

UCLASS(DisplayName = "SFC-002 SFCameraSystem")
class USFCCameraSubsystem
	: public ULocalPlayerSubsystem
	, public FTickableGameObject
	, public ISFTicketExecutor
{
	GENERATED_BODY()

	uint64 LastFrameNumberWeTicked = (uint64)-1;

	int _assigend_handle = 0;

protected:

	UPROPERTY()
	FName ExecutorName;

	UPROPERTY()
	TSet<FName>	_processableTicketNames;

	UPROPERTY()
	TScriptInterface<ISFMainCamera>		m_ISFMainCamera;

	UPROPERTY()
	TMap<FName, TScriptInterface<ISFMainCamera>>	m_SubCameras;
	TMap<int, FName> m_camHandleNamePairMap;

	// BP에서 티켓 처리를 위한 delegate
	UPROPERTY(BlueprintAssignable, Category = "SFC-002 SFCameraSystem")
	FDele_OnExecuteSFTicketMng Func_OnExecuteSFTicketMng;

	void Update(float dt);

public:

	virtual void Tick(float DeltaTime) override;

	virtual ETickableTickType GetTickableTickType() const override
	{
		return ETickableTickType::Always;
	}

	virtual TStatId GetStatId() const override
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FSFCameraSystem, STATGROUP_Tickables);
	}

	virtual bool IsTickableWhenPaused() const override
	{
		return true;
	}

	// Begin
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	// 실행기 이름 설정, 가져오기
	virtual void SetExecutorName_Implementation(FName name) override;
	virtual void GetExecutorName_Implementation(FName& name) override;


	// 실행기에 연결할 명령어(태그) 리스트 등록
	virtual void RegisterTickets_Implementation(const TArray<FName>& events) override;

	// 실행기 명령어 연결 해제
	virtual void UnregisterTickets_Implementation(const TArray<FName>& events) override;

	// 등록된 태그 리스트
	virtual int GetRegisteredTickets_Implementation(TArray<FName>& registerEvents) override;

	// 티겟 실 처리 부분
	virtual void ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket) override;


	// 메인 카메라 인터페이스 등록
	UFUNCTION(BlueprintCallable, Category = "SFC-002 SFCameraSystem")
	void SetSFMainCameraInterface(const TScriptInterface<ISFMainCamera>& iSFMainCamera);

	// 메인 카메라 인터페이스 가져오기
	UFUNCTION(BlueprintCallable, Category = "SFC-002 SFCameraSystem")
	bool GetSFMainCameraInterface(TScriptInterface<ISFMainCamera>& iSFMainCamera);

	// 서브 카메라(메인 교체용) 인터페이스, 이름으로 등록
	UFUNCTION(BlueprintCallable, Category = "SFC-002 SFCameraSystem")
	void AddSubCameraInterface(FName camAliasName, const TScriptInterface<ISFMainCamera>& iSFCamera, int& AssignedHandle);

	// 이름으로 서브 카메라 인터페이스 가져오기
	UFUNCTION(BlueprintCallable, Category = "SFC-002 SFCameraSystem")
	bool GetSubCameraInterface(FName camAliasName, TScriptInterface<ISFMainCamera>& iSFCamera);

	UFUNCTION(BlueprintCallable, Category = "SFC-002 SFCameraSystem")
	void RemoveSubCameraInterface(FName camAliasName);
};
