﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Engine/DataAsset.h"
#include "SFMainCamera.h"

#include "GameFramework/Actor.h"

#include "SFCameraSubjectVolume.generated.h"

class UWorld;

UENUM(BlueprintType)
enum class ESubjectVertex : uint8
{
	Center, Left, Right, Up, Down, Backward, Forward,
	FU, FD, FL, FR, LU, LD, RU, RD, BU, BD, BL, BR,
	FUL, FUR, FDL, FDR, BUL, BUR, BDL, BDR
};

USTRUCT(Atomic, BlueprintType)
struct FSFSubjectVolume
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCameraSubjectVolume Struct")
		FTransform Transform;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCameraSubjectVolume Struct")
		bool	IgnoreRotation = false;
};

UCLASS(BlueprintType)
class SFCAMERASYSTEM_API USFSubjectVolumeAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCameraSubjectVolume Asset")
		FName Key;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCameraSubjectVolume Asset")
		FSFSubjectVolume Data;
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFCameraSubjectVolume : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class SFCAMERASYSTEM_API ISFCameraSubjectVolume
{
	GENERATED_BODY()
		// Add interface functions to this class. This is the class that will be inherited to implement this interface.

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolume")
	FTransform GetSubjectActorTransform();
	virtual FTransform GetSubjectActorTransform_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolume")
	void SetSubjectActorLocation(FVector Location);
	virtual void SetSubjectActorLocation_Implementation(FVector Location) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolume")
	void SetSubjectActorRotation(FRotator Rotator);
	virtual void SetSubjectActorRotation_Implementation(FRotator Rotator) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolume")
	void SetSubjectActorScale(FVector Scale);
	virtual void SetSubjectActorScale_Implementation(FVector Scale) {}

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolume")
	void GetVertexInfo(ESubjectVertex TargetVertex, FVector& Directional, FVector& Location);
	virtual void GetVertexInfo_Implementation(ESubjectVertex TargetVertex, FVector& Directional, FVector& Location) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolume")
	float GetPlanArea(ESubjectVertex TargetVertex);
	virtual float GetPlanArea_Implementation(ESubjectVertex TargetVertex);

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolume")
	void SetFollowObject(UObject* Target, FName TargetSocket);
	virtual void SetFollowObject_Implementation(UObject* Target, FName TargetSocket) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolume")
	void SetAssist(bool IsAssist, const TScriptInterface<ISFCameraSubjectVolume>& SubjectVolume1, const TScriptInterface<ISFCameraSubjectVolume>& SubjectVolume2);
	virtual void SetAssist_Implementation(bool IsAssist, const TScriptInterface<ISFCameraSubjectVolume>& SubjectVolume1, const TScriptInterface<ISFCameraSubjectVolume>& SubjectVolume2) {}

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SubjectVolume")
	void SetRotate(bool State);
	virtual void SetRotate_Implementation(bool State) {}
};

UCLASS(Blueprintable)
class SFCAMERASYSTEM_API ASFCameraSubjectVolumeActor : public AActor, public ISFCameraSubjectVolume
{
	GENERATED_BODY()

public:
	virtual FTransform GetSubjectActorTransform_Implementation();
	virtual void SetSubjectActorLocation_Implementation(FVector Location);
	virtual void SetSubjectActorRotation_Implementation(FRotator Rotator);
	virtual void SetSubjectActorScale_Implementation(FVector Scale);

public:
	virtual void GetVertexInfo_Implementation(ESubjectVertex TargetVertex, FVector& Directional, FVector& Location) override;

};
