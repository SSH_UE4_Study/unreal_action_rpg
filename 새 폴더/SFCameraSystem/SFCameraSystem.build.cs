﻿using UnrealBuildTool;
 
public class SFCameraSystem : ModuleRules
{
	public SFCameraSystem(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"
			, "GameplayTags"
			, "SFCTicketSystem"});
 
		PublicIncludePaths.AddRange(new string[] {"SFCameraSystem/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFCameraSystem/Private"});
	}
}
