﻿using UnrealBuildTool;
 
public class SFInputSystem : ModuleRules
{
	public SFInputSystem(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"
			, "EnhancedInput"
			, "SFCTicketSystem"
			,"SFCCharacterSystem"
		});

		CircularlyReferencedDependentModules.AddRange(
			new string[] {
				"SFCCharacterSystem",
			});

		//PrivateIncludePathModuleNames.AddRange(
		//	new string[] {
		//		"SFCCharacterSystem",
		//	}
		//);

		//PublicIncludePathModuleNames.AddRange(
		//	new string[] {
		//		"SFCCharacterSystem",
		//	}
		//);

		PublicIncludePaths.AddRange(new string[] {"SFInputSystem/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFInputSystem/Private"});

		//PublicIncludePaths.AddRange(new string[] { "SFCCharacterSystem/Public" });
		//PrivateIncludePaths.AddRange(new string[] { "SFCCharacterSystem/Private" });
	}
}
