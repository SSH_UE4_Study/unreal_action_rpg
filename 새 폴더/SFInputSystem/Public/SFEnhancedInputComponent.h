﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "SFEnhancedInputComponent.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class SFINPUTSYSTEM_API USFEnhancedInputComponent : public UEnhancedInputComponent
{
	GENERATED_BODY()
	
};
