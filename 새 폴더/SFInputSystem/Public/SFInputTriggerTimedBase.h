﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputTriggers.h"
#include "SFInputTriggerTimedBase.generated.h"

/**
 * EnhancedInput의 InputTriggerTimedBase 함수 호출이 불가해서 만든 class
 */
UCLASS()
class SFINPUTSYSTEM_API USFInputTriggerTimedBase : public UInputTrigger
{
	GENERATED_BODY()
	
public:
	virtual ETriggerEventsSupported GetSupportedTriggerEvents() const override { return ETriggerEventsSupported::Ongoing; }

	virtual FString GetDebugState() const override { return HeldDuration ? FString::Printf(TEXT("Held:%.2f"), HeldDuration) : FString(); }

protected:
	// Transitions to Ongoing on actuation. Never triggers.
	virtual ETriggerState UpdateState_Implementation(const UEnhancedPlayerInput* PlayerInput, FInputActionValue ModifiedValue, float DeltaTime) override;

	// Calculates the new held duration given the current player input and delta time
	float CalculateHeldDuration(const UEnhancedPlayerInput* const PlayerInput, const float DeltaTime) const;

public:
	UPROPERTY(EditAnywhere, Config, BlueprintReadWrite, Category = "Trigger Settings")
	bool bAffectedByTimeDilation = false;

protected:
	// How long have we been actuating this trigger?
	UPROPERTY(BlueprintReadWrite, Category = "Trigger Settings")
	float HeldDuration = 0.0f;
};
