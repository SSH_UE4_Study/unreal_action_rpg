﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SFInputTriggerTimedBase.h"
#include "SFInputTriggerHoldInterval.generated.h"

/**
 * 
 */
UCLASS()
class SFINPUTSYSTEM_API USFInputTriggerHoldInterval : public USFInputTriggerTimedBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Trigger Settings", meta = (ClampMin = "0"))
	float HoldTimeThreshold = 1.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Trigger Settings", meta = (ClampMin = "0"))
	float Interval = 1.0f;

protected:
	virtual ETriggerState UpdateState_Implementation(const UEnhancedPlayerInput* PlayerInput, FInputActionValue ModifiedValue, float DeltaTime) override;

private:
	int32 TriggerCount = 0;
};
