﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SFPawnBehaviourState.generated.h"

struct FSFEHInputState;
struct FSFMovementStatus;
enum class ESurfaceType : uint8;


UENUM(BlueprintType)
enum class ESFPawnBehaviourType : uint8
{
	EPBT_Idle = 0 UMETA(DisplayName = "EPBT_Idle"),
	EPBT_Walk = 1 UMETA(DisplayName = "EPBT_Walk"),
	EPBT_Run = 2 UMETA(DisplayName = "EPBT_Run"),
	EPBT_Dash = 3 UMETA(DisplayName = "EPBT_Dash"),
	EPBT_Sprint = 4 UMETA(DisplayName = "EPBT_Sprint"),

	EPBT_ClimbIdle = 5 UMETA(DisplayName = "EPBT_ClimbIdle"),
	EPBT_Climbing = 6 UMETA(DisplayName = "EPBT_Climbing"),

	EPBT_StartDiving = 7 UMETA(DisplayName = "EPBT_StartDiving"),
	EPBT_Diving = 8 UMETA(DisplayName = "EPBT_Diving"),
	EPBT_DivingEnd = 9 UMETA(DisplayName = "EPBT_DivingEnd"),

	EPBT_StartGliding = 10 UMETA(DisplayName = "EPBT_StartGliding"),
	EPBT_Gliding = 11 UMETA(DisplayName = "EPBT_Gliding"),

	EPBT_StartJump = 12 UMETA(DisplayName = "EPBT_StartJump"),

	/// <summary>
	/// 향후 Rise 부분과 Fall 부분으로 확장 필요
	/// </summary>
	EPBT_Falling = 13 UMETA(DisplayName = "EPBT_Falling"),
	EPBT_Land = 14 UMETA(DisplayName = "EPBT_Land"),
	EPBT_RollingLand = 15 UMETA(DisplayName = "EPBT_RollingLand")
};



USTRUCT(BlueprintType)
struct FSFPawnState
{
	GENERATED_BODY()

		FSFPawnState() = default;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF PawnState")
		ESFPawnBehaviourType CurBehaviourState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF PawnState")
		ESFPawnBehaviourType PrevBehaviourState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF PawnState")
		ESurfaceType	PrevSurfaceType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF PawnState")
		ESurfaceType	CurSurfaceType;

	static ESFPawnBehaviourType CalcPawnBehaviourState(const FSFMovementStatus& prevMovStatus
		, const FSFMovementStatus& curMovStatus
		, ESurfaceType prevSurfaceType, ESurfaceType curSurfaceType);
};

