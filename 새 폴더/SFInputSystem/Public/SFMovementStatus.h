﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SFMovementStatus.generated.h"


UENUM(BlueprintType)
enum class ESurfaceType : uint8
{
	EST_OnGround	= 0	UMETA(DisplayName = "EST_OnGround"),
	EST_OnWall		= 1 UMETA(DisplayName = "EST_OnWall"),
	EST_InAir		= 2 UMETA(DisplayName = "EST_InAir"),
	EST_InWater		= 4 UMETA(DisplayName = "EST_InWater"),
};


USTRUCT(BlueprintType)
struct FSFMovementStatus
{
	GENERATED_BODY()

	FSFMovementStatus() = default;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF MovementStatus Payload")
	ESurfaceType SurfaceType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF MovementStatus Payload")
	FVector Acceleration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF MovementStatus Payload")
	FVector Velocity;

	// Body Forward
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF MovementStatus Payload")
	FVector BodyForward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF MovementStatus Payload")
	FVector Slope;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF MovementStatus Payload")
	FVector Location;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF MovementStatus Payload")
	FVector InputVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF MovementStatus Payload")
	float CurrentStamina;

	float PredictionLength;
};

/**
 * 
 */
UCLASS()
class SFINPUTSYSTEM_API USFMovementStatusAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF MovementStatus Asset")
	TArray<FSFMovementStatus> MovementStatus;
};
