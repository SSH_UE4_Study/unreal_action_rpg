﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"

#include "Engine/DataAsset.h"
#include "SFTicket.h"
#include "SFTicketExecutor.h"

#include "SFOperand.h"
#include "SFOperandPool.h"

#include "SFMovSensor.h"
#include "SFCTicketSystem.h"
#include "SFTicket.h"
#include "SFTicketExecutor.h"


#include "SFEHInputState.generated.h"


//class USFMovSensor;
//None = 0,

UENUM(meta=(Bitflags, UseEnumValuesAsMaskValuesInEditor = "true"))
enum class ESFInputActionFlags : uint32
{
	InputAction_Dash			= 1 << 0,
	InputAction_Jump			= 1 << 1,
	InputAction_Fly				= 1 << 2,
	InputAction_Diving			= 1 << 3,
	InputAction_Climb			= 1 << 4,
	InputAction_ClimbJump		= 1 << 5,
	InputAction_Fall			= 1 << 6,
	InputAction_Action			= 1 << 7,
	InputAction_Skill1			= 1 << 8,
	InputAction_Skill2			= 1 << 9,
	InputAction_WalkRunToggle	= 1 << 10,
	InputAction_DriveSkill		= 1 << 11,
	InputAction_PartnerSkill	= 1 << 12,
	InputAction_ClimbCancel		= 1 << 13,
	InputAction_CallRunners		= 1 << 14,
	InputAction_ChangeHero1		= 1 << 15,
	InputAction_ChangeHero2		= 1 << 16,
	InputAction_ChangeHero3		= 1 << 17,
	InputAction_ChangeHero4		= 1 << 18,
	InputAction_DefaultView		= 1 << 19,
};
ENUM_CLASS_FLAGS(ESFInputActionFlags)

UENUM(BlueprintType)
enum class ESFInputActionStatus : uint8
{
	ActionNone,
	ActionPressed,
	ActionReleased,
};

UENUM(BlueprintType)
enum class ESFInputActionMask : uint8
{
	None				= 0,
	Dash				= 1,
	Jump				= 2,
	Fly					= 3,
	Diving				= 4,
	Climb				= 5,
	ClimbJump			= 6,
	Fall				= 7,
	Action				= 8,
	Skill1				= 9,
	Skill2				= 10,
	WalkRunToggle		= 11,
	DriveSkill			= 12,
	PartnerSkill		= 13,
	ClimbCancel			= 14,
	CallRunners			= 15,
	ChangeHero1			= 16,
	ChangeHero2			= 17,
	ChangeHero3			= 18,
	ChangeHero4			= 19,
	DefaultView			= 20,
};



USTRUCT(BlueprintType)
struct FSFEHInputState
{
	GENERATED_BODY()

	FSFEHInputState() = default;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF EHInputState")
	FVector InputDir;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF EHInputState")
	FVector CamTurnDir;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF EHInputState", meta=(Bitmask, BitmaskEnum=ESFInputActionFlags))
	int32	InputActionFlags = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF EHInputState", meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))
	int32	ReleaseActionFlags = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF EHInputState")
		float ActionHold = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SF EHInputState")
		float Skill1Hold = 0.f;
};

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDele_OnPressReleaseAction, bool, IsPressed);
DECLARE_DYNAMIC_DELEGATE_OneParam(FDele_OnPressReleaseAction, bool, IsPressed);

UCLASS(BlueprintType)
class SFINPUTSYSTEM_API USFEHInputStateAsset : public UDataAsset // public UObject
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFEHInputState Asset")
	FVector InputDir;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFEHInputState Asset")
	FVector CamTurnDir;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFEHInputState Asset", meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))
	int32	InputActionFlags = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFEHInputState Asset", meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))
	int32	ReleaseActionFlags = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFEHInputState Asset")
	float ActionHold = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFEHInputState Asset")
	float Skill1Hold = 0.f;

	//Transient
	//UPROPERTY(BlueprintAssignable, Category = "SFEHInputState Asset")
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFEHInputState Asset")
	UPROPERTY()
	TMap<int32, FDele_OnPressReleaseAction> PressEventMap;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFEHInputState Asset")
	UPROPERTY()
	TMap<int32, FDele_OnPressReleaseAction> ReleaseEventMap;


	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void GetEHInputStateStruct(FSFEHInputState& InputStateStruct);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void ApplyEHInputStateStruct(const FSFEHInputState& InputStateStruct);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void OnPressFlag(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void OnRemovePressFlag(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void ClearPressFlag() { InputActionFlags = 0;	}

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	bool IsPressed(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);



	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void OnReleaseFlag(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void OnRemoveReleaseFlag(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	bool IsReleased(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void ClearReleaseActionFlag() { ReleaseActionFlags = 0; }

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset", meta=(ExpandEnumAsExecs="ActionStatus"))
	void SwitchInputStatus(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag, ESFInputActionStatus& ActionStatus);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void GetPressedActionMaskArray(TArray<ESFInputActionMask>& retMaskArray);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void GetReleaseActionMaskArray(TArray<ESFInputActionMask>& retMaskArray);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset", meta=(ToolTip="OnPressXXXX 형태의 BP함수를 호출한다. XXXX는 Enum"))
	void CallBPPressActionFunc(AActor* Owner, ESFInputActionMask ActionMask);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset", meta = (ToolTip = "OnReleaseXXXX 형태의 BP함수를 호출한다. XXXX는 Enum"))
	void CallBPReleaseActionFunc(AActor* Owner, ESFInputActionMask ActionMask);

	UFUNCTION(BlueprintPure, Category = "SFEHInputState Asset")
	void ActionFlagToActionMask(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag, ESFInputActionMask& mask);

	void _CallBPFunc(AActor* pOwner, FName funcName);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void BindPressActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag, const FDele_OnPressReleaseAction& userEvent);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void UnbindPressActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void CallBindPressActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);


	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void BindReleaseActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag, const FDele_OnPressReleaseAction& userEvent);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void UnbindReleaseActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);

	UFUNCTION(BlueprintCallable, Category = "SFEHInputState Asset")
	void CallBindReleaseActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag);

	// , meta=(Bitmask, BitmaskEnum = ESFInputActionFlags)
	// , meta = (Bitmask, BitmaskEnum = ESFInputActionFlags)
};




UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SFINPUTSYSTEM_API USFInputBufferComponent
	: public UActorComponent
	, public ISFTicketExecutor
{
	GENERATED_BODY()

protected:

	FName ExecutorName;

	virtual void InitializeComponent() override;

public:
	UPROPERTY(Transient)
	FSFEHInputState		InputStateBuffer[2];

	UPROPERTY(Transient)
	int CurrentBuffer = 0;

	// 연결된 센서
	UPROPERTY(Transient)
	TObjectPtr<USFMovSensor> ConnectedMovSensor;

	UFUNCTION(BlueprintCallable, Category = "Custom Movement")
	void OnConnectMovSensor(USFMovSensor* MovSensor);


	FSFEHInputState& GetCurrentBuffer() { return InputStateBuffer[CurrentBuffer]; }

	void PushInputState(const FSFEHInputState& curInputState);
	void UpdateInputState(const USFMovSensor& movSensor);


public:
	USFInputBufferComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	// 실행기 이름 설정, 가져오기
	virtual void SetExecutorName_Implementation(FName name) override;

	virtual void GetExecutorName_Implementation(FName& name) override;

	// 실행기에 연결할 명령어(태그) 리스트 등록
	virtual void RegisterTickets_Implementation(const TArray<FName>& events) override;

	// 실행기 명령어 연결 해제
	virtual void UnregisterTickets_Implementation(const TArray<FName>& events) override;

	// 등록된 태그 리스트
	virtual int GetRegisteredTickets_Implementation(TArray<FName>& registerEvents) override;

	// 티겟 실 처리 부분
	virtual void ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket) override;

	virtual void ManualExecuteTicket_Implementation(const FSFTicketStruct& manualTicket) override;

};
