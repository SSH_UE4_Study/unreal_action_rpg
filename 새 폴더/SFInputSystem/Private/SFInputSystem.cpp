﻿#include "SFInputSystem.h"

DEFINE_LOG_CATEGORY(LogSFInputSystem);

#define LOCTEXT_NAMESPACE "FSFInputSystem"

void FSFInputSystem::StartupModule()
{
	SF_LOG(Warning, TEXT("SFInputSystem module has been loaded"));
}

void FSFInputSystem::ShutdownModule()
{
	SF_LOG(Warning, TEXT("SFInputSystem module has been unloaded"));
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFInputSystem, SFInputSystem)
