﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SFInputTriggerHoldInterval.h"

ETriggerState USFInputTriggerHoldInterval::UpdateState_Implementation(const UEnhancedPlayerInput* PlayerInput, FInputActionValue ModifiedValue, float DeltaTime)
{
	ETriggerState State = Super::UpdateState_Implementation(PlayerInput, ModifiedValue, DeltaTime);

	if (HeldDuration >= HoldTimeThreshold)
	{
		if (HeldDuration - HoldTimeThreshold >= Interval * (TriggerCount + 1))
		{
			++TriggerCount;
			return ETriggerState::Triggered;
		}
		else
		{
			return ETriggerState::None;
		}
	}

	return State;
}
