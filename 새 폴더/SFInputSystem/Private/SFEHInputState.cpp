﻿
#include "SFEHInputState.h"
//#include "SFInputSystem.h"
#include "SFPawnBehaviourState.h"
#include "SFMovementStatus.h"

#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFOperandPoolSubsystem.h"

//#include "SFMovSensor.h"
//#include "SFCTicketSystem.h"
//#include "SFTicket.h"
//#include "SFTicketExecutor.h"


void USFEHInputStateAsset::GetEHInputStateStruct(FSFEHInputState& InputStateStruct)
{
	InputStateStruct.InputDir = InputDir;
	InputStateStruct.CamTurnDir = CamTurnDir;
	InputStateStruct.InputActionFlags = InputActionFlags;
	InputStateStruct.ReleaseActionFlags = ReleaseActionFlags;
	InputStateStruct.ActionHold = ActionHold;
	InputStateStruct.Skill1Hold = Skill1Hold;
}

void USFEHInputStateAsset::ApplyEHInputStateStruct(const FSFEHInputState& InputStateStruct)
{
	InputDir	= InputStateStruct.InputDir;
	CamTurnDir	= InputStateStruct.CamTurnDir;
	InputActionFlags = InputStateStruct.InputActionFlags;
	ReleaseActionFlags = InputStateStruct.ReleaseActionFlags;
	ActionHold	= InputStateStruct.ActionHold;
	Skill1Hold	= InputStateStruct.Skill1Hold;
}

// 버튼 프레스는 Press 설정, Release 제거
void USFEHInputStateAsset::OnPressFlag(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	ReleaseActionFlags &= ~flag;
	InputActionFlags |= flag;
	UE_LOG(LogTemp, Warning, TEXT("%x"), InputActionFlags);
}

// 버튼 프레스 제거는 Press 제거
void USFEHInputStateAsset::OnRemovePressFlag(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	InputActionFlags &= ~flag;
	UE_LOG(LogTemp, Warning, TEXT("%x"), InputActionFlags);
}

bool USFEHInputStateAsset::IsPressed(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	return (InputActionFlags & flag);
}


// 버튼 릴리즈 적용은 Press 제거, Release 세팅
void USFEHInputStateAsset::OnReleaseFlag(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	ReleaseActionFlags |= flag;
	InputActionFlags &= ~flag;
}

// 버튼 릴리즈는 Press, Release 둘다 제거
void USFEHInputStateAsset::OnRemoveReleaseFlag(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	InputActionFlags &= ~flag;
	ReleaseActionFlags &= ~flag;
}

bool USFEHInputStateAsset::IsReleased(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	return (ReleaseActionFlags & flag);
}

void USFEHInputStateAsset::SwitchInputStatus(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag
											 , ESFInputActionStatus& ActionStatus)
{
	ActionStatus = (InputActionFlags & flag) ? ESFInputActionStatus::ActionPressed :
		(ReleaseActionFlags & flag ) ? ESFInputActionStatus::ActionReleased : ESFInputActionStatus::ActionNone;
}

void USFEHInputStateAsset::GetPressedActionMaskArray(TArray<ESFInputActionMask>& retMaskArray)
{
	int _actionMask = (int)ESFInputActionMask::Dash;

	int _testMask = 1;
	for (int i = 0; i < 20; ++i)
	{
		if(InputActionFlags & (_testMask << i))
			retMaskArray.Add( (ESFInputActionMask)(_actionMask + i));
	}
}

void USFEHInputStateAsset::GetReleaseActionMaskArray(TArray<ESFInputActionMask>& retMaskArray)
{
	int _actionMask = (int)ESFInputActionMask::Dash;

	int _testMask = 1;
	for (int i = 0; i < 20; ++i)
	{
		if (ReleaseActionFlags & (_testMask << i))
			retMaskArray.Add((ESFInputActionMask)(_actionMask + i));
	}
}

void USFEHInputStateAsset::ActionFlagToActionMask(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag, ESFInputActionMask& mask)
{
	ESFInputActionFlags _em = (ESFInputActionFlags)flag;
	ESFInputActionMask _retMask = ESFInputActionMask::None;

	switch (_em)
	{
	case ESFInputActionFlags::InputAction_Dash:
		_retMask = ESFInputActionMask::Dash;
		break;
	case ESFInputActionFlags::InputAction_Jump:
		_retMask = ESFInputActionMask::Jump;
		break;
	case ESFInputActionFlags::InputAction_Fly:
		_retMask = ESFInputActionMask::Fly;
		break;
	case ESFInputActionFlags::InputAction_Diving:
		_retMask = ESFInputActionMask::Diving;
		break;
	case ESFInputActionFlags::InputAction_Climb:
		_retMask = ESFInputActionMask::Climb;
		break;
	case ESFInputActionFlags::InputAction_ClimbJump:
		_retMask = ESFInputActionMask::ClimbJump;
		break;
	case ESFInputActionFlags::InputAction_Fall:
		_retMask = ESFInputActionMask::Fall;
		break;
	case ESFInputActionFlags::InputAction_Action:
		_retMask = ESFInputActionMask::Action;
		break;
	case ESFInputActionFlags::InputAction_Skill1:
		_retMask = ESFInputActionMask::Skill1;
		break;
	case ESFInputActionFlags::InputAction_Skill2:
		_retMask = ESFInputActionMask::Skill2;
		break;
	case ESFInputActionFlags::InputAction_WalkRunToggle:
		_retMask = ESFInputActionMask::WalkRunToggle;
		break;
	case ESFInputActionFlags::InputAction_DriveSkill:
		_retMask = ESFInputActionMask::DriveSkill;
		break;
	case ESFInputActionFlags::InputAction_PartnerSkill:
		_retMask = ESFInputActionMask::PartnerSkill;
		break;
	case ESFInputActionFlags::InputAction_ClimbCancel:
		_retMask = ESFInputActionMask::ClimbCancel;
		break;
	case ESFInputActionFlags::InputAction_CallRunners:
		_retMask = ESFInputActionMask::CallRunners;
		break;
	case ESFInputActionFlags::InputAction_ChangeHero1:
		_retMask = ESFInputActionMask::ChangeHero1;
		break;
	case ESFInputActionFlags::InputAction_ChangeHero2:
		_retMask = ESFInputActionMask::ChangeHero2;
		break;
	case ESFInputActionFlags::InputAction_ChangeHero3:
		_retMask = ESFInputActionMask::ChangeHero3;
		break;
	case ESFInputActionFlags::InputAction_ChangeHero4:
		_retMask = ESFInputActionMask::ChangeHero4;
		break;
	case ESFInputActionFlags::InputAction_DefaultView:
		_retMask = ESFInputActionMask::DefaultView;
		break;
	default:
		break;
	}
}

// BP 사이드 기능 함수 호출
void USFEHInputStateAsset::CallBPPressActionFunc(AActor* Owner, ESFInputActionMask ActionMask)
{
	if (!Owner)
		return;

	const UEnum* ActionMaskEnum = FindObject<UEnum>(ANY_PACKAGE, TEXT("ESFInputActionMask"), true);

	if (ActionMaskEnum)
	{
		FString postfixName = ActionMaskEnum->GetNameStringByValue((int64)ActionMask);
		_CallBPFunc(Owner, FName( *FString::Printf(TEXT("OnPress%s")
			,*ActionMaskEnum->GetNameStringByValue((int64)ActionMask))));
	}
}

void USFEHInputStateAsset::CallBPReleaseActionFunc(AActor* Owner, ESFInputActionMask ActionMask)
{
	if (!Owner)
		return;

	const UEnum* ActionMaskEnum = FindObject<UEnum>(ANY_PACKAGE, TEXT("ESFInputActionMask"), true);

	if (ActionMaskEnum)
	{
		_CallBPFunc(Owner, FName(*FString::Printf(TEXT("OnRelease%s")
			, *ActionMaskEnum->GetNameStringByValue((int64)ActionMask))));
	}
}

void USFEHInputStateAsset::_CallBPFunc(AActor * pOwner, FName funcName)
{
	UFunction* const func = pOwner->FindFunction(funcName);

	if (func)
	{
		SF_LOG(Warning, TEXT(">> CallBP InputAction Func : (%s)"), *funcName.ToString());

		if (func->ParmsSize == 0)
			pOwner->ProcessEvent(func, nullptr);
		//else
		//{
		//	void* buf = FMemory_Alloca(func->ParmsSize);
		//	FMemory::Memzero(buf, func->ParmsSize);

		//	for (TFieldIterator<FProperty> It(func);
		//		 It && It->HasAnyPropertyFlags(CPF_Parm); ++It)
		//	{
		//		FProperty* Prop = *It;
		//		FString strType = Prop->GetCPPType();

		//		if (strType == TEXT("TScriptInterface"))
		//		{
		//			TScriptInterface<ISFTicket>* ppTicketParam =
		//				Prop->ContainerPtrToValuePtr<TScriptInterface<ISFTicket>>(buf);
		//			*ppTicketParam = iTicket;
		//			//.GetInterface();
		//		}
		//	}

		//	pOwner->ProcessEvent(func, buf);
		//}
	}
}

void USFEHInputStateAsset::BindPressActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag
												, const FDele_OnPressReleaseAction& userEvent)
{
	PressEventMap.Add(flag, userEvent);
}

void USFEHInputStateAsset::UnbindPressActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	PressEventMap.Remove(flag);
}

void USFEHInputStateAsset::CallBindPressActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	auto* pEvent = PressEventMap.Find(flag);

	if (pEvent && pEvent->IsBound()) {
		pEvent->Execute(true);
	}
}

void USFEHInputStateAsset::BindReleaseActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag
												  , const FDele_OnPressReleaseAction& userEvent)
{
	ReleaseEventMap.Add(flag, userEvent);
}

void USFEHInputStateAsset::UnbindReleaseActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	ReleaseEventMap.Remove(flag);
}

void USFEHInputStateAsset::CallBindReleaseActionEvent(UPARAM(meta = (Bitmask, BitmaskEnum = ESFInputActionFlags))int32 flag)
{
	auto* pEvent = ReleaseEventMap.Find(flag);

	if (pEvent && pEvent->IsBound()) {
		pEvent->Execute(false);
	}
}






//
//
//
//
//
void USFInputBufferComponent::PushInputState(const FSFEHInputState& curInputState)
{
	InputStateBuffer[CurrentBuffer] = curInputState;

	//CurrentBuffer = !CurrentBuffer;	// 계산 후 버퍼 채인지
}


void USFInputBufferComponent::UpdateInputState(const USFMovSensor& movSensor)
{
	FSFEHInputState& preIS = InputStateBuffer[!CurrentBuffer];
	FSFEHInputState& curIS = InputStateBuffer[CurrentBuffer];

	//curIS.InputDir




	
	// 다 처리 후 
	CurrentBuffer = !CurrentBuffer;	// 계산 후 버퍼 채인지
}

void USFInputBufferComponent::OnConnectMovSensor(USFMovSensor* MovSensor)
{
	ConnectedMovSensor = MovSensor;
}

//
//
//
USFInputBufferComponent::USFInputBufferComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
}

void USFInputBufferComponent::InitializeComponent()
{
	Super::InitializeComponent();

}

void USFInputBufferComponent::SetExecutorName_Implementation(FName name)
{
	ExecutorName = name;
}

void USFInputBufferComponent::GetExecutorName_Implementation(FName& name)
{
	name = ExecutorName;
}

void USFInputBufferComponent::RegisterTickets_Implementation(const TArray<FName>& events)
{
}

void USFInputBufferComponent::UnregisterTickets_Implementation(const TArray<FName>& events)
{
}

int USFInputBufferComponent::GetRegisteredTickets_Implementation(TArray<FName>& registerEvents)
{
	return 0;
}

void USFInputBufferComponent::ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket)
{
	ensureMsgf(iTicket, TEXT("Ticket is null or none"));

	// BP Event 호출하기
	//FSFTicketStruct _rawTicket;
	//ISFTicket::Execute_GetTicket(iTicket.GetObject(), _rawTicket);

	//FName funcName = FName(*(TEXT("On") + _rawTicket.TicketName.ToString()));
	//CallBPFunc(GetOwner(), funcName, iTicket);
}

void USFInputBufferComponent::ManualExecuteTicket_Implementation(const FSFTicketStruct& manualTicket)
{
}



void USFInputBufferComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateInputState(*ConnectedMovSensor);
}
