using UnrealBuildTool;
 
public class SFEnvironmentSystem : ModuleRules
{
	public SFEnvironmentSystem(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"});
 
		PublicIncludePaths.AddRange(new string[] {"SFEnvironmentSystem/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFEnvironmentSystem/Private"});
	}
}