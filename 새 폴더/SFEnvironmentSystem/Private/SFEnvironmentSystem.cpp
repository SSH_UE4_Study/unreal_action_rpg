#include "SFEnvironmentSystem.h"

DEFINE_LOG_CATEGORY(SFEnvironmentSystem);

#define LOCTEXT_NAMESPACE "FSFEnvironmentSystem"

void FSFEnvironmentSystem::StartupModule()
{
	UE_LOG(SFEnvironmentSystem, Warning, TEXT("SFEnvironmentSystem module has been loaded"));
}

void FSFEnvironmentSystem::ShutdownModule()
{
	UE_LOG(SFEnvironmentSystem, Warning, TEXT("SFEnvironmentSystem module has been unloaded"));
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFEnvironmentSystem, SFEnvironmentSystem)