﻿#include "SFDataAssetGenerator.h"
#include "IAssetTools.h"
#include "Factories/Factory.h"
#include "SFTicket.h"
#include "SFTicketAssetFactory.h"

#include "SFHFSM.h"
#include "SFHFSMAssetDetailsCustomization.h"

#include "EditorAssetLibrary.h"
#include "AssetRegistry/AssetRegistryModule.h"



DEFINE_LOG_CATEGORY(SFDataAssetGenerator);

#define LOCTEXT_NAMESPACE "FSFDataAssetGenerator"


// FAssetTypeActions_Base 상속받은 클래스를 각 데이터 별로 복사하여 확장.
class FSFTicketAssetActions : public FAssetTypeActions_Base
{
public:

	FSFTicketAssetActions(EAssetTypeCategories::Type assetCategory)
		: AssetCategory(assetCategory) {}

	virtual FText GetName() const override
	{
		return NSLOCTEXT("AssetTypeActions"
			, "AssetTypeActions_FSFTicketAssetModule"
			, "SFTicket Asset");
	}
	virtual UClass* GetSupportedClass() const override
	{
		return USFTicketAsset::StaticClass();
	}
	virtual uint32 GetCategories() override
	{
		return AssetCategory;
	}
	virtual FColor GetTypeColor() const override
	{
		return FColor::Yellow;
	}

private:
	EAssetTypeCategories::Type AssetCategory = EAssetTypeCategories::Misc;
};


//UCLASS()
//class USFTicketAssetFactory : public UFactory
//{
//	GENERATED_BODY()
//public:
//	USFTicketAssetFactory(const FObjectInitializer& ObjectInitializer)
//		: Super(ObjectInitializer)
//	{
//		SupportedClass = USFTicketAsset::StaticClass();
//		bCreateNew = true;
//		bEditAfterNew = true;
//	}
//
//	virtual UObject* FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override
//	{
//		return NewObject<USFTicketAsset>(InParent, InClass, InName, Flags);
//	}
//	virtual bool ShouldShowInNewMenu() const override
//	{
//		return true;
//	}
//};




void FSFDataAssetGenerator::StartupModule()
{
	UE_LOG(SFDataAssetGenerator, Warning, TEXT("SFDataAssetGenerator module has been loaded"));

	IAssetTools& AssetTools =
		FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();

	// 1) 커스텀 에셋 카테고리 등록
	const auto AssetCategory =
		AssetTools.RegisterAdvancedAssetCategory(
			FName(TEXT("SF용 데이터 에셋")), LOCTEXT("NewAssetCategory", "SF용 데이터 에셋"));

	/// 3) 티켓 데이터 액션 등록
	AssetTools.RegisterAssetTypeActions(MakeShareable(new FSFTicketAssetActions(AssetCategory)));

	
	// 프로퍼티 등록
	FPropertyEditorModule& PM =
		FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PM.RegisterCustomClassLayout(USFHFSMState::StaticClass()->GetFName(),
		FOnGetDetailCustomizationInstance::CreateStatic(
			&FSFHFSMAssetDetailsCustomization::MakeInstance));
	PM.NotifyCustomizationModuleChanged();

}

void FSFDataAssetGenerator::ShutdownModule()
{
	UE_LOG(SFDataAssetGenerator, Warning, TEXT("SFDataAssetGenerator module has been unloaded"));

	FPropertyEditorModule& PM =
		FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PM.UnregisterCustomClassLayout(USFHFSMState::StaticClass()->GetFName());
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFDataAssetGenerator, SFDataAssetGenerator)
