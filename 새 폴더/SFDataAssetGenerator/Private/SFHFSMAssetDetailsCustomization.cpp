﻿#include "SFHFSMAssetDetailsCustomization.h"
#include "IDetailsView.h"
#include "DetailCategoryBuilder.h"

#include "DetailWidgetRow.h"


#include "EditorAssetLibrary.h"
#include "AssetRegistry/AssetRegistryModule.h"
#include "Kismet/GameplayStatics.h"

#include "SFHFSMCondition.h"
#include "SFHFSM.h"


#define LOCTEXT_NAMESPACE "SFHFSMAssetDetailsCustomizationNameSpace"

void FSFHFSMAssetDetailsCustomization::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
	const TArray<TWeakObjectPtr<UObject>>& SelectObjects =
		DetailBuilder.GetDetailsView()->GetSelectedObjects();

	for (int32 ObjectIndex = 0; !CurAsset.IsValid() && ObjectIndex < SelectObjects.Num(); ++ObjectIndex)
	{
		const TWeakObjectPtr<UObject>& CurrentObject = SelectObjects[ObjectIndex];
		if( CurrentObject.IsValid() )
			CurAsset = Cast<USFHFSMState>(CurrentObject.Get());
	}

	FillComboBoxWithCoditionObjectNames();

	IDetailCategoryBuilder& Category
		= DetailBuilder.EditCategory(TEXT("SFHFSM State"));

	// Take all
	TArray<TSharedRef<IPropertyHandle>> sfHFSMProperties;
	Category.GetDefaultProperties(sfHFSMProperties);

	TSharedRef<IPropertyHandle> ConProp = 
		DetailBuilder.GetProperty(GET_MEMBER_NAME_CHECKED(USFHFSMState, Condition)
			, USFHFSMState::StaticClass());
	ConProp->MarkHiddenByCustomization();

	for (TSharedPtr<IPropertyHandle> Property : sfHFSMProperties)
	{
		Category.AddProperty(Property);
	}

	return;

	// 결과 : 기존 Object list 안나오고, 오브젝트 선택 시 하위 리스트도 안나옴.
	for (TSharedPtr<IPropertyHandle> Property : sfHFSMProperties)
	{
		if( Property->GetProperty() != ConProp->GetProperty() )
			Category.AddProperty(Property);
		else
		{
			Category.AddCustomRow(ConProp->GetPropertyDisplayName(), false)
				.NameContent()
				[
					ConProp->CreatePropertyNameWidget()
					//SNew(STextBlock)
					//.Text(FText::FromString(TEXT("테스트")))
				]
			.ValueContent()
				[
					SNew(SComboBox<FSFComboItemType>)
					.OptionsSource(&Options)
				.OnSelectionChanged(this, &FSFHFSMAssetDetailsCustomization::OnSelectionChanged)
				.OnGenerateWidget(this, &FSFHFSMAssetDetailsCustomization::MakeWidgetForOption)
				.InitiallySelectedItem(CurrentOption)
				[
					SNew(STextBlock)
					.Text(this,
						  &FSFHFSMAssetDetailsCustomization::GetCurrentItemLabel)
				]
			//SNew(SButton)
			//.Text(FText::FromString("Test"))
				];
		}
	}
}

void FSFHFSMAssetDetailsCustomization::OnSelectionChanged(FSFComboItemType NewValue, ESelectInfo::Type)
{
	CurrentOption = NewValue;
	if (CurAsset.IsValid())
	{
		CurAsset->Condition.Empty();// = nullptr;

		if( *NewValue.Get() == "CmpPlayerState")
			CurAsset->SetNewCondition(USFHFSMConditionCmpPlayerState::StaticClass());
		else if( *NewValue.Get() == "CmpWorldState")
			CurAsset->SetNewCondition(USFHFSMConditionCmpWorldState::StaticClass());
	}
}

TSharedRef<SWidget> FSFHFSMAssetDetailsCustomization::MakeWidgetForOption(FSFComboItemType InOption)
{
	return SNew(STextBlock).Text(FText::FromString(*InOption));
}

FText FSFHFSMAssetDetailsCustomization::GetCurrentItemLabel() const
{
	if (CurrentOption.IsValid())
	{
		return FText::FromString(*CurrentOption);
	}

	return FText::FromString("Invalid Selection");
}

void FSFHFSMAssetDetailsCustomization::FillComboBoxWithCoditionObjectNames()
{
	Options.Empty();
	if (CurAsset.IsValid())
	{
		//CurAsset->Condition = nullptr


		Options.Add(MakeShareable(new FString("CmpPlayerState")));
		Options.Add(MakeShareable(new FString("CmpWorldState")));
	}
}


//
//
//
//
//
//
void FSFHFSMAssetPropertyDetails::CustomizeHeader(TSharedRef<IPropertyHandle> PropertyHandle
												  , FDetailWidgetRow& HeaderRow
												  , IPropertyTypeCustomizationUtils& CustomizationUtils)
{
	//uint32 NumChildren;
	//PropertyHandle->GetNumChildren(NumChildren);

	//for (uint32 ChildIndex = 0; ChildIndex < NumChildren; ++ChildIndex)
	//{
	//	const TShar
	//}
}


#undef LOCTEXT_NAMESPACE
