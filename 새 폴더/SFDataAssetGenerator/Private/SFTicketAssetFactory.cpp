﻿#include "SFTicketAssetFactory.h"
#include "SFTicket.h"

//
//
//
//
//
USFTicketAssetFactory::USFTicketAssetFactory(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SupportedClass = USFTicketAsset::StaticClass();
	bCreateNew = true;
	bEditAfterNew = true;
}

UObject* USFTicketAssetFactory::FactoryCreateNew(UClass* InClass
												 , UObject* InParent
												 , FName InName
												 , EObjectFlags Flags
												 , UObject* Context
												 , FFeedbackContext* Warn)
{
	return NewObject<USFTicketAsset>(InParent, InClass, InName, Flags);
}

bool USFTicketAssetFactory::ShouldShowInNewMenu() const
{
	return true;
}
