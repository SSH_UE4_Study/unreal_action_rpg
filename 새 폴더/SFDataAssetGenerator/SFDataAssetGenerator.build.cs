﻿using UnrealBuildTool;
 
public class SFDataAssetGenerator : ModuleRules
{
	public SFDataAssetGenerator(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] 
			{ "Core"
			, "CoreUObject"
			, "Engine"
			, "UnrealEd"
			, "SFCTicketSystem"
			, "SFCameraSystem"
			, "SFInputSystem"
			, "SFCCharacterSystem"});

		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"Projects",
				"InputCore",
				"EditorFramework",
				"UnrealEd",
				"ToolMenus",
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				// ... add private dependencies that you statically link with here ...	
				"PropertyEditor",
				"EditorStyle",
				"JsonUtilities",
				"Json",
				"SFCTicketSystem",
				"EditorScriptingUtilities",
				"AssetRegistry",
			}
			);


		PublicIncludePaths.AddRange(new string[] {"SFDataAssetGenerator/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFDataAssetGenerator/Private"});
	}
}
