﻿#pragma once

#include "SFHFSM.h"
#include "DetailLayoutBuilder.h"
#include "IDetailCustomization.h"
#include "IPropertyTypeCustomization.h"

class FSFHFSMAssetDetailsCustomization : public IDetailCustomization
{
public:
	virtual void CustomizeDetails(IDetailLayoutBuilder& DetailBuilder) override;

	static TSharedRef<IDetailCustomization> MakeInstance()
	{
		return MakeShareable(new FSFHFSMAssetDetailsCustomization);
	}


	//void SelectCondition();


	TWeakObjectPtr<class USFHFSMState> CurAsset;

	// Combo data
	typedef TSharedPtr<FString> FSFComboItemType;

	FSFComboItemType CurrentOption;
	TArray<FSFComboItemType> Options;

	FText GetCurrentItemLabel() const;
	void OnSelectionChanged(FSFComboItemType NewValue, ESelectInfo::Type);
	TSharedRef<SWidget> MakeWidgetForOption(FSFComboItemType InOption);

	void FillComboBoxWithCoditionObjectNames();
};


//
// 프로퍼티 커스터마이징
//
class FSFHFSMAssetPropertyDetails : public IPropertyTypeCustomization
{
public:
	//void SelectCondition();

	static TSharedPtr<IPropertyTypeCustomization> MakeInstance()
	{
		return MakeShareable(new FSFHFSMAssetPropertyDetails);
	}

	USFHFSMState* CurAsset;

	virtual void CustomizeHeader(TSharedRef<IPropertyHandle> PropertyHandle, FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& CustomizationUtils) override;
	virtual void CustomizeChildren(TSharedRef<IPropertyHandle> PropertyHandle, IDetailChildrenBuilder& ChildBuilder, IPropertyTypeCustomizationUtils& CustomizationUtils) override;

	
};
