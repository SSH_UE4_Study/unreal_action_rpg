﻿#pragma once

#include "Modules/ModuleManager.h"
#include "AssetTypeActions_Base.h"


DECLARE_LOG_CATEGORY_EXTERN(SFDataAssetGenerator, All, All);

class FSFDataAssetGenerator : public IModuleInterface
{
	public:

	/* Called when the module is loaded */
	virtual void StartupModule() override;

	/* Called when the module is unloaded */
	virtual void ShutdownModule() override;
};
