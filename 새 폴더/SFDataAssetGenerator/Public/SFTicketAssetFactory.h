﻿#pragma once

#include "CoreMinimal.h"
#include "Factories/Factory.h"
#include "SFTicketAssetFactory.generated.h"


UCLASS()
class USFTicketAssetFactory : public UFactory
{
	GENERATED_BODY()
public:
	USFTicketAssetFactory(const FObjectInitializer& ObjectInitializer);

	virtual UObject* FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;
	virtual bool ShouldShowInNewMenu() const override;
};
