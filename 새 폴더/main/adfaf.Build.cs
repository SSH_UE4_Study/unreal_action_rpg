// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class ProjectSF : ModuleRules
{
	public ProjectSF(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		bUseUnity = false;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "GameplayTags", "AIModule", 
			"NavigationSystem", "Niagara", "AnimGraphRuntime", "PhysicsCore", "UMG","Networking", "Sockets", "GameplayTasks", "ZoneGraph", 
			/*"Blutility", "AssetManagerEditor",*/ "ContextualAnimation", "MassZoneGraphNavigation", "MassTraffic", "StructUtils", "StateTreeModule",
			"ApplicationCore", 
			"SFInputSystem", 
			"SFCCharacterSystem", 
			"SFHUDSystem",
			"SFCTicketSystem"});

		CircularlyReferencedDependentModules.AddRange(
			new string[]
			{
				"SFCCharacterSystem",
			});

		PrivateDependencyModuleNames.AddRange(new string[]
		{
            "AnimToTexture",
            "CitySampleMassCrowd",

			// AI/AISupport Plugin Modules
			"AISupportModule",

			// AI/MassAI Plugin Modules
			"MassAIBehavior",
            "MassAIDebug",
			
			// AI/MassCrowd Plugin Modules
			"MassCrowd",

			// Runtime/MassEntity Plugin Modules
			"MassEntity",

			// Runtime/MassGameplay Plugin Modules
			"MassSmartObjects",
            "SmartObjectsModule", // work around until we find a better way to get data from smart object configs
			"MassActors",
            "MassCommon",
            "MassGameplayDebug",
            "MassLOD",
            "MassMovement",
            "MassNavigation",
            "MassRepresentation",
            "MassReplication",
            "MassSpawner",
            "MassSimulation",
            "MassSignals",
			// Gameplay Ability System
            "GameplayAbilities"
		});

        if (Target.bBuildEditor == true)
        {
            PrivateDependencyModuleNames.Add("Blutility");
            PrivateDependencyModuleNames.Add("AssetManagerEditor");
        }

        // Uncomment if you are using Slate UI
        PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
