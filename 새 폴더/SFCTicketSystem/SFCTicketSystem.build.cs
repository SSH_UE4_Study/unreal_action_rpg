﻿using UnrealBuildTool;
 
public class SFCTicketSystem : ModuleRules
{
	public SFCTicketSystem(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		bUseUnity = false;

		PublicDependencyModuleNames.AddRange(new string[] { 
			"Core"
			, "CoreUObject"
			, "Engine"
			, "GameplayTags"
		});
 
		PublicIncludePaths.AddRange(new string[] {"SFCTicketSystem/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFCTicketSystem/Private"});
	}
}
