﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SFTicket.h"
#include "SFOperand.h"
#include "SFOperandPool.generated.h"

#define SFOPERANDPOOL_SIZE		512


// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFOperandPool : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SFCTICKETSYSTEM_API ISFOperandPool
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	// Get Type
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02 ISFOperandPool Interface")
	bool GetOperandType(FName operandName, ESFOpHandleType& opHndType);

	// Variant
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02 ISFOperandPool Interface")
	bool PushVariant(FName operandName, const FSFOpVariant& variant);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02 ISFOperandPool Interface")
	bool PeekVariant(FName operandName, FSFOpVariant& variant);

	/// <summary>
	/// 핸들 값에 의한 내부에서 오브젝트 선별 후 제거
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02 ISFOperandPool Interface")
	void ReleaseHandle(FName operandName);
};


UCLASS(Blueprintable)
class SFCTICKETSYSTEM_API USFOperandPoolComponent
	: public UActorComponent // UObject
	, public ISFOperandPool
{
	GENERATED_BODY()
protected:
	// https://docs.unrealengine.com/4.26/en-US/ProgrammingAndScripting/ProgrammingWithCPP/UnrealArchitecture/Objects/Optimizations/
	// 이거 믿고 T 컨테이너에 넣음


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFC-010.02 SFOperandPool|Data")
	TMap<FName, int>				_managedVariantPool;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFC-010.02 SFOperandPool|Data")
	TArray<FSFOpVariant>			_rawOpVariantPool;
	TQueue<int>						_idxQueue;

	TMap<FName, ESFOpHandleType>		_optypeMap;

public:
	USFOperandPoolComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void CreatePool();
	FORCEINLINE int GetEmptyHandle();
	FORCEINLINE void ReleaseHandle(int handle);

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Get Type
	virtual bool GetOperandType_Implementation(FName operandName, ESFOpHandleType& opHndType) override;

	// Variant
	virtual bool PushVariant_Implementation(FName operandName, const FSFOpVariant& variant) override;

	virtual bool PeekVariant_Implementation(FName operandName, FSFOpVariant& variant) override;

	/// <summary>
	/// 핸들 값에 의한 내부에서 오브젝트 선별 후 제거
	/// </summary>
	virtual void ReleaseHandle_Implementation(FName operandName) override;

};
