﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"

#include "SFTicket.h"
#include "SFOperand.h"
#include "SFOperandPool.h"

#include "SFTicketExecutor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFTicketExecutor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SFCTICKETSYSTEM_API ISFTicketExecutor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	// 실행기 이름 설정, 가져오기
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.03 ISFTicket Executor")
	void SetExecutorName(FName name);
	virtual void SetExecutorName_Implementation(FName name) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.03 ISFTicket Executor")
	void GetExecutorName(FName& name);
	virtual void GetExecutorName_Implementation(FName& name) { name = NAME_None; }

	// 실행기에 연결할 명령어(태그) 리스트 등록
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.03 ISFTicket Executor")
	void RegisterTickets(const TArray<FName>& events);
	virtual void RegisterTickets_Implementation(const TArray<FName>& events) {}

	// 실행기 명령어 연결 해제
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.03 ISFTicket Executor")
	void UnregisterTickets(const TArray<FName>& events);
	virtual void UnregisterTickets_Implementation(const TArray<FName>& events) {}

	// 등록된 태그 리스트
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.03 ISFTicket Executor")
	int GetRegisteredTickets(TArray<FName>& registerEvents);
	virtual int GetRegisteredTickets_Implementation(TArray<FName>& registerEvents) { return 0; }

	// 티겟 실 처리 부분
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.03 ISFTicket Executor")
	void ExecuteTicket(const TScriptInterface<ISFTicket>& iTicket);

	// 티켓 직접 실행 콜 부분
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.03 ISFTicket Executor")
	void ManualExecuteTicket(const FSFTicketStruct& manualTicket);
	virtual void ManualExecuteTicket_Implementation(const FSFTicketStruct& manualTicket) {}
};



DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDele_OnExecuteSFTicket, const TScriptInterface<ISFTicket>&, iTicket);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDele_OnExecuteSFTicketMng, const TScriptInterface<ISFTicket>&, iTicket, int, uid);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDele_OnExecuteSFTicketStruct, const FSFTicketStruct&, rawTicket);


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SFCTICKETSYSTEM_API USFTicketExecutorComponent
	: public UActorComponent
	, public ISFTicketExecutor
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ticket Executor")
	FName ExecutorName;


	UPROPERTY(Transient)
	TSet<FName>	_processableTicketNames;

	UPROPERTY(BlueprintAssignable, Category = "Ticket Executor")
	FDele_OnExecuteSFTicket Func_OnExecuteSFTicket;

	UPROPERTY(BlueprintAssignable, Category = "Ticket Executor")
	FDele_OnExecuteSFTicketStruct Func_OnExecuteSFTicketStruct;

public:
	USFTicketExecutorComponent();

protected:
	virtual void BeginPlay() override;

	void CallBPFunc(AActor* pOwner, FName funcName, const TScriptInterface<ISFTicket>& iTicket);

public:
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	// 실행기 이름 설정, 가져오기
	virtual void SetExecutorName_Implementation(FName name) override;

	virtual void GetExecutorName_Implementation(FName& name) override;

	// 실행기에 연결할 명령어(태그) 리스트 등록
	virtual void RegisterTickets_Implementation(const TArray<FName>& events) override;

	// 실행기 명령어 연결 해제
	virtual void UnregisterTickets_Implementation(const TArray<FName>& events) override;

	// 등록된 태그 리스트
	virtual int GetRegisteredTickets_Implementation(TArray<FName>& registerEvents) override;

	// 티겟 실 처리 부분
	virtual void ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket) override;

	virtual void ManualExecuteTicket_Implementation(const FSFTicketStruct& manualTicket) override;
};
