﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UObject/Interface.h"
#include "GameplayTagContainer.h"
#include "Engine/DataAsset.h"
#include "SFOperand.h"
#include "SFTicketLocalOperand.h"
#include "SFTicket.generated.h"


class USFTicketProcessorSubsystem;
// FSFOpVariant
class SFCTICKETSYSTEM_API ISFTicket;

struct FSFTicketStruct;

USTRUCT(Atomic, BlueprintType)
struct FSFTicketLocalOperand
{
	GENERATED_BODY()

public:
	// 발행 UID Size(20)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
	int	OwnerUID = 0;

	// 대상 UID Size(24)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
	int	TargetUID = 0;

	// int32 사용자 로컬 파라미터 1 (28 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
	int nParam1 = 0;

	// int32 사용자 로컬 파라미터 2 (32 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
	int nParam2 = 0;

	// 티겟 발행 위치, 시작 위치 또는 1st FVector 데이터로 이용 (56 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
	FVector vParam1 = FVector::ZeroVector;

	// 티겟 발행 시 방향 또는 2nd FVector 데이터로 이용 (80 by )
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
	FVector vParam2 = FVector::ZeroVector;

	// 티켓에서 사용할 글로벌 오퍼랜드 명 (96 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
	FName	OperandName = NAME_None;

	// 임시 오브젝트 (104 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
	TObjectPtr<UObject>	UserObject = nullptr;

	bool operator!=(const FSFTicketStruct& Other) const;
	//{
	//	return (
	//		OwnerUID != Other.OwnerUID ||
	//		TargetUID != Other.TargetUID ||
	//		nParam1 != Other.nParam1 ||
	//		nParam2 != Other.nParam2 ||
	//		vParam1 != Other.vParam1 ||
	//		vParam2 != Other.vParam2
	//		);
	//}
};


USTRUCT(Atomic, BlueprintType)
struct FSFTicketStruct
{
	GENERATED_BODY()

public:
	// FName Size:16(12)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	FName TicketName = NAME_None;

	// 발행 UID Size(20)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	int	OwnerUID = 0;

	// 대상 UID Size(24)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	int	TargetUID = 0;

	// int32 사용자 로컬 파라미터 1 (28 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	int nParam1 = 0;

	// int32 사용자 로컬 파라미터 2 (32 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	int nParam2 = 0;

	// 티겟 발행 위치, 시작 위치 또는 1st FVector 데이터로 이용 (56 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	FVector vParam1 = FVector::ZeroVector;

	// 티겟 발행 시 방향 또는 2nd FVector 데이터로 이용 (80 by )
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	FVector vParam2 = FVector::ZeroVector;

	// 티켓에서 사용할 글로벌 오퍼랜드 명 (96 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	FName	OperandName = NAME_None;

	// 임시 오브젝트 (104 by)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	TObjectPtr<UObject>	UserObject = nullptr;


	FSFTicketStruct& operator^=(const FSFTicketLocalOperand& Other);
	//{
	//	OwnerUID = (OwnerUID != Other.OwnerUID) ? Other.OwnerUID : OwnerUID;
	//	TargetUID = (TargetUID != Other.TargetUID) ? Other.TargetUID : TargetUID;
	//	nParam1 = ( nParam1 != Other.nParam1) ? Other.nParam1 : nParam1;
	//	nParam2 = ( nParam2 != Other.nParam2) ? Other.nParam2 : nParam2;
	//	vParam1 = ( vParam1 != Other.vParam1) ? Other.vParam1 : vParam1;
	//	vParam2 = ( vParam2 != Other.vParam2) ? Other.vParam2 : vParam2;

	//	return *this;
	//}

	//friend FSFTicketStruct operator^=(FSFTicketStruct& Src, const FSFTicketLocalOperand& Other);

	bool operator!=(const FSFTicketLocalOperand& Other) const;
	//{
	//	return (
	//		OwnerUID != Other.OwnerUID ||
	//		TargetUID != Other.TargetUID ||
	//		nParam1 != Other.nParam1 ||
	//		nParam2 != Other.nParam2 ||
	//		vParam1 != Other.vParam1 ||
	//		vParam2 != Other.vParam2
	//		);
	//}
};





// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFTicket : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SFCTICKETSYSTEM_API ISFTicket
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	// Generator에서 전달 받은 관리용 핸들.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void SetHandle(int64 Handle);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void GetHandle(int64& Handle);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void SetTicket(FSFTicketStruct ticket);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void GetTicket(FSFTicketStruct& ticket);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void ClearTicket();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	int GetOwnerUID();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	int GetTargetUID();

	/// <summary>
	/// Operand set / get
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void SetLocalOperand(const FSFOpVariant& opVariant);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void GetLocalOperand(FSFOpVariant& opVariant);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void PushGlobalOperandNames(UPARAM(ref)TArray<FName>& opNames);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void GetGlobalOperandNames(TArray<FName>& opNames);


	/// <summary>
	/// 티겟 회수 관련 플래그
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void SetDiscard();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void IsDiscard(bool& result);

	/// 확장.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	int GetRepeatCount();
	virtual int GetRepeatCount_Implementation() { return 1; }

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	float GetStartDelayTime();
	virtual float GetStartDelayTime_Implementation() { return 0.f; }

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void DecRepeatCount();
	virtual void DecRepeatCount_Implementation() {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01.01 ISFTicket Interface")
	void DecStartDelayTime(float DeltaTime);
	virtual void DecStartDelayTime_Implementation(float DeltaTime) {}

};


UCLASS(BlueprintType)
class SFCTICKETSYSTEM_API USFTicketAsset : public UDataAsset // public UObject
{
	GENERATED_BODY()
public:
	USFTicketAsset(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Asset")
	FSFTicketStruct TicketRAWData;

	// 오퍼랜드
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Asset")
	TMap<FName, FSFOpVariant> OperandData;
	//TArray<FSFOpVariant> OperandData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Asset")
	bool IsDirectTicket = false;
};

// 티켓을 실행할 실행기들 이름 모음
//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Asset")
//TArray<FName> ExecutorNames;


//
// 최소 단위 SF티켓 오브젝트
//
UCLASS(Blueprintable)
class SFCTICKETSYSTEM_API USFTicketObject : public UObject, public ISFTicket
{
	GENERATED_BODY()

	friend USFTicketProcessorSubsystem;

	bool _discard_flag = false;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	int64 _managedHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	FSFTicketStruct _ticketRAW;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	FSFOpVariant _localOpVariant;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	TArray<FName> _operandNamePool;

//public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	int RepeatCount = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket Struct")
	float StartDelayTime = 0.f;

public:

	virtual void BeginDestroy() override;

	virtual void SetHandle_Implementation(int64 Handle) override;
	virtual void GetHandle_Implementation(int64& Handle) override;
	virtual void SetTicket_Implementation(FSFTicketStruct ticket) override;
	virtual void GetTicket_Implementation(FSFTicketStruct& ticket) override;
	virtual void ClearTicket_Implementation() override;

	virtual int GetOwnerUID_Implementation() override;
	virtual int GetTargetUID_Implementation() override;

	/// <summary>
	/// Operand set / get
	/// </summary>
	virtual void SetLocalOperand_Implementation(const FSFOpVariant& opVariant) override;
	virtual void GetLocalOperand_Implementation(FSFOpVariant& opVariant) override;
	virtual void PushGlobalOperandNames_Implementation(UPARAM(ref)TArray<FName>& names) override;
	virtual void GetGlobalOperandNames_Implementation(TArray<FName>& opNames) override;
	virtual void SetDiscard_Implementation() override;
	virtual void IsDiscard_Implementation(bool& result) override;

	virtual int GetRepeatCount_Implementation() override;
	virtual float GetStartDelayTime_Implementation() override;
	virtual void DecRepeatCount_Implementation() override;
	virtual void DecStartDelayTime_Implementation(float DeltaTime) override;
};
