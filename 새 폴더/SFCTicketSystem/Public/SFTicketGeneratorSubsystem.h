﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"

#include "SFTicket.h"
#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFTicketExecutor.h"
#include "SFTicketGenerator.h"
#include "SFTicketProcessor.h"

#include "SFTicketGeneratorSubsystem.generated.h"

/**
 * 
 */
UCLASS(DisplayName = "SFC-010.01 SFTicketGenerator")
class SFCTICKETSYSTEM_API USFTicketGeneratorSubsystem 
	: public UGameInstanceSubsystem
	, public ISFTicketGenerator
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	int64										_HandleCnt = 1;

	TSet<TScriptInterface<ISFTicket>>			_RAWTicketObjectSet;

	UPROPERTY()
	TMap<int64, TScriptInterface<ISFTicket>>	_RAWTicketObjectMap;

	UPROPERTY()
	TArray<TScriptInterface<ISFTicket>>			_TicketObjectArray;

	TQueue<int>									_IdxQueue;


public:

	// Begin
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01 ISFTicket Generator")
	//void CreatePool();
	virtual void CreatePool_Implementation() override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Generator")
	//void ReleasePool();
	virtual void ReleasePool_Implementation() override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Generator")
	//bool GetNewTicket(TScriptInterface<ISFTicket>& newTicket, int64& handle);
	virtual bool GetNewTicket_Implementation(TScriptInterface<ISFTicket>& newTicket, int64& handle) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Generator")
	//void ReleaseTicket(const TScriptInterface<ISFTicket>& ticket);
	virtual void ReleaseTicket_Implementation(const TScriptInterface<ISFTicket>& ticket) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Generator")
	//void ReleaseTicketByHandle(int64 handle);
	virtual void ReleaseTicketByHandle_Implementation(int64 handle) override;
};
