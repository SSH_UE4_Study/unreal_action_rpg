﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"

#include "SFTicket.h"
#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFTicketExecutor.h"
#include "SFTicketGenerator.h"
#include "SFTicketProcessor.h"


#include "SFTicketProcessorSubsystem.generated.h"

/**
 * 
 */
UCLASS(DisplayName = "SFC-010.04 SFTicketProcessor")
class SFCTICKETSYSTEM_API USFTicketProcessorSubsystem 
	: public UGameInstanceSubsystem
	, public FTickableGameObject
	, public ISFTicketProcessor
{
	GENERATED_BODY()

	TQueue<TScriptInterface<ISFTicket>> _activeTicketQueue;

	uint64 LastFrameNumberWeTicked = (uint64)-1;

protected:

	/// <summary>
	/// 여러 티켓 테이블들 등록할 수 있도록 Array 로 설정
	/// </summary>
	//UPROPERTY()
	//TArray<UDataTable*> TicketInfoDataTables;

	UPROPERTY()
	TScriptInterface<ISFTicketGenerator>	LinkedTicketGenerator;

	UPROPERTY()
	TScriptInterface<ISFOperandPool>		LinkedOperandPool;

	//UPROPERTY(Transient, BlueprintReadOnly, Category = "ISFTicket Process|Properties")
	UPROPERTY()
	TMap<FName, TScriptInterface<ISFTicketExecutor>> _ticketExecutorMap;

	TMap<FName, TSet<FName>> _executorFinder;

	UFUNCTION(BlueprintCallable, Category = "SFC-010.04 SFCTicketProcessor")
	void InitTicketInfoTable(UPARAM(ref)TArray<UDataTable*> DataTables);

	template<typename AllocatorType>
	void InitTicketInfoTableRAW(TArray<UDataTable*, AllocatorType>& DataTables);

	void ExecuteSingleTicket(const TScriptInterface<ISFTicket>& CurTicket, bool* pIsRepeatCurTicket);

public:

	// Begin
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	void LoadTicketInfoTables();

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FuncSFPSPostTickEvent, const TScriptInterface<ISFTicket>&, currentTicket, int64, FrameNumber );

	UPROPERTY(BlueprintAssignable, Category = "SFC-010.04 SFCTicketProcessor")
	FuncSFPSPostTickEvent OnPostTickEvent;

	virtual void Tick(float DeltaTime) override;

	virtual ETickableTickType GetTickableTickType() const override
	{
		return ETickableTickType::Always;
	}

	virtual TStatId GetStatId() const override
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FSFCTicketSystem, STATGROUP_Tickables);
	}

	virtual bool IsTickableWhenPaused() const override
	{
		return true;
	}


	// 티겟 명령 실행기 등록
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void RegisterExecutorWithName(FName executorName, const TScriptInterface<ISFTicketExecutor>& iExecutor);
	virtual void RegisterExecutorWithName_Implementation(FName executorName, const TScriptInterface<ISFTicketExecutor>& iExecutor) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void RegisterExecutor(const TScriptInterface<ISFTicketExecutor>& iExecutor);
	virtual void RegisterExecutor_Implementation(const TScriptInterface<ISFTicketExecutor>& iExecutor) override;

	// 프로세스에서 실행기 해제
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void UnregisterExecutor(FName executorName);
	virtual void UnregisterExecutor_Implementation(FName executorName) override;

	// executor의 delayed registered GameplayTag 갱신하기
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void UpdateExecutor(FName executorName);
	virtual void UpdateExecutor_Implementation(FName executorName) override;

	// 이름으로 등록된 executor 얻기
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//bool GetExecutor(FName executorName, TScriptInterface<ISFTicketExecutor>& iExecutor);
	virtual bool GetExecutor_Implementation(FName executorName, TScriptInterface<ISFTicketExecutor>& iExecutor) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void ProcessTickets();
	virtual void ProcessTickets_Implementation(float DeltaTime) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void DispatchTicket(const TScriptInterface<ISFTicket>& iTicket, bool IsDirect);
	virtual void DispatchTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket, bool IsDirect) override;

	// 이름 변경 : SetForAutoReleaseGenerator -> SetTicketGenerator
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void SetTicketGenerator(const TScriptInterface<ISFTicketGenerator>& iGenerator);
	virtual void SetTicketGenerator_Implementation(const TScriptInterface<ISFTicketGenerator>& iGenerator) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void SetOperandPool(const TScriptInterface<ISFOperandPool>& iOperandPool);
	virtual void SetOperandPool_Implementation(const TScriptInterface<ISFOperandPool>& iOperandPool) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void DispatchRAWTicket(UPARAM(ref)FSFTicketStruct& rawSFTicket, bool IsDirect);
	virtual void DispatchRAWTicket_Implementation(UPARAM(ref)FSFTicketStruct& rawSFTicket, bool IsDirect) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	//void DispatchTicketAsset(const USFTicketAsset* ticketAsset, FSFTicketLocalOperand localOperand);
	virtual void DispatchTicketAsset_Implementation(const USFTicketAsset* ticketAsset, FSFTicketLocalOperand localOperand, bool UseLocalOperand) override;
	

	UFUNCTION(BlueprintCallable, Category = "SFC-010.04 SFCTicketProcessor")
	void DispatchTicketEx(const TScriptInterface<ISFTicket>& iTicket, bool IsDirect, int Repeat, float delayTime);
};
