﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Containers/Array.h"
#include "Containers/Queue.h"
#include "SFTicket.h"
#include "SFOperandPool.h"
#include "SFTicketGenerator.generated.h"


#define SFPOOL_MAX		128


// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFTicketGenerator : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SFCTICKETSYSTEM_API ISFTicketGenerator
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01 ISFTicketGenerator Interface")
	void CreatePool();
	// virtual void CreatePool_Implementation() = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01 ISFTicketGenerator Interface")
	void ReleasePool();
	// virtual void ReleasePool_Implementation() = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01 ISFTicketGenerator Interface")
	bool GetNewTicket(TScriptInterface<ISFTicket>& newTicket, int64& handle);
	// virtual bool GetNewTicket_Implementation(TScriptInterface<ISFTicket>& newTicket, int64& handle) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01 ISFTicketGenerator Interface")
	void ReleaseTicket(const TScriptInterface<ISFTicket>& ticket);
	// virtual void ReleaseTicket_Implementation(const TScriptInterface<ISFTicket>& ticket) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.01 ISFTicketGenerator Interface")
	void ReleaseTicketByHandle(int64 handle);
	// virtual void ReleaseTicketByHandle_Implementation(int64 handle) = 0;
};


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SFCTICKETSYSTEM_API USFTicketGeneratorComponent
	: public UActorComponent
	, public ISFTicketGenerator
{
	GENERATED_BODY()

protected:

	UPROPERTY(Transient)
	int64										_HandleCnt = 1;

	TSet<TScriptInterface<ISFTicket>>			_RAWTicketObjectSet;

	UPROPERTY(Transient)
	TMap<int64, TScriptInterface<ISFTicket>>	_RAWTicketObjectMap;

	UPROPERTY(Transient)
	TArray<TScriptInterface<ISFTicket>>			_TicketObjectArray;

	TQueue<int>									_IdxQueue;

public:
	// Sets default values for this component's properties
	USFTicketGeneratorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void InitializeComponent() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


public:
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicketGenerator Interface")
	//void CreatePool();
	virtual void CreatePool_Implementation() override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicketGenerator Interface")
	//void ReleasePool();
	virtual void ReleasePool_Implementation() override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicketGenerator Interface")
	//bool GetNewTicket(TScriptInterface<ISFTicket>& newTicket, int64& handle);
	virtual bool GetNewTicket_Implementation(TScriptInterface<ISFTicket>& newTicket, int64& handle) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicketGenerator Interface")
	//void ReleaseTicket(const TScriptInterface<ISFTicket>& ticket);
	virtual void ReleaseTicket_Implementation(const TScriptInterface<ISFTicket>& ticket) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicketGenerator Interface")
	//void ReleaseTicketByHandle(int64 handle);
	virtual void ReleaseTicketByHandle_Implementation(int64 handle) override;
};
