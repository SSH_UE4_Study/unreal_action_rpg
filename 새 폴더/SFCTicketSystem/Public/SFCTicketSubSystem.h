﻿#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"

#include "SFTicket.h"
#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFTicketExecutor.h"
#include "SFTicketGenerator.h"
#include "SFTicketProcessor.h"

#include "SFCTicketSubSystem.generated.h"

UCLASS(DisplayName = "SFC-010 SFCTicketSystem")
class USFCTicketSubsystem 
	: public UGameInstanceSubsystem
	//, public FTickableGameObject
	//, public ISFTicketProcessor
{
	GENERATED_BODY()

protected:

public:
	
	// Begin
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	//virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, Category = "SFC-010 SFCTicketSystem", meta = (DisplayName = "Get TicketGenterator Interface"))
	bool GetTicketGeneratorInterface(TScriptInterface<ISFTicketGenerator>& iGenerator);

	UFUNCTION(BlueprintPure, Category = "SFC-010 SFCTicketSystem", meta = (DisplayName = "Get OperandPool Interface"))
	bool GetOperandPoolInterface(TScriptInterface<ISFOperandPool>& iOperandPool);

	UFUNCTION(BlueprintPure, Category = "SFC-010 SFCTicketSystem", meta = (DisplayName = "Get TicketProcessor Interface"))
	bool GetTicketProcessorInterface(TScriptInterface<ISFTicketProcessor>& iTicketProcessor);
};
