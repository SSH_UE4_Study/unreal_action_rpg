﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "SFOperand.generated.h"


UENUM(BlueprintType)
enum class ESFOpHandleType : uint8
{
	EOHT_None = 0	UMETA(DisplayName = "ESFOHT_None"),
	EOHT_String = 1 UMETA(DisplayName = "ESFOHT_String"),
	EOHT_Object = 2 UMETA(DisplayName = "ESFOHT_Object"),
	EOHT_Vector = 3 UMETA(DisplayName = "ESFOHT_Vector"),
	EOHT_IntVector = 4 UMETA(DisplayName = "ESFOHT_IntVector"),
	EOHT_Variant = 5 UMETA(DisplayName = "ESFOHT_Variant"),
};

USTRUCT(BlueprintType)
struct FSFOpVariant
{
	GENERATED_BODY()

public:
	// Variant 외 타입은 싱글 타입으로 사용할 때 마킹
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Operand Variant")
	ESFOpHandleType operandType = ESFOpHandleType::EOHT_None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFOperand Variant")
	FString strData = "";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFOperand Variant")
	TObjectPtr<UObject>	objectData = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFOperand Variant")
	FVector	vecData = FVector::ZeroVector;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFOperand Variant")
	FIntVector ivecData = FIntVector::ZeroValue;

	FString ToString() const
	{
		FString out;
		out.Append(TEXT("opType:"));
		switch (operandType)
		{
		case ESFOpHandleType::EOHT_None:
			out.Append(TEXT("EOHT_None"));
			break;
		case ESFOpHandleType::EOHT_String:
			out.Append(TEXT("EOHT_String"));
			out.Append(TEXT(" StrData:'"));
			out.Append(strData);
			out.Append(TEXT("'"));
			break;
		case ESFOpHandleType::EOHT_Object:
			out.Append(TEXT("EOHT_Object"));
			out.Append(TEXT(" ObjType:("));
			out.Append( (objectData == nullptr ) ? TEXT("nullptr") 
				: objectData->GetClass()->GetFName().ToString() );
			out.Append(TEXT(")"));
			break;
		case ESFOpHandleType::EOHT_Vector:
			out.Append(TEXT("EOHT_Vector"));
			out.Append(TEXT(" Vector:("));
			out.Append( vecData.ToString());
			out.Append(TEXT(")"));
			break;
		case ESFOpHandleType::EOHT_IntVector:
			out.Append(TEXT("EOHT_IntVector"));
			out.Append(TEXT(" IntVector:("));
			out.Append(ivecData.ToString());
			out.Append(TEXT(")"));
			break;
		case ESFOpHandleType::EOHT_Variant:
			out.Append(TEXT("EOHT_Variant"));

			out.Append(TEXT(" StrData:'"));
			out.Append(strData);
			out.Append(TEXT("',"));

			out.Append(TEXT(" ObjType:("));
			out.Append((objectData == nullptr) ? TEXT("nullptr")
					   : objectData->GetClass()->GetFName().ToString());
			out.Append(TEXT("),"));

			out.Append(TEXT(" Vector:("));
			out.Append(vecData.ToString());
			out.Append(TEXT("),"));

			out.Append(TEXT(" IntVector:("));
			out.Append(ivecData.ToString());
			out.Append(TEXT(")"));
			break;
		}
		
		return out;
	}
};


// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFOperand : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SFCTICKETSYSTEM_API ISFOperand
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02.01 ISFOperand")
	void SetOperandName(FName operandName);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02.01 ISFOperand")
	void GetOperandName(FName& operandName);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02.01 ISFOperand")
	void GetOperandType(ESFOpHandleType& operandType);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02.01 ISFOperand")
	void SetVariant(UPARAM(ref)FSFOpVariant& variant);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02.01 ISFOperand")
	void GetVariant(FSFOpVariant& variant);
};


UCLASS(Blueprintable)
class SFCTICKETSYSTEM_API USFOperandObject : public UObject, public ISFOperand
{
	GENERATED_BODY()
protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFC-010.02.01 ISFOperand|Data")
	FName _operandName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFC-010.02.01 ISFOperand|Data")
	ESFOpHandleType _opType = ESFOpHandleType::EOHT_None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFC-010.02.01 ISFOperand|Data")
	FSFOpVariant _opData;

public:

	virtual void SetOperandName_Implementation(FName operandName) override;
	virtual void GetOperandName_Implementation(FName& operandName) override;
	virtual void GetOperandType_Implementation(ESFOpHandleType& operandType) override;
	virtual void SetVariant_Implementation(UPARAM(ref)FSFOpVariant& variant) override;
	virtual void GetVariant_Implementation(FSFOpVariant& variant) override;
};

UCLASS()
class SFCTICKETSYSTEM_API USFOpVariantData : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	// Settters
	UFUNCTION(BlueprintCallable, BlueprintInternalUseOnly)
	static bool SetStringByName(UObject* Target, FName VarName, FString NewValue, FString& OutValue);

	//UFUNCTION(BlueprintCallable, BlueprintInternalUseOnly)
	UFUNCTION(BlueprintCallable, Category="SFOpVariant Library")
	static bool SetVectorByName(UObject* Target, FName VarName, FVector NewValue, FVector& OutValue);

	UFUNCTION(BlueprintCallable, BlueprintInternalUseOnly)
	static bool SetFloatByName(UObject* Target, FName VarName, float NewValue, float& OutValue);

	// Getters
	UFUNCTION(BlueprintCallable, BlueprintInternalUseOnly)
	static bool GetStringByName(UObject* Target, FName VarName, FString& OutValue);

	UFUNCTION(BlueprintCallable, BlueprintInternalUseOnly)
	static bool GetVectorByName(UObject* Target, FName VarName, FVector& OutValue);

	UFUNCTION(BlueprintCallable, BlueprintInternalUseOnly)
	static bool GetFloatByName(UObject* Target, FName VarName, float& OutValue);
};
