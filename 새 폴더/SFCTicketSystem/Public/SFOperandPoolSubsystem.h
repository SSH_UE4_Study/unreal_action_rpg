﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"

#include "SFTicket.h"
#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFTicketExecutor.h"
#include "SFTicketGenerator.h"
#include "SFTicketProcessor.h"

#include "SFOperandPoolSubsystem.generated.h"

/**
 * 
 */
UCLASS(DisplayName = "SFC-010.2 SFOperandPool")
class SFCTICKETSYSTEM_API USFOperandPoolSubsystem 
	: public UGameInstanceSubsystem
	, public ISFOperandPool
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	TMap<FName, int>				_managedVariantPool;

	UPROPERTY()
	TArray<FSFOpVariant>			_rawOpVariantPool;
	TQueue<int>						_idxQueue;

	TMap<FName, ESFOpHandleType>		_optypeMap;

	void CreatePool();
	FORCEINLINE int GetEmptyHandle();
	FORCEINLINE void ReleaseHandle(int handle);

public:
	// Begin
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	// Get Type
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02 ISFOperand Pool")
	//bool GetOperandType(FName operandName, ESFOpHandleType& opHndType);
	virtual bool GetOperandType_Implementation(FName operandName, ESFOpHandleType& opHndType) override;

	// Variant
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02 ISFOperand Pool")
	//bool PushVariant(FName operandName, UPARAM(ref)FSFOpVariant& variant);
	virtual bool PushVariant_Implementation(FName operandName, const FSFOpVariant& variant) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02 ISFOperand Pool")
	//bool PeekVariant(FName operandName, FSFOpVariant& variant);
	virtual bool PeekVariant_Implementation(FName operandName, FSFOpVariant& variant) override;

	/// <summary>
	/// 핸들 값에 의한 내부에서 오브젝트 선별 후 제거
	/// </summary>
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.02 ISFOperand Pool")
	//void ReleaseHandle(FName operandName);
	virtual void ReleaseHandle_Implementation(FName operandName) override;

	void ShowGOPPool();

	UFUNCTION(BlueprintCallable, Category = "GC Test")
	void CreateTicketObjectAndRegister();

	UFUNCTION(BlueprintCallable, Category = "GC Test")
	void ForceGC();
	
	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool")
	bool PushVector(FName operandName, const FVector& vecData);

	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool")
	bool PushString(FName operandName, const FString& strData);

	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool")
	bool PushObject(FName operandName, UObject* objData);

	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool")
	bool PeekVector(FName operandName, FVector& retVector);

	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool")
	bool PeekString(FName operandName, FString& retString);

	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool")
	bool PeekObject(FName operandName, UObject*& retObject);

	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool", meta = (ToolTip = "PropertyName : 접근할 오브젝트의 프로터티 명"))
	bool PushVectorToObject(FName operandName, FName propertyName, const FVector& vecData);

	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool", meta = (ToolTip = "PropertyName : 접근할 오브젝트의 프로터티 명"))
	bool PeekVectorFromObject(FName operandName, FName propertyName, FVector& retVector);

	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool", meta = (ToolTip = "PropertyName : 접근할 오브젝트의 프로터티 명"))
	bool PushStringToObject(FName operandName, FName propertyName, const FString& strData);

	UFUNCTION(BlueprintCallable, Category = "SFC-010.2 SFOperandPool", meta = (ToolTip = "PropertyName : 접근할 오브젝트의 프로터티 명"))
	bool PeekStringFromObject(FName operandName, FName propertyName, FString& retString);
	
};
