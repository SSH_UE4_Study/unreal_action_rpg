﻿#pragma once

#include "CoreMinimal.h"
#include "SFTicket.h"
//#include "SFTicketLocalOperand.generated.h"

//USTRUCT(Atomic, BlueprintType)
//struct FSFTicketLocalOperand
//{
//	GENERATED_BODY()
//
//public:
//	// 발행 UID Size(20)
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
//		int	OwnerUID;
//
//	// 대상 UID Size(24)
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
//		int	TargetUID;
//
//	// int32 사용자 로컬 파라미터 1 (28 by)
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
//		int nParam1;
//
//	// int32 사용자 로컬 파라미터 2 (32 by)
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
//		int nParam2;
//
//	// 티겟 발행 위치, 시작 위치 또는 1st FVector 데이터로 이용 (56 by)
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
//		FVector vParam1;
//
//	// 티겟 발행 시 방향 또는 2nd FVector 데이터로 이용 (80 by )
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicket LocalOperand")
//		FVector vParam2;
//
//	bool operator!=(const FSFTicketStruct& Other) const
//	{
//		return (
//			OwnerUID != Other.OwnerUID ||
//			TargetUID != Other.TargetUID ||
//			nParam1 != Other.nParam1 ||
//			nParam2 != Other.nParam2 ||
//			vParam1 != Other.vParam1 ||
//			vParam2 != Other.vParam2
//			);
//	}
//};
