﻿#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSFCTicketSystem, Log, All);

#define USE_SF_LOG		0

#ifdef SF_LOG
	#undef SF_LOG
#endif

#define SF_LOG_CALLINFO (FString(__FILE__)+TEXT(" -> ") + FString(__FUNCTION__)+TEXT("(")+ FString::FromInt(__LINE__) + TEXT(")"))

#define SF_LOG_FILE (FString(__FILE__))
#define SF_LOG_FUNC (FString(__FUNCTION__))
#define SF_LOG_LINE (FString::FromInt(__LINE__))

#if USE_SF_LOG == 1
#if WITH_EDITOR && PLATFORM_WINDOWS
#define SF_LOG(Verbosity,Format,...)                   UE_LOG(LogSFCTicketSystem,Verbosity,TEXT("[%s (%s)] %s"), *SF_LOG_FUNC, *SF_LOG_LINE, *FString::Printf(Format,##__VA_ARGS__)); if (false) _stprintf_s(nullptr, 0, Format, ##__VA_ARGS__)
#define SF_LOG_EX(Verbosity,func_name,line,Format,...) UE_LOG(LogSFCTicketSystem,Verbosity,TEXT("[%s (%d)] %s"), *FString(func_name), line, *FString::Printf(Format,##__VA_ARGS__));  if (false) _stprintf_s(nullptr, 0, Format, ##__VA_ARGS__)
#else
#define SF_LOG(Verbosity,Format,...)                   UE_LOG(LogSFCTicketSystem,Verbosity,TEXT("[%s (%s)] %s"), *SF_LOG_FUNC, *SF_LOG_LINE, *FString::Printf(Format,##__VA_ARGS__))
#define SF_LOG_EX(Verbosity,func_name,line,Format,...) UE_LOG(LogSFCTicketSystem,Verbosity,TEXT("[%s (%d)] %s"), *FString(func_name), line, *FString::Printf(Format,##__VA_ARGS__))
#endif
#else
#define SF_LOG(Verbosity,Format,...)
#define SF_LOG_EX(Verbosity,func_name,line,Format,...)
#endif


#if !WITH_EDITOR
#define SF_ELOG(Verbosity,Format,...) ; 
#else
#define SF_ELOG SF_LOG
#endif


class ISFCTicketSystemModule : public IModuleInterface
{
public:

	static inline ISFCTicketSystemModule& Get()
	{
		QUICK_SCOPE_CYCLE_COUNTER(STAT_ISFCTicketSystemModule_Get);
		static ISFCTicketSystemModule& Singleton = 
			FModuleManager::LoadModuleChecked<ISFCTicketSystemModule>(
				FName(TEXT("SFCTicketSystem")));
		return Singleton;
	}

	static inline bool IsAvailable()
	{
		QUICK_SCOPE_CYCLE_COUNTER(STAT_ISFCTicketSystemModule_IsAvailable);
		return FModuleManager::Get().IsModuleLoaded("SFCTicketSystem");
	}

	//virtual 
};

class FSFCTicketSystem : public IModuleInterface
{
public:

	IConsoleCommand* ShowGOPCommand;

	/* Called when the module is loaded */
	virtual void StartupModule() override;

	/* Called when the module is unloaded */
	virtual void ShutdownModule() override;

	void ShowGOPCmd(FString args);
};

