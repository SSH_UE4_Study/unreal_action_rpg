﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"

#include "SFTicket.h"
#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFTicketExecutor.h"
#include "SFTicketGenerator.h"

#include "SFTicketProcessor.generated.h"

USTRUCT(BlueprintType)
struct FSFTicketInfoTable : public FTableRowBase
{
	GENERATED_BODY()
public:
	// 티켓 명
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicketInfo Table")
	FName TicketName;

	// 등록할 실행기 이름 목록
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicketInfo Table")
	TArray<FName> ExecutorNames;

	// 티겟에 대한 설명
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFTicketInfo Table")
	FString Desc;
};



// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFTicketProcessor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SFCTICKETSYSTEM_API ISFTicketProcessor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	// 티겟 명령 실행기 등록
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void RegisterExecutorWithName(FName executorName, const TScriptInterface<ISFTicketExecutor>& iExecutor);
	//virtual void RegisterExecutorWithName_Implementation(FName executorName, const TScriptInterface<ISFTicketExecutor>& iExecutor) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void RegisterExecutor(const TScriptInterface<ISFTicketExecutor>& iExecutor);
	//virtual void RegisterExecutor_Implementation(const TScriptInterface<ISFTicketExecutor>& iExecutor) = 0;

	// 프로세스에서 실행기 해제
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void UnregisterExecutor(FName executorName);
	//virtual void UnregisterExecutor_Implementation(FName executorName) = 0;

	// executor의 delayed registered GameplayTag 갱신하기
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void UpdateExecutor(FName executorName);
	//virtual void UpdateExecutor_Implementation(FName executorName) = 0;

	// 이름으로 등록된 executor 얻기
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	bool GetExecutor(FName executorName, TScriptInterface<ISFTicketExecutor>& iExecutor);
	//virtual bool GetExecutor_Implementation(FName executorName, TScriptInterface<ISFTicketExecutor>& iExecutor) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void ProcessTickets(float DeltaTime);
	//virtual void ProcessTickets_Implementation() = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void DispatchTicket(const TScriptInterface<ISFTicket>& iTicket, bool IsDirect);
	//virtual void DispatchTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket, bool IsDirect) = 0;

	// SetForAutoReleaseGenerator
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void SetTicketGenerator(const TScriptInterface<ISFTicketGenerator>& iGenerator);
	//virtual void SetTicketGenerator_Implementation(const TScriptInterface<ISFTicketGenerator>& iGenerator) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void SetOperandPool(const TScriptInterface<ISFOperandPool>& iOperandPool);
	//virtual void SetOperandPool_Implementation(const TScriptInterface<ISFOperandPool>& iOperandPool) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void DispatchRAWTicket(UPARAM(ref)FSFTicketStruct& rawSFTicket, bool IsDirect);
	//virtual void DispatchRAWTicket_Implementation(UPARAM(ref)FSFTicketStruct& rawSFTicket, bool IsDirect) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-010.04 ISFTicket Process")
	void DispatchTicketAsset(const USFTicketAsset* ticketAsset, FSFTicketLocalOperand localOperand, bool UseLocalOperand);
	//virtual void DispatchTicketAsset_Implementation(const USFTicketAsset* ticketAsset, FSFTicketLocalOperand localOperand, bool UseLocalOperand) = 0;
};


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SFCTICKETSYSTEM_API USFTicketProcessorComponent
	: public UActorComponent
	, public ISFTicketProcessor
{
	GENERATED_BODY()

	TQueue<TScriptInterface<ISFTicket>> _activeTicketQueue;

protected:

	/// <summary>
	/// 여러 티켓 테이블들 등록할 수 있도록 Array 로 설정
	/// </summary>
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ISFTicket Process|Properties")
	TArray<UDataTable*> TicketInfoDataTables;

	//UPROPERTY(Transient, BlueprintReadOnly, Category = "ISFTicket Process|Properties")
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ISFTicket Process|Properties")
	TScriptInterface<ISFTicketGenerator>	LinkedTicketGenerator;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ISFTicket Process|Properties")
	TScriptInterface<ISFOperandPool>		LinkedOperandPool;

	//UPROPERTY(Transient, BlueprintReadOnly, Category = "ISFTicket Process|Properties")
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ISFTicket Process|Properties")
	TMap<FName, TScriptInterface<ISFTicketExecutor>> _ticketExecutorMap;

	TMap<FName, TSet<FName>> _executorFinder;

public:
	USFTicketProcessorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void InitializeComponent() override;

	void InitTicketInfoTable();
	void ExecuteSingleTicket(const TScriptInterface<ISFTicket>& CurTicket);

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	// 티겟 명령 실행기 등록
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void RegisterExecutorWithName(FName executorName, const TScriptInterface<ISFTicketExecutor>& iExecutor);
	virtual void RegisterExecutorWithName_Implementation(FName executorName, const TScriptInterface<ISFTicketExecutor>& iExecutor) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void RegisterExecutor(const TScriptInterface<ISFTicketExecutor>& iExecutor);
	virtual void RegisterExecutor_Implementation(const TScriptInterface<ISFTicketExecutor>& iExecutor) override;

	// 프로세스에서 실행기 해제
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void UnregisterExecutor(FName executorName);
	virtual void UnregisterExecutor_Implementation(FName executorName) override;

	// executor의 delayed registered GameplayTag 갱신하기
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void UpdateExecutor(FName executorName);
	virtual void UpdateExecutor_Implementation(FName executorName) override;

	// 이름으로 등록된 executor 얻기
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//bool GetExecutor(FName executorName, TScriptInterface<ISFTicketExecutor>& iExecutor);
	virtual bool GetExecutor_Implementation(FName executorName, TScriptInterface<ISFTicketExecutor>& iExecutor) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void ProcessTickets();
	virtual void ProcessTickets_Implementation(float DeltaTime) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void DispatchTicket(const TScriptInterface<ISFTicket>& iTicket, bool IsDirect);
	virtual void DispatchTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket, bool IsDirect) override;

	// 이름 변경 : SetForAutoReleaseGenerator -> SetTicketGenerator
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void SetTicketGenerator(const TScriptInterface<ISFTicketGenerator>& iGenerator);
	virtual void SetTicketGenerator_Implementation(const TScriptInterface<ISFTicketGenerator>& iGenerator) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void SetOperandPool(const TScriptInterface<ISFOperandPool>& iOperandPool);
	virtual void SetOperandPool_Implementation(const TScriptInterface<ISFOperandPool>& iOperandPool) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void DispatchRAWTicket(UPARAM(ref)FSFTicketStruct& rawSFTicket, bool IsDirect);
	virtual void DispatchRAWTicket_Implementation(UPARAM(ref)FSFTicketStruct& rawSFTicket, bool IsDirect) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFTicket Process")
	//void DispatchTicketAsset(const USFTicketAsset* ticketAsset, FSFTicketLocalOperand localOperand);
	virtual void DispatchTicketAsset_Implementation(const USFTicketAsset* ticketAsset, FSFTicketLocalOperand localOperand, bool UseLocalOperand) override;
};
