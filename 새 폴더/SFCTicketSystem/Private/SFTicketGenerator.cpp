﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SFTicketGenerator.h"
#include "SFCTicketSystem.h"
#include "SFTicket.h"
#include "SFOperandPool.h"


// Add default functionality here for any ISFTicketGenerator functions that are not pure virtual.
USFTicketGeneratorComponent::USFTicketGeneratorComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
}

void USFTicketGeneratorComponent::BeginPlay()
{
	Super::BeginPlay();
	SF_LOG(Warning, TEXT("SFTicketGenerator Com BeginPlay"));
}

void USFTicketGeneratorComponent::InitializeComponent()
{
	Super::InitializeComponent();
	CreatePool_Implementation();
}

void USFTicketGeneratorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void USFTicketGeneratorComponent::CreatePool_Implementation()
{
	//FSFTicketStruct _empty;
	//_rawDataPool.Init(_empty, SFPOOL_MAX);

	//for(int i = 0 ; i < SFPOOL_MAX ; ++i)
	//	_idxQueue.Enqueue(i);

	SF_LOG(Warning, TEXT("SFTicket Create Pool (%d)"), SFPOOL_MAX);

	for (int i = 0; i < SFPOOL_MAX; ++i)
	{
		_TicketObjectArray.Add(NewObject<USFTicketObject>());
		_IdxQueue.Enqueue(i);
	}

	SF_LOG(Warning, TEXT("SFTicketStruct size : (%zd)/int(%zd),FNameSize(%zd),FVectorSize(%zd)")
		,sizeof(FSFTicketStruct)
		,sizeof(int)
		,sizeof(FName)
		,sizeof(FVector));

}

void USFTicketGeneratorComponent::ReleasePool_Implementation()
{
	_IdxQueue.Empty();
	_TicketObjectArray.Empty();
}

bool USFTicketGeneratorComponent::GetNewTicket_Implementation(TScriptInterface<ISFTicket>& newTicket, int64& handle)
{
	//_RAWTicketObjectSet
	if (!_IdxQueue.IsEmpty())
	{
		int curIdx = -1;
		_IdxQueue.Peek(curIdx);

		/// 새 큐 얻기
		newTicket = _TicketObjectArray[curIdx];
		handle = curIdx;
		// 핸들 입력
		ISFTicket::Execute_SetHandle(newTicket.GetObject(), (int64)curIdx);

		SF_LOG(Warning, TEXT("SF티겟(Hnd:%d) 생성됨"), curIdx);

		_IdxQueue.Pop();

		return true;
	}


	return false;
}


void USFTicketGeneratorComponent::ReleaseTicket_Implementation(const TScriptInterface<ISFTicket>& ticket)
{
	int64 _ticketHandle = -1;
	bool _isDiscard = false;

	if (ticket)
	{
		ISFTicket::Execute_IsDiscard(ticket.GetObject(), _isDiscard);
		ISFTicket::Execute_GetHandle(ticket.GetObject(), _ticketHandle);

		if( _isDiscard && _ticketHandle != -1 )
		{
			ISFTicket::Execute_ClearTicket(ticket.GetObject());

			// 핸들만 회수, 오브젝트는 리셋
			_IdxQueue.Enqueue((int)_ticketHandle);

			SF_LOG(Warning, TEXT("생성된 SF티겟(Hnd:%d) 회수됨"), (int)_ticketHandle);
		}
	}
}


void USFTicketGeneratorComponent::ReleaseTicketByHandle_Implementation(int64 handle)
{
	TScriptInterface<ISFTicket> iTicket;

	if (handle > 0 && handle < _TicketObjectArray.Num())
	{
		iTicket = _TicketObjectArray[(int)handle];
		ReleaseTicket_Implementation(iTicket);
	}
}



