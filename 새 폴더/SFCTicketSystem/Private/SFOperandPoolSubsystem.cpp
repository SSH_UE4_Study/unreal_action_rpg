﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SFOperandPoolSubsystem.h"
#include "SFCTicketSystem.h"

void USFOperandPoolSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	CreatePool();
	SF_LOG(Warning, TEXT("SFC-010.02 SFCOperandPool Subsystem Init"));
}

void USFOperandPoolSubsystem::Deinitialize()
{
	SF_LOG(Warning, TEXT("SFC-010.02 SFCOperandPool Subsystem Deinit"));
}



void USFOperandPoolSubsystem::CreatePool()
{
	FSFOpVariant _empty;
	_rawOpVariantPool.SetNumZeroed(SFOPERANDPOOL_SIZE);

	for (int i = 0; i < SFOPERANDPOOL_SIZE; ++i)
		_idxQueue.Enqueue(i);

	SF_LOG(Warning, TEXT("Operand Pool(%d) Created."), SFOPERANDPOOL_SIZE);
}

int USFOperandPoolSubsystem::GetEmptyHandle()
{
	int idx = -1;
	if (_idxQueue.Peek(idx))	// Peek 못하면 false,
		_idxQueue.Pop();
	return idx;
}

void USFOperandPoolSubsystem::ReleaseHandle(int handle)
{
	if (handle != -1)
	{
		_rawOpVariantPool[handle].operandType = ESFOpHandleType::EOHT_None;
		_rawOpVariantPool[handle].strData.Empty();
		_rawOpVariantPool[handle].objectData = nullptr;

		_idxQueue.Enqueue(handle);
	}
}




bool USFOperandPoolSubsystem::GetOperandType_Implementation(FName operandName, ESFOpHandleType& opHndType)
{
	auto* pElem = _managedVariantPool.Find(operandName);
	if (pElem) {
		// opHndType = pElem->operandType;
		opHndType = _rawOpVariantPool[*pElem].operandType;
		return true;
	}

	opHndType = ESFOpHandleType::EOHT_None;
	return false;
}

bool USFOperandPoolSubsystem::PushVariant_Implementation(FName operandName, const FSFOpVariant& variant)
{
	auto* pValue = _managedVariantPool.Find(operandName);

	if (pValue)
	{
		_rawOpVariantPool[*pValue] = variant;
		return true;
	}
	else
	{
		int idx = GetEmptyHandle();
		if( idx != -1 ) {
			_managedVariantPool.Add(operandName, idx);
			FSFOpVariant& value = _rawOpVariantPool[idx];
			value = variant;
			return true;
		}
		SF_LOG(Error, TEXT("There is not empty variant"));
	}
	return false;
}

bool USFOperandPoolSubsystem::PeekVariant_Implementation(FName operandName, FSFOpVariant& variant)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	//if (pElem && _rawOpVariantPool[*pElem].operandType == ESFOpHandleType::EOHT_Variant) {
	if (pElem && _rawOpVariantPool[*pElem].operandType != ESFOpHandleType::EOHT_None) {
		variant = _rawOpVariantPool[*pElem];
		return true;
	}

	return false;
}

void USFOperandPoolSubsystem::ReleaseHandle_Implementation(FName operandName)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	if (pElem) {
		ReleaseHandle(*pElem);
		_managedVariantPool.Remove(operandName);
	}
}

void USFOperandPoolSubsystem::ShowGOPPool()
{
	for (auto& _pair : _managedVariantPool)
	{
		UE_LOG(LogTemp, Log, TEXT("[%s] {%s}")
			, *_pair.Key.ToString()
			, *_rawOpVariantPool[_pair.Value].ToString());
	}
}



void USFOperandPoolSubsystem::CreateTicketObjectAndRegister()
{
	USFTicketObject* pObject = NewObject<USFTicketObject>();
	pObject->SetHandle_Implementation(1234000L);

	FSFOpVariant _temp;
	_temp.operandType = ESFOpHandleType::EOHT_Object;
	_temp.objectData = pObject;

	PushVariant_Implementation(FName(TEXT("Test")), _temp);
	SF_LOG(Warning, TEXT("PushVariant : Test"));
}

void USFOperandPoolSubsystem::ForceGC()
{
	GEngine->ForceGarbageCollection();
	SF_LOG(Warning, TEXT("*** ForceGarbageCollection"));
}


// 편의 기능
bool USFOperandPoolSubsystem::PushVector(FName operandName, const FVector& vecData)
{
	auto* pValue = _managedVariantPool.Find(operandName);

	if (pValue)
	{
		_rawOpVariantPool[*pValue].vecData = vecData;
		return true;
	}
	else
	{
		int idx = GetEmptyHandle();
		if (idx != -1) {
			_managedVariantPool.Add(operandName, idx);
			FSFOpVariant& value = _rawOpVariantPool[idx];
			value.operandType = ESFOpHandleType::EOHT_Vector;
			value.vecData = vecData;
			return true;
		}
		SF_LOG(Error, TEXT("There is not empty variant"));
	}

	return false;
}

bool USFOperandPoolSubsystem::PushString(FName operandName, const FString& strData)
{
	auto* pValue = _managedVariantPool.Find(operandName);

	if (pValue)
	{
		_rawOpVariantPool[*pValue].strData = strData;
		return true;
	}
	else
	{
		int idx = GetEmptyHandle();
		if (idx != -1) {
			_managedVariantPool.Add(operandName, idx);
			FSFOpVariant& value = _rawOpVariantPool[idx];
			value.operandType = ESFOpHandleType::EOHT_String;
			value.strData = strData;
			return true;
		}
		SF_LOG(Error, TEXT("There is not empty variant"));
	}

	return false;
}

bool USFOperandPoolSubsystem::PushObject(FName operandName, UObject* objData)
{
	auto* pValue = _managedVariantPool.Find(operandName);

	if (pValue)
	{
		_rawOpVariantPool[*pValue].objectData = objData;
		return true;
	}
	else
	{
		int idx = GetEmptyHandle();
		if (idx != -1) {
			_managedVariantPool.Add(operandName, idx);
			FSFOpVariant& value = _rawOpVariantPool[idx];
			value.operandType = ESFOpHandleType::EOHT_Object;
			value.objectData = objData;
			return true;
		}
		SF_LOG(Error, TEXT("There is not empty variant"));
	}

	return false;
}

bool USFOperandPoolSubsystem::PeekVector(FName operandName, FVector& retVector)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	if (pElem 
	&& _rawOpVariantPool[*pElem].operandType != ESFOpHandleType::EOHT_None) {
		retVector = _rawOpVariantPool[*pElem].vecData;
		return true;
	}

	retVector = FVector::ZeroVector;
	return false;
}

bool USFOperandPoolSubsystem::PeekString(FName operandName, FString& retString)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	if (pElem
		&& _rawOpVariantPool[*pElem].operandType != ESFOpHandleType::EOHT_None) {
		retString = _rawOpVariantPool[*pElem].strData;
		return true;
	}

	retString = TEXT("");
	return false;
}

bool USFOperandPoolSubsystem::PeekObject(FName operandName, UObject*& retObject)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	if (pElem
		&& _rawOpVariantPool[*pElem].operandType != ESFOpHandleType::EOHT_None) {
		retObject = _rawOpVariantPool[*pElem].objectData;
		return true;
	}

	retObject = nullptr;
	return false;
}


bool USFOperandPoolSubsystem::PushVectorToObject(FName operandName, FName propertyName, const FVector& vecData)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	if (pElem
		&& _rawOpVariantPool[*pElem].operandType != ESFOpHandleType::EOHT_None) {
		UObject* pObject = _rawOpVariantPool[*pElem].objectData;

		if (pObject)
		{
			FProperty* ValueProp = FindFProperty<FProperty>(pObject->GetClass(), propertyName);
			if (ValueProp)
			{
				FVector* Dest = ValueProp->ContainerPtrToValuePtr<FVector>(pObject);
				if (Dest)
				{
					*Dest = vecData;
					return true;
				}
			}
		}
	}

	return false;
}


bool USFOperandPoolSubsystem::PeekVectorFromObject(FName operandName, FName propertyName, FVector& retVector)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	if (pElem
		&& _rawOpVariantPool[*pElem].operandType != ESFOpHandleType::EOHT_None) {
		UObject* pObject = _rawOpVariantPool[*pElem].objectData;

		if (pObject)
		{
			FProperty* ValueProp = FindFProperty<FProperty>(pObject->GetClass(), propertyName);
			if (ValueProp)
			{
				FVector* Dest = ValueProp->ContainerPtrToValuePtr<FVector>(pObject);
				if (Dest)
				{
					retVector = *Dest;
					return true;
				}
			}
		}
	}

	retVector = FVector::ZeroVector;
	return false;
}

bool USFOperandPoolSubsystem::PushStringToObject(FName operandName, FName propertyName, const FString& strData)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	if (pElem
		&& _rawOpVariantPool[*pElem].operandType != ESFOpHandleType::EOHT_None) {
		UObject* pObject = _rawOpVariantPool[*pElem].objectData;

		if (pObject)
		{
			FProperty* ValueProp = FindFProperty<FProperty>(pObject->GetClass(), propertyName);
			if (ValueProp)
			{
				FString* Dest = ValueProp->ContainerPtrToValuePtr<FString>(pObject);
				if (Dest)
				{
					*Dest = strData;
					return true;
				}
			}
		}
	}

	return false;
}

bool USFOperandPoolSubsystem::PeekStringFromObject(FName operandName, FName propertyName, FString& retString)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	if (pElem
		&& _rawOpVariantPool[*pElem].operandType != ESFOpHandleType::EOHT_None) {
		UObject* pObject = _rawOpVariantPool[*pElem].objectData;

		if (pObject)
		{
			FProperty* ValueProp = FindFProperty<FProperty>(pObject->GetClass(), propertyName);
			if (ValueProp)
			{
				FString* Dest = ValueProp->ContainerPtrToValuePtr<FString>(pObject);
				if (Dest)
				{
					retString = *Dest;
					return true;
				}
			}
		}
	}

	retString = TEXT("");
	return false;
}
