﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SFTicketProcessor.h"
#include "SFCTicketSystem.h"

// Add default functionality here for any ISFTicketProcessor functions that are not pure virtual.

USFTicketProcessorComponent::USFTicketProcessorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
}

// Called when the game starts
void USFTicketProcessorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	SF_LOG(Warning, TEXT("USFTicketProcessorComponent BeginPlay"));

}

void USFTicketProcessorComponent::InitializeComponent()
{
	Super::InitializeComponent();

	InitTicketInfoTable();
}


// Called every frame
void USFTicketProcessorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	ProcessTickets_Implementation(DeltaTime);
}



void USFTicketProcessorComponent::InitTicketInfoTable()
{
	if (TicketInfoDataTables.Num() > 0)
	{
		int _accTicketNum = 0;

		for (auto* pDataTable : TicketInfoDataTables)
		{
			if ( ::IsValid(pDataTable))
			{
				FString _temp;
				TArray<FSFTicketInfoTable*> _ticketInfos;
				pDataTable->GetAllRows<FSFTicketInfoTable>(_temp, _ticketInfos);

				// 데이터 테이블을 이용한 티겟 - 실행기 세트 등록.
				for (FSFTicketInfoTable* _elem : _ticketInfos)
				{
					auto& _sets = _executorFinder.FindOrAdd(_elem->TicketName);

					for( FName& _executorName : _elem->ExecutorNames ) {
						if( !_sets.Contains(_executorName) )
							_sets.Emplace(_executorName);
					}
				}

				_accTicketNum += _ticketInfos.Num();
			}
		}

		SF_LOG(Warning, TEXT("TicketTable Read Completed. ticket total cnt (%d)"), _accTicketNum);
	}
}

void USFTicketProcessorComponent::SetTicketGenerator_Implementation(const TScriptInterface<ISFTicketGenerator>& iGenerator)
{
	LinkedTicketGenerator = iGenerator;
}

void USFTicketProcessorComponent::SetOperandPool_Implementation(const TScriptInterface<ISFOperandPool>& iOperandPool)
{
	LinkedOperandPool = iOperandPool;
}

void USFTicketProcessorComponent::RegisterExecutorWithName_Implementation(FName executorName
	, const TScriptInterface<ISFTicketExecutor>& iExecutor)
{
	_ticketExecutorMap.FindOrAdd(executorName, iExecutor);

	TArray<FName> _Tickets;
	if (ISFTicketExecutor::Execute_GetRegisteredTickets(iExecutor.GetObject(), _Tickets) > 0)
	{
		for (auto& ticket : _Tickets)
		{
			auto& _sets = _executorFinder.FindOrAdd(ticket);
			if( !_sets.Contains(executorName))
				_sets.Emplace(executorName);	// 실행기 이름으로 Ticket 등록
		}
	}
}

void USFTicketProcessorComponent::RegisterExecutor_Implementation(const TScriptInterface<ISFTicketExecutor>& iExecutor)
{
	FName name;

	ISFTicketExecutor::Execute_GetExecutorName(iExecutor.GetObject(), name);

	if (!name.IsNone())
	{
		RegisterExecutorWithName_Implementation(name, iExecutor);
	}
	else
	{
		SF_LOG(Error, TEXT("Executor name is none."));
	}
}

void USFTicketProcessorComponent::UnregisterExecutor_Implementation(FName executorName)
{
	const TScriptInterface<ISFTicketExecutor>* _iExecutor = _ticketExecutorMap.Find(executorName);

	if (_iExecutor)
	{
		TArray<FName> _gpTags;
		if( ISFTicketExecutor::Execute_GetRegisteredTickets(_iExecutor->GetObject(), _gpTags) > 0 )
		{
			for( auto& tag : _gpTags )
			{
				auto* _pSets = _executorFinder.Find(tag);
				if( _pSets )
					_pSets->Remove(executorName);
			}
		}

		_ticketExecutorMap.Remove(executorName);
	}
}

void USFTicketProcessorComponent::UpdateExecutor_Implementation(FName executorName)
{
	const TScriptInterface<ISFTicketExecutor>* _iExecutor = _ticketExecutorMap.Find(executorName);

	if (_iExecutor)
	{
		TArray<FName> _gpTags;
		if (ISFTicketExecutor::Execute_GetRegisteredTickets(_iExecutor->GetObject(), _gpTags) > 0)
		{
			for (auto& tag : _gpTags)
			{
				auto& _sets = _executorFinder.FindOrAdd(tag);
				_sets.Emplace(executorName);	// 실행기 이름으로 GameplayTag 등록
			}
		}
	}
}

bool USFTicketProcessorComponent::GetExecutor_Implementation(FName executorName
	, TScriptInterface<ISFTicketExecutor>& iExecutor)
{
	const TScriptInterface<ISFTicketExecutor>* _iExecutor = _ticketExecutorMap.Find(executorName);

	if (_iExecutor)
	{
		iExecutor = *_iExecutor;
		return true;
	}
	return false;
}

void USFTicketProcessorComponent::ProcessTickets_Implementation(float DeltaTime)
{
	while (!_activeTicketQueue.IsEmpty())
	{
		TScriptInterface<ISFTicket> CurTicket = *_activeTicketQueue.Peek();
		
		ExecuteSingleTicket(CurTicket);

		_activeTicketQueue.Pop();
	}

}

void USFTicketProcessorComponent::DispatchTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket
	, bool IsDirect)
{
	if( IsDirect)
		ExecuteSingleTicket(iTicket);
	else
		_activeTicketQueue.Enqueue(iTicket);
}

void USFTicketProcessorComponent::DispatchRAWTicket_Implementation(UPARAM(ref)FSFTicketStruct& rawSFTicket, bool IsDirect)
{
	if (LinkedTicketGenerator.GetObject()) {

		int64 handle;
		TScriptInterface<ISFTicket> _newTicket;

		//
		if (ISFTicketGenerator::Execute_GetNewTicket(
			LinkedTicketGenerator.GetObject(), _newTicket, handle))
		{
			ISFTicket::Execute_SetTicket(_newTicket.GetObject(), rawSFTicket);
			DispatchTicket_Implementation(_newTicket, IsDirect);
		}
	}
}

void USFTicketProcessorComponent::DispatchTicketAsset_Implementation(
	const USFTicketAsset* ticketAsset
	, FSFTicketLocalOperand localOperand
	, bool UseLocalOperand)
{
	if ( ::IsValid(ticketAsset) && LinkedTicketGenerator.GetObject()) 
	{
		int64 handle;
		TScriptInterface<ISFTicket> _newTicket;

		//
		if (ISFTicketGenerator::Execute_GetNewTicket(
			LinkedTicketGenerator.GetObject(), _newTicket, handle))
		{
			FSFTicketStruct _temp = ticketAsset->TicketRAWData;

			if( UseLocalOperand && _temp != localOperand )
				_temp ^= localOperand;

			ISFTicket::Execute_SetTicket(_newTicket.GetObject(), _temp);

			// 오퍼랜드 처리


			DispatchTicket_Implementation(_newTicket, ticketAsset->IsDirectTicket);
		}
	}
}


void USFTicketProcessorComponent::ExecuteSingleTicket(const TScriptInterface<ISFTicket>& CurTicket)
{
	if (CurTicket && CurTicket.GetObject() != nullptr)
	{
		FSFTicketStruct _rawTicket;
		ISFTicket::Execute_GetTicket(CurTicket.GetObject(), _rawTicket);

		TSet<FName>* _pSets = _executorFinder.Find(_rawTicket.TicketName);
		if (_pSets)
		{
			TArray<UObject*, TFixedAllocator<32>> _execArray;

			for (auto& name : *_pSets)
			{
				auto* _pIExecutor = _ticketExecutorMap.Find(name);

				if (!_pIExecutor) {
					SF_LOG(Error, TEXT("Invalid Executor : (%s)"), *name.ToString());
					continue;
				}

				_execArray.Add(_pIExecutor->GetObject());
			}

			SF_LOG(Warning, TEXT("---- Execute Ticket-(%s)"), *_rawTicket.TicketName.ToString());

			FName _executorName;
			// 리스트 순회
			for (auto* pObj : _execArray)
			{
				ISFTicketExecutor::Execute_GetExecutorName(pObj, _executorName);
				SF_LOG(Warning, TEXT("Ticket Executor (%s)"), *_executorName.ToString());
				ISFTicketExecutor::Execute_ExecuteTicket(pObj, CurTicket);
			}

			SF_LOG(Warning, TEXT("---- End Execute ----"));
			
			// Discard 세팅
			ISFTicket::Execute_SetDiscard(CurTicket.GetObject());

			if (LinkedTicketGenerator.GetObject()) {
				ISFTicketGenerator::Execute_ReleaseTicket(LinkedTicketGenerator.GetObject(), CurTicket);
			}
		}
	}
}
