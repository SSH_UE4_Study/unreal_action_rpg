﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SFOperand.h"

// Add default functionality here for any ISFOperand functions that are not pure virtual.

void USFOperandObject::SetOperandName_Implementation(FName operandName)
{
	_operandName = operandName;
}

void USFOperandObject::GetOperandName_Implementation(FName& operandName)
{
	operandName = _operandName;
}

void USFOperandObject::GetOperandType_Implementation(ESFOpHandleType& operandType)
{
	operandType = _opType;
}

void USFOperandObject::SetVariant_Implementation(UPARAM(ref)FSFOpVariant& variant)
{
	_opType = ESFOpHandleType::EOHT_Variant;
	_opData = variant;
}

void USFOperandObject::GetVariant_Implementation(FSFOpVariant& variant)
{
	variant = _opData;
}



bool USFOpVariantData::SetStringByName(UObject* Target, FName VarName, FString NewValue, FString& OutValue)
{
	if (Target)
	{
		FString FoundValue;
		FStrProperty* ValueProp = FindFProperty<FStrProperty>(Target->GetClass(), VarName);
		if (ValueProp)
		{
			ValueProp->SetPropertyValue_InContainer(Target, NewValue);

			/// 향후 Shipping 시에는 아래 검증 내용 제거하기
			FoundValue = ValueProp->GetPropertyValue_InContainer(Target);
			OutValue = FoundValue;
			return true;
		}
	}
	return false;
}

bool USFOpVariantData::SetVectorByName(UObject* Target, FName VarName, FVector NewValue, FVector& OutValue)
{
	if (Target)
	{
		FString FoundValue;
		
		FProperty* ValueProp = FindFProperty<FProperty>(Target->GetClass(), VarName);
		if (ValueProp )
		{
			UE_LOG(LogTemp, Warning, TEXT("Type : (%s)"), *ValueProp->GetCPPType() );
			FVector* Dest = ValueProp->ContainerPtrToValuePtr<FVector>(Target);
			if( Dest )
			{
				*Dest = NewValue;
				OutValue = NewValue;
				return true;
			}
		}
	}
	return false;
}

bool USFOpVariantData::SetFloatByName(UObject* Target, FName VarName, float NewValue, float& OutValue)
{
	if (Target)
	{
		float FoundValue;
		FFloatProperty* ValueProp = FindFProperty<FFloatProperty>(Target->GetClass(), VarName);
		if (ValueProp)
		{
			ValueProp->SetPropertyValue_InContainer(Target, NewValue);

			/// 향후 Shipping 시에는 아래 검증 내용 제거하기
			FoundValue = ValueProp->GetPropertyValue_InContainer(Target);
			OutValue = FoundValue;
			return true;
		}
	}
	return false;
}


bool USFOpVariantData::GetStringByName(UObject* Target, FName VarName, FString& OutValue)
{
	if (Target)
	{
		FString FoundValue;
		FStrProperty* ValueProp = FindFProperty<FStrProperty>(Target->GetClass(), VarName);
		if (ValueProp)
		{
			FoundValue = ValueProp->GetPropertyValue_InContainer(Target);
			OutValue = FoundValue;
			return true;
		}
	}
	return false;
}

bool USFOpVariantData::GetVectorByName(UObject* Target, FName VarName, FVector& OutValue)
{
	if (Target)
	{
		FProperty* ValueProp = FindFProperty<FProperty>(Target->GetClass(), VarName);
		if (ValueProp)
		{
			FVector* FoundValue = ValueProp->ContainerPtrToValuePtr<FVector>(Target);
			if (FoundValue)
			{
				OutValue = *FoundValue;
				return true;
			}
		}
	}
	return false;
}

bool USFOpVariantData::GetFloatByName(UObject* Target, FName VarName, float& OutValue)
{
	if (Target)
	{
		FFloatProperty* ValueProp = FindFProperty<FFloatProperty>(Target->GetClass(), VarName);
		if (ValueProp)
		{
			OutValue = ValueProp->GetPropertyValue_InContainer(Target);
			return true;
		}
	}
	return false;
}
