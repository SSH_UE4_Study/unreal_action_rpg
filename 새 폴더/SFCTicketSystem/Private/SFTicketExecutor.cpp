﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SFTicketExecutor.h"
#include "SFCTicketSystem.h"
#include "SFTicketGeneratorSubsystem.h"
#include "Kismet/GameplayStatics.h"

// Add default functionality here for any ISFTicketExecutor functions that are not pure virtual.


USFTicketExecutorComponent::USFTicketExecutorComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void USFTicketExecutorComponent::BeginPlay()
{
	Super::BeginPlay();
}


//void USFTicketExecutorComponent::TickComponent(float DeltaTime
//											   , ELevelTick TickType
//											   , FActorComponentTickFunction* ThisTickFunction)
//{
//	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//}

void USFTicketExecutorComponent::SetExecutorName_Implementation(FName name)
{
	ExecutorName = name;
}

void USFTicketExecutorComponent::GetExecutorName_Implementation(FName& name)
{
	name = ExecutorName;
}

void USFTicketExecutorComponent::RegisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Emplace(_elem);
	}
}

void USFTicketExecutorComponent::UnregisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Remove(_elem);
	}
}

int USFTicketExecutorComponent::GetRegisteredTickets_Implementation(TArray<FName>& registerEvents)
{
	registerEvents = _processableTicketNames.Array();
	return registerEvents.Num();
}

void USFTicketExecutorComponent::ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket)
{
	ensureMsgf(iTicket, TEXT("Ticket is null or none"));

	//UE_LOG(LogTemp, Warning, TEXT("SFTicket 실행"));

	if( Func_OnExecuteSFTicket.IsBound() )
		Func_OnExecuteSFTicket.Broadcast(iTicket);

	// BP Event 호출하기
	FSFTicketStruct _rawTicket;
	ISFTicket::Execute_GetTicket(iTicket.GetObject(), _rawTicket);

	FName funcName = FName( *(TEXT("On") + _rawTicket.TicketName.ToString() ));
	CallBPFunc( GetOwner(), funcName, iTicket);
}

void USFTicketExecutorComponent::ManualExecuteTicket_Implementation(const FSFTicketStruct& manualTicket)
{
	if (Func_OnExecuteSFTicketStruct.IsBound())
		Func_OnExecuteSFTicketStruct.Broadcast(manualTicket);
	/*
	UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(GetOwner()); //GetGameInstance();
	USFTicketGeneratorSubsystem* pTicketGenerator =
		GameInstance->GetSubsystem<USFTicketGeneratorSubsystem>();

	if (pTicketGenerator)
	{
		int64 handle;
		TScriptInterface<ISFTicket> _newTicket;

		if(pTicketGenerator->GetNewTicket_Implementation(_newTicket, handle))
		{
			ISFTicket::Execute_SetTicket(_newTicket.GetObject(), manualTicket);

			if (Func_OnExecuteSFTicketStruct.IsBound())
				Func_OnExecuteSFTicketStruct.Broadcast(manualTicket);

			FName funcName = FName(*(TEXT("On") + manualTicket.TicketName.ToString()));
			CallBPFunc(GetOwner(), funcName, _newTicket);


			pTicketGenerator->ReleaseTicket_Implementation(_newTicket);
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Did not load TicketGeneratorSubSystem"));
	}
	*/
}


//void USFTicketExecutorComponent::ConnectGlobalOperandPool_Implementation(const TScriptInterface<ISFOperandPool>& iGlobalOperandPool)
//{
//	_globalOperandPool = iGlobalOperandPool;
//}


// BP 사이드 기능 함수 호출
void USFTicketExecutorComponent::CallBPFunc(AActor* pOwner
											, FName funcName
											, const TScriptInterface<ISFTicket>& iTicket)
{
	if( !pOwner )
		return;

	SF_LOG(Warning, TEXT(">> CallBP Event : (%s)::(%s)")
		, *funcName.ToString() , *ExecutorName.ToString());

	UFunction* const func = pOwner->FindFunction(funcName);
	if (func)
	{
		if( func->ParmsSize == 0 )
			pOwner->ProcessEvent(func, nullptr);
		else
		{
			void* buf = FMemory_Alloca(func->ParmsSize);
			FMemory::Memzero(buf, func->ParmsSize);

			for (TFieldIterator<FProperty> It(func);
				 It && It->HasAnyPropertyFlags(CPF_Parm); ++It)
			{
				FProperty* Prop = *It;
				FString strType = Prop->GetCPPType();

				if (strType == TEXT("TScriptInterface"))
				{
					TScriptInterface<ISFTicket>* ppTicketParam =
						Prop->ContainerPtrToValuePtr<TScriptInterface<ISFTicket>>(buf);
					*ppTicketParam = iTicket;
					//.GetInterface();
				}
			}

			pOwner->ProcessEvent(func, buf);
		}
	}
}
