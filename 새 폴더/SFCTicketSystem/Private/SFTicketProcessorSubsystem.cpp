﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SFTicketProcessorSubsystem.h"
#include "SFCTicketSystem.h"
#include "SFTicketGeneratorSubsystem.h"
#include "SFOperandPoolSubsystem.h"
#include "SFCTicketSubSystem.h"
#include "Subsystems/SubsystemCollection.h"

#include "AssetRegistry/AssetRegistryModule.h"
#include "Kismet/GameplayStatics.h"

#define SET_BOOLPTR_VALUE(a, value)	{ if(a) { *a = value; } }


void USFTicketProcessorSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Collection.InitializeDependency(USFCTicketSubsystem::StaticClass());
	Collection.InitializeDependency(USFTicketGeneratorSubsystem::StaticClass());
	Collection.InitializeDependency(USFOperandPoolSubsystem::StaticClass());
	SF_LOG(Warning, TEXT("SFC-010.04 SFTicketProcessor Subsystem Init"));

	UGameInstance* GameInstance = GetGameInstance();
	USFTicketGeneratorSubsystem* pTicketGenerator = 
		GameInstance->GetSubsystem<USFTicketGeneratorSubsystem>();

	if (pTicketGenerator)
	{
		LinkedTicketGenerator = pTicketGenerator;
		SF_LOG(Warning, TEXT("Ticket Generator(%p) 연결"), LinkedTicketGenerator.GetObject());
	}

	USFOperandPoolSubsystem* pOperandPool =
		GameInstance->GetSubsystem<USFOperandPoolSubsystem>();

	if (pOperandPool)
	{
		LinkedOperandPool = pOperandPool;
		SF_LOG(Warning, TEXT("OperandPool(%p) 연결"), LinkedOperandPool.GetObject());
	}

	// 테이블 에셋 읽기
	LoadTicketInfoTables();
}

void USFTicketProcessorSubsystem::Deinitialize()
{
	SF_LOG(Warning, TEXT("SFC-010.04 SFTicketProcessor Subsystem Deinit"));
}

// 테이블 모두 읽기
void USFTicketProcessorSubsystem::LoadTicketInfoTables()
{
	FAssetRegistryModule& AssetRegistryModule
		= FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
	TArray<FAssetData> AssetData;

	FARFilter filter;
	filter.ClassNames.Add(UDataTable::StaticClass()->GetFName());
	filter.PackagePaths.Add(TEXT("/Game/SFTickets"));
	AssetRegistryModule.Get().GetAssets(filter, AssetData);

	TArray<UDataTable*, TInlineAllocator<128>> tempAssetData;
	//TArray<TSharedPtr<FString>, TInlineAllocator<1024>> _tempTicketNames;

	for (auto& _asset : AssetData)
	{
		SF_LOG(Warning, TEXT("TicketInfoTable AssetName : [%s]"),
			   *(_asset.AssetName.ToString()));

		tempAssetData.Add(Cast<UDataTable>(_asset.GetAsset()));
	}

	InitTicketInfoTableRAW(tempAssetData);
}



void USFTicketProcessorSubsystem::Tick(float DeltaTime)
{
	if (GetWorld() && GetWorld()->IsGameWorld())
	{
		if (LastFrameNumberWeTicked == GFrameCounter)
			return;
		LastFrameNumberWeTicked = GFrameCounter;

		//UE_LOG(LogTemp, Warning, TEXT("TicketSystem Module Tick"));
		ProcessTickets_Implementation(DeltaTime);
	}
}

void USFTicketProcessorSubsystem::InitTicketInfoTable(UPARAM(ref)TArray<UDataTable*> DataTables)
{
	InitTicketInfoTableRAW(DataTables);
}

template<typename AllocatorType>
void USFTicketProcessorSubsystem::InitTicketInfoTableRAW(TArray<UDataTable*, AllocatorType>& DataTables)
{
	if (DataTables.Num() > 0)
	{
		int _accTicketNum = 0;

		for (auto* pDataTable : DataTables)
		{
			if (::IsValid(pDataTable))
			{
				FString _temp;
				TArray<FSFTicketInfoTable*> _ticketInfos;
				pDataTable->template GetAllRows<FSFTicketInfoTable>(_temp, _ticketInfos);

				// 데이터 테이블을 이용한 티겟 - 실행기 세트 등록.
				for (FSFTicketInfoTable* _elem : _ticketInfos)
				{
					auto& _sets = _executorFinder.FindOrAdd(_elem->TicketName);

					for (FName& _executorName : _elem->ExecutorNames) {
						if (!_sets.Contains(_executorName))
							_sets.Emplace(_executorName);
					}
				}

				_accTicketNum += _ticketInfos.Num();
			}
		}

		SF_LOG(Warning, TEXT("TicketTable Read Completed. ticket total cnt (%d)"), _accTicketNum);
	}
}



void USFTicketProcessorSubsystem::SetTicketGenerator_Implementation(const TScriptInterface<ISFTicketGenerator>& iGenerator)
{
	LinkedTicketGenerator = iGenerator;
}

void USFTicketProcessorSubsystem::SetOperandPool_Implementation(const TScriptInterface<ISFOperandPool>& iOperandPool)
{
	LinkedOperandPool = iOperandPool;
}

void USFTicketProcessorSubsystem::RegisterExecutorWithName_Implementation(FName executorName
																  , const TScriptInterface<ISFTicketExecutor>& iExecutor)
{
	_ticketExecutorMap.FindOrAdd(executorName, iExecutor);

	TArray<FName> _Tickets;
	if (ISFTicketExecutor::Execute_GetRegisteredTickets(iExecutor.GetObject(), _Tickets) > 0)
	{
		for (auto& ticket : _Tickets)
		{
			auto& _sets = _executorFinder.FindOrAdd(ticket);
			if (!_sets.Contains(executorName))
				_sets.Emplace(executorName);	// 실행기 이름으로 Ticket 등록
		}
	}
}

void USFTicketProcessorSubsystem::RegisterExecutor_Implementation(const TScriptInterface<ISFTicketExecutor>& iExecutor)
{
	FName name;

	ISFTicketExecutor::Execute_GetExecutorName(iExecutor.GetObject(), name);

	if (!name.IsNone())
	{
		RegisterExecutorWithName_Implementation(name, iExecutor);
	}
	else
	{
		SF_LOG(Error, TEXT("Executor name is none."));
	}
}

void USFTicketProcessorSubsystem::UnregisterExecutor_Implementation(FName executorName)
{
	const TScriptInterface<ISFTicketExecutor>* _iExecutor = _ticketExecutorMap.Find(executorName);

	if (_iExecutor)
	{
		TArray<FName> _gpTags;
		if (ISFTicketExecutor::Execute_GetRegisteredTickets(_iExecutor->GetObject(), _gpTags) > 0)
		{
			for (auto& tag : _gpTags)
			{
				auto* _pSets = _executorFinder.Find(tag);
				if (_pSets)
					_pSets->Remove(executorName);
			}
		}

		_ticketExecutorMap.Remove(executorName);
	}
}

void USFTicketProcessorSubsystem::UpdateExecutor_Implementation(FName executorName)
{
	const TScriptInterface<ISFTicketExecutor>* _iExecutor = _ticketExecutorMap.Find(executorName);

	if (_iExecutor)
	{
		TArray<FName> _gpTags;
		if (ISFTicketExecutor::Execute_GetRegisteredTickets(_iExecutor->GetObject(), _gpTags) > 0)
		{
			for (auto& tag : _gpTags)
			{
				auto& _sets = _executorFinder.FindOrAdd(tag);
				_sets.Emplace(executorName);	// 실행기 이름으로 GameplayTag 등록
			}
		}
	}
}

bool USFTicketProcessorSubsystem::GetExecutor_Implementation(FName executorName
													 , TScriptInterface<ISFTicketExecutor>& iExecutor)
{
	const TScriptInterface<ISFTicketExecutor>* _iExecutor = _ticketExecutorMap.Find(executorName);

	if (_iExecutor)
	{
		iExecutor = *_iExecutor;
		return true;
	}
	return false;
}

void USFTicketProcessorSubsystem::ProcessTickets_Implementation(float DeltaTime)
{
	bool bRepeat;
	TArray<TScriptInterface<ISFTicket>, TInlineAllocator<32>> _tempBuffer;

	while (!_activeTicketQueue.IsEmpty())
	{
		TScriptInterface<ISFTicket> CurTicket = *_activeTicketQueue.Peek();

		/// 시작 딜레이가 없다면 바로 티켓 실행
		if (ISFTicket::Execute_GetStartDelayTime(CurTicket.GetObject()) <= 0.f)
		{
			/// 반복 확인은 내부에서, 무조건 한 번은 실행하는 것으로.
			ExecuteSingleTicket(CurTicket, &bRepeat);

			if( bRepeat )
				_tempBuffer.Add(CurTicket);	// 반복 확인에 의한 push
		}
		else 
		{
			ISFTicket::Execute_DecStartDelayTime(CurTicket.GetObject(), DeltaTime);
			_tempBuffer.Add(CurTicket);
		}

		_activeTicketQueue.Pop();
	}

	/// 재실행 필요로 하는 티켓들은 다시 ActiveTicketQueue로 수거
	if (_tempBuffer.Num())
	{
		for (auto& it : _tempBuffer)
		{
			_activeTicketQueue.Enqueue(it);
		}
	}

}

void USFTicketProcessorSubsystem::DispatchTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket
														, bool IsDirect)
{
	if (IsDirect)
		ExecuteSingleTicket(iTicket, nullptr);
	else
		_activeTicketQueue.Enqueue(iTicket);
}

void USFTicketProcessorSubsystem::DispatchRAWTicket_Implementation(UPARAM(ref)FSFTicketStruct& rawSFTicket, bool IsDirect)
{
	if (LinkedTicketGenerator.GetObject()) {

		int64 handle;
		TScriptInterface<ISFTicket> _newTicket;

		//
		if (ISFTicketGenerator::Execute_GetNewTicket(
			LinkedTicketGenerator.GetObject(), _newTicket, handle))
		{
			ISFTicket::Execute_SetTicket(_newTicket.GetObject(), rawSFTicket);
			DispatchTicket_Implementation(_newTicket, IsDirect);
		}
	}
}

void USFTicketProcessorSubsystem::DispatchTicketAsset_Implementation(
	const USFTicketAsset* ticketAsset
	, FSFTicketLocalOperand localOperand
	, bool UseLocalOperand)
{
	if (::IsValid(ticketAsset) && LinkedTicketGenerator.GetObject())
	{
		int64 handle;
		TScriptInterface<ISFTicket> _newTicket;

		//
		if (ISFTicketGenerator::Execute_GetNewTicket(
			LinkedTicketGenerator.GetObject(), _newTicket, handle))
		{
			FSFTicketStruct _temp = ticketAsset->TicketRAWData;

			if (UseLocalOperand && _temp != localOperand)
				_temp ^= localOperand;

			ISFTicket::Execute_SetTicket(_newTicket.GetObject(), _temp);

			// 오퍼랜드 처리
			if (LinkedOperandPool && ticketAsset->OperandData.Num())
			{
				for (auto& _opPair : ticketAsset->OperandData)
				{
					LinkedOperandPool->PushVariant_Implementation(_opPair.Key, _opPair.Value);
				}
			}

			DispatchTicket_Implementation(_newTicket, ticketAsset->IsDirectTicket);
		}
	}
}


void USFTicketProcessorSubsystem::ExecuteSingleTicket(const TScriptInterface<ISFTicket>& CurTicket, bool* pIsRepeatCurTicket)
{
	if (CurTicket && CurTicket.GetObject() != nullptr)
	{
		FSFTicketStruct _rawTicket;
		ISFTicket::Execute_GetTicket(CurTicket.GetObject(), _rawTicket);

		// Executor Find.
		TSet<FName>* _pSets = _executorFinder.Find(_rawTicket.TicketName);
		if (_pSets)
		{
			TArray<UObject*, TFixedAllocator<32>> _execArray;

			for (auto& name : *_pSets)
			{
				auto* _pIExecutor = _ticketExecutorMap.Find(name);

				if (!_pIExecutor) {
					SF_LOG(Error, TEXT("Invalid Executor : (%s)"), *name.ToString());
					continue;
				}

				_execArray.Add(_pIExecutor->GetObject());
			}

			SF_LOG(Warning, TEXT("---- Execute Ticket-(%s)"), *_rawTicket.TicketName.ToString());

			FName _executorName;
			// 리스트 순회
			for (auto* pObj : _execArray)
			{
				ISFTicketExecutor::Execute_GetExecutorName(pObj, _executorName);
				SF_LOG(Warning, TEXT("Ticket Executor (%s)"), *_executorName.ToString());
				ISFTicketExecutor::Execute_ExecuteTicket(pObj, CurTicket);
			}

			// 외부에서 실행할게 있는가?
			if (OnPostTickEvent.IsBound())
				OnPostTickEvent.Broadcast(CurTicket,LastFrameNumberWeTicked);

			SF_LOG(Warning, TEXT("---- End Execute ----"));

			/// 반복 카운드 감소
			ISFTicket::Execute_DecRepeatCount(CurTicket.GetObject());

			/// 남은 반복 카운트 확인
			if (ISFTicket::Execute_GetRepeatCount(CurTicket.GetObject()) > 0)
			{
				SET_BOOLPTR_VALUE(pIsRepeatCurTicket, true);	// 티켓 반복
			}
			else
			{
				// Discard 세팅
				ISFTicket::Execute_SetDiscard(CurTicket.GetObject());

				// 티켓 수거
				if (LinkedTicketGenerator.GetObject())
					ISFTicketGenerator::Execute_ReleaseTicket(LinkedTicketGenerator.GetObject(), CurTicket);

				SET_BOOLPTR_VALUE(pIsRepeatCurTicket, false);	// 티켓 반복 종료
			}				
		}
	}
}



void USFTicketProcessorSubsystem::DispatchTicketEx(const TScriptInterface<ISFTicket>& iTicket, bool IsDirect, int Repeat, float delayTime)
{
	if (iTicket)
	{
		if ( !IsDirect 
			&& (Repeat > 0 || delayTime > 0.f))
		{
			//iTicket.GetObject()->IsA( USFTicketObject::GetClass() )
			USFTicketObject* pOriginTKObj = Cast<USFTicketObject>(iTicket.GetObject());

			if (pOriginTKObj)
			{
				/// 반복, 타임 설정은 티켓 인터페이스가 아닌 프로퍼티로 직접 접근,
				/// 반복 관련 프로퍼티에 접근 가능한 대상을 한정짓기 위함.
				pOriginTKObj->RepeatCount = Repeat;
				pOriginTKObj->StartDelayTime = delayTime;
			}
		}

		DispatchTicket_Implementation(iTicket, IsDirect);
	}
}
