﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SFOperandPool.h"
#include "SFCTicketSystem.h"

// Add default functionality here for any ISFOperandPool functions that are not pure virtual.
USFOperandPoolComponent::USFOperandPoolComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void USFOperandPoolComponent::BeginPlay()
{
	CreatePool();

	Super::BeginPlay();

	// ...
}

// Called every frame
void USFOperandPoolComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}


void USFOperandPoolComponent::CreatePool()
{
	FSFOpVariant _empty;
	_rawOpVariantPool.SetNumZeroed(SFOPERANDPOOL_SIZE);

	for( int i = 0 ; i < SFOPERANDPOOL_SIZE ; ++i )
		_idxQueue.Enqueue(i);

	SF_LOG(Warning, TEXT("Operand Pool(%d) Created."), SFOPERANDPOOL_SIZE);
}

int USFOperandPoolComponent::GetEmptyHandle()
{
	int idx = -1;
	if( _idxQueue.Peek(idx) )	// Peek 못하면 false,
		_idxQueue.Pop();
	return idx;
}

void USFOperandPoolComponent::ReleaseHandle(int handle)
{
	if (handle != -1)
	{
		_rawOpVariantPool[handle].operandType = ESFOpHandleType::EOHT_None;
		_rawOpVariantPool[handle].strData.Empty();
		_rawOpVariantPool[handle].objectData = nullptr;

		_idxQueue.Enqueue(handle);
	}
}


	

bool USFOperandPoolComponent::GetOperandType_Implementation(FName operandName, ESFOpHandleType& opHndType)
{
	auto* pElem = _managedVariantPool.Find(operandName);
	if (pElem) {
		// opHndType = pElem->operandType;
		opHndType = _rawOpVariantPool[*pElem].operandType;
		return true;
	}

	opHndType = ESFOpHandleType::EOHT_None;
	return false;
}

bool USFOperandPoolComponent::PushVariant_Implementation(FName operandName, const FSFOpVariant& variant)
{
	//_optypeMap.Add(operandName, ESFOpHandleType::EOHT_Variant);
	//_managedVariantPool.Add(operandName, variant);

	int idx = GetEmptyHandle();
	_managedVariantPool.Add(operandName, idx);
	FSFOpVariant& value = _rawOpVariantPool[idx];

	value = variant;
	return true;
}

bool USFOperandPoolComponent::PeekVariant_Implementation(FName operandName, FSFOpVariant& variant)
{
	auto* pElem = _managedVariantPool.Find(operandName);
	//if (pElem) {
	//	variant = *pElem;
	//	return true;
	//}

	if (pElem && _rawOpVariantPool[*pElem].operandType == ESFOpHandleType::EOHT_Variant) {
		variant = _rawOpVariantPool[*pElem];
		return true;
	}

	return false;
}

void USFOperandPoolComponent::ReleaseHandle_Implementation(FName operandName)
{
	auto* pElem = _managedVariantPool.Find(operandName);

	if (pElem) {
		ReleaseHandle(*pElem);
		_managedVariantPool.Remove(operandName);
	}
}
