﻿#include "SFCTicketSubSystem.h"
#include "SFCTicketSystem.h"
#include "SFTicketProcessorSubsystem.h"
#include "SFOperandPoolSubsystem.h"
#include "SFTicketGeneratorSubsystem.h"

#include "Subsystems/SubsystemCollection.h"


void USFCTicketSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	SF_LOG(Warning, TEXT("SFCTicketSubsystem Init"));
}


void USFCTicketSubsystem::Deinitialize()
{
	SF_LOG(Warning, TEXT("SFCTicketSubsystem Deinit"));
}

//void USFCTicketSubsystem::Tick(float DeltaTime)
//{
//	if (LastFrameNumberWeTicked == GFrameCounter)
//		return;
//	LastFrameNumberWeTicked = GFrameCounter;
//
//	//UE_LOG(LogTemp, Warning, TEXT("TicketSystem Module Tick"));
//	ProcessTickets_Implementation();
//
//	if(OnPostTickEvent.IsBound())
//		OnPostTickEvent.Broadcast((int)LastFrameNumberWeTicked);
//}


bool USFCTicketSubsystem::GetTicketGeneratorInterface(TScriptInterface<ISFTicketGenerator>& iGenerator)
{
	UGameInstance* pGameInstance = GetGameInstance();
	if (pGameInstance)
	{
		USFTicketGeneratorSubsystem* pSubSystem = pGameInstance->GetSubsystem<USFTicketGeneratorSubsystem>();
		if (pSubSystem)
		{
			iGenerator = pSubSystem;
			return true;
		}
	}

	return false;
}

bool USFCTicketSubsystem::GetOperandPoolInterface(TScriptInterface<ISFOperandPool>& iOperandPool)
{
	UGameInstance* pGameInstance = GetGameInstance();
	if (pGameInstance)
	{
		USFOperandPoolSubsystem* pSubSystem = pGameInstance->GetSubsystem<USFOperandPoolSubsystem>();
		if (pSubSystem)
		{
			iOperandPool = pSubSystem;
			return true;
		}
	}
	return false;
}

bool USFCTicketSubsystem::GetTicketProcessorInterface(TScriptInterface<ISFTicketProcessor>& iTicketProcessor)
{
	UGameInstance* pGameInstance = GetGameInstance();
	if (pGameInstance)
	{
		USFTicketProcessorSubsystem* pSubSystem = pGameInstance->GetSubsystem<USFTicketProcessorSubsystem>();
		if (pSubSystem)
		{
			iTicketProcessor = pSubSystem;
			return true;
		}
	}

	return false;
}
