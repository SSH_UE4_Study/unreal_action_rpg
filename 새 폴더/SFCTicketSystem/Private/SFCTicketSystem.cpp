﻿#include "SFCTicketSystem.h"

#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFOperandPoolSubsystem.h"

#include "Kismet/GameplayStatics.h"


DEFINE_LOG_CATEGORY(LogSFCTicketSystem);

#define LOCTEXT_NAMESPACE "FSFCTicketSystem"


// IEnhancedInputModule


void FSFCTicketSystem::StartupModule()
{
	SF_LOG(Warning, TEXT("SFCTicketSystem module has been loaded"));


	ShowGOPCommand = IConsoleManager::Get().RegisterConsoleCommand(
		TEXT("ShowSFGOP"), TEXT("SF 티켓 시스템의 동작중인 Global Operand Pool을 출력합니다.")
		, FConsoleCommandDelegate::CreateRaw(
			this, &FSFCTicketSystem::ShowGOPCmd, FString(""))
		, ECVF_Default);
}

void FSFCTicketSystem::ShutdownModule()
{
	SF_LOG(Warning, TEXT("SFCTicketSystem module has been unloaded"));

	IConsoleManager::Get().UnregisterConsoleObject(ShowGOPCommand);
	ShowGOPCommand = nullptr;
}

void FSFCTicketSystem::ShowGOPCmd(FString args)
{
	UWorld* pWorld = (GEngine) ? GEngine->GetCurrentPlayWorld() : nullptr;

	if (!pWorld) {
		SF_LOG(Error, TEXT("PlayWorld did not found."));
		return;
	}

	UGameInstance* pGameInstance = UGameplayStatics::GetGameInstance(pWorld);

	if (pGameInstance)
	{
		USFOperandPoolSubsystem* pOperandPool =
			pGameInstance->GetSubsystem<USFOperandPoolSubsystem>();

		if (pOperandPool)
		{
			UE_LOG(LogTemp, Log, TEXT("\r\nSF Ticekt System GOP Start --------------------------"));

			pOperandPool->ShowGOPPool();

			UE_LOG(LogTemp, Log, TEXT("SF Ticekt System GOP End ----------------------------"));
		}
	}
	else {
		SF_LOG(Error, TEXT("GameInstance did not found"));
	}
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFCTicketSystem, SFCTicketSystem)
