﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SFTicket.h"
#include "SFCTicketSystem.h"

#include "SFTicketProcessorSubsystem.h"

// Add default functionality here for any ISFTicket functions that are not pure virtual.

bool FSFTicketLocalOperand::operator!=(const FSFTicketStruct& Other) const
{
	return (
		OwnerUID != Other.OwnerUID ||
		TargetUID != Other.TargetUID ||
		nParam1 != Other.nParam1 ||
		nParam2 != Other.nParam2 ||
		vParam1 != Other.vParam1 ||
		vParam2 != Other.vParam2 ||
		OperandName != Other.OperandName ||
		UserObject != Other.UserObject
		);
}

FSFTicketStruct& FSFTicketStruct::operator^=(const FSFTicketLocalOperand& Other)
{
	OwnerUID = (OwnerUID != Other.OwnerUID) ? Other.OwnerUID : OwnerUID;
	TargetUID = (TargetUID != Other.TargetUID) ? Other.TargetUID : TargetUID;
	nParam1 = (nParam1 != Other.nParam1) ? Other.nParam1 : nParam1;
	nParam2 = (nParam2 != Other.nParam2) ? Other.nParam2 : nParam2;
	vParam1 = (vParam1 != Other.vParam1) ? Other.vParam1 : vParam1;
	vParam2 = (vParam2 != Other.vParam2) ? Other.vParam2 : vParam2;
	OperandName = (OperandName != Other.OperandName)
		? Other.OperandName : OperandName;
	UserObject = (UserObject != Other.UserObject)
		? Other.UserObject : UserObject;

	return *this;
}

//FSFTicketStruct FSFTicketStruct::operator^=(FSFTicketStruct& Src, const FSFTicketLocalOperand& Other)
//{
//	FSFTicketStruct _temp;
//	_temp.OwnerUID = (Src.OwnerUID != Other.OwnerUID) ? Other.OwnerUID : Src.OwnerUID;
//	_temp.TargetUID = (Src.TargetUID != Other.TargetUID) ? Other.TargetUID : Src.TargetUID;
//	_temp.nParam1 = (Src.nParam1 != Other.nParam1) ? Other.nParam1 : Src.nParam1;
//	_temp.nParam2 = (Src.nParam2 != Other.nParam2) ? Other.nParam2 : Src.nParam2;
//	_temp.vParam1 = (Src.vParam1 != Other.vParam1) ? Other.vParam1 : Src.vParam1;
//	_temp.vParam2 = (Src.vParam2 != Other.vParam2) ? Other.vParam2 : Src.vParam2;
//
//	return _temp;
//}

bool FSFTicketStruct::operator!=(const FSFTicketLocalOperand& Other) const
{
	return (
		OwnerUID != Other.OwnerUID ||
		TargetUID != Other.TargetUID ||
		nParam1 != Other.nParam1 ||
		nParam2 != Other.nParam2 ||
		vParam1 != Other.vParam1 ||
		vParam2 != Other.vParam2 ||
		OperandName != Other.OperandName ||
		UserObject != Other.UserObject
		);
}


void USFTicketObject::SetHandle_Implementation(int64 Handle)
{
	_managedHandle = Handle;
}

void USFTicketObject::GetHandle_Implementation(int64& Handle)
{
	Handle = _managedHandle;
}

void USFTicketObject::ClearTicket_Implementation()
{
	_operandNamePool.Empty();

	// 기본 값 초기화
	RepeatCount = 1;
	StartDelayTime = 0.f;

}

int USFTicketObject::GetOwnerUID_Implementation()
{
	return _ticketRAW.OwnerUID;
}

int USFTicketObject::GetTargetUID_Implementation()
{
	return _ticketRAW.TargetUID;
}


void USFTicketObject::SetTicket_Implementation(FSFTicketStruct ticket)
{
	_ticketRAW = ticket;
}


void USFTicketObject::GetTicket_Implementation(FSFTicketStruct& ticket)
{
	ticket = _ticketRAW;
}

void USFTicketObject::SetLocalOperand_Implementation(const FSFOpVariant& opVariant)
{
	_localOpVariant = opVariant;
}

void USFTicketObject::GetLocalOperand_Implementation(FSFOpVariant& opVariant)
{
	opVariant = _localOpVariant;
}

void USFTicketObject::PushGlobalOperandNames_Implementation(UPARAM(ref)TArray<FName>& names)
{
	_operandNamePool.Append(names);
}

void USFTicketObject::GetGlobalOperandNames_Implementation(TArray<FName>& opNames)
{
	opNames.Append(_operandNamePool);
}

void USFTicketObject::SetDiscard_Implementation()
{
	_discard_flag = true;
	//UE_LOG(LogTemp, Warning, TEXT("SF티켓 Set Discard"));
}

void USFTicketObject::IsDiscard_Implementation(bool& result)
{
	result = _discard_flag;
}

void USFTicketObject::BeginDestroy()
{
	if(_managedHandle == 1234000L) {
		SF_LOG(Warning
			, TEXT("USFTicketObject BeginDestroy : Handle(%lld)"), _managedHandle);
	}
	Super::BeginDestroy();
}

// 확장
int USFTicketObject::GetRepeatCount_Implementation()
{
	return RepeatCount; 
}

float USFTicketObject::GetStartDelayTime_Implementation()
{
	return StartDelayTime;
}

void USFTicketObject::DecRepeatCount_Implementation()
{
	--RepeatCount;
}

void USFTicketObject::DecStartDelayTime_Implementation(float DeltaTime)
{
	StartDelayTime = (StartDelayTime - DeltaTime < 0.f ) ? 0.f : StartDelayTime - DeltaTime;
}

//
//
//
//
//
USFTicketAsset::USFTicketAsset(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}


