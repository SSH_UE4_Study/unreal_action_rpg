﻿using UnrealBuildTool;
 
public class SFCCharacterSystem : ModuleRules
{
	public SFCCharacterSystem(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		bUseUnity = false;

		PublicDependencyModuleNames.AddRange(new string[] { "Core"
			, "CoreUObject", "Engine"
			, "PhysicsCore"
			, "GameplayTags"
			, "SFInputSystem"
			, "SFCTicketSystem"
		});

		//PrivateDependencyModuleNames.AddRange(
		//	new string[]
		//	{
		//		"SFInputSystem",
		//	});

		//PrivateIncludePathModuleNames.AddRange(
		//	new string[] {
		//		"SFInputSystem",
		//		//"SFCTicketSystem",
		//	}
		//);

		//PublicIncludePathModuleNames.AddRange(
		//	new string[] {
		//		"SFInputSystem",
		//		//"SFCTicketSystem",
		//	}
		//);

		PublicIncludePaths.AddRange(new string[] {"SFCCharacterSystem/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFCCharacterSystem/Private"});

		//PublicIncludePaths.AddRange(new string[] { "SFInputSystem/Public" });
		//PrivateIncludePaths.AddRange(new string[] { "SFInputSystem/Private" });
	}
}
