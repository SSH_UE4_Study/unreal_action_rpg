﻿#include "SFEntityManagerSubsystem.h"

#include "SFTicket.h"
#include "SFTicketExecutor.h"
#include "SFTicketGenerator.h"
#include "SFTicketProcessor.h"

#include "SFTicketProcessorSubsystem.h"

#include "Subsystems/SubsystemCollection.h"

void USFEntityManagerSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Collection.InitializeDependency(USFTicketProcessorSubsystem::StaticClass());
	UE_LOG(LogTemp, Warning, TEXT("SFC-003 SFEntityManager Init"));

	UGameInstance* GameInstance = GetGameInstance();
	USFTicketProcessorSubsystem* pTicketProcessor =
		GameInstance->GetSubsystem<USFTicketProcessorSubsystem>();

	if (pTicketProcessor)
	{
		ExecutorName = FName(TEXT("SFEntityManager"));
		pTicketProcessor->RegisterExecutor_Implementation(
			TScriptInterface<ISFTicketExecutor>(this));
		
		UE_LOG(LogTemp, Warning, TEXT("EntityManager, Ticket Executor 'SFEntityManager'이름으로 등록"));
	}
}

void USFEntityManagerSubsystem::Deinitialize()
{
	UE_LOG(LogTemp, Warning, TEXT("SFC-003 SFEntityManager Deinit"));
}




void USFEntityManagerSubsystem::RegisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity, int fixedUID, bool& bResult)
{
	if (!_entities.Find(iSFEntity))
	{
		/// 1) fixedUID -1인 경우 UID Generated
		int _newUID = fixedUID;

		if (fixedUID < 0)
		{
			GenerateUID_Implementation(_newUID);

			/// 1-1) Entity 에 Generated UID 설정
			ISFEntity::Execute_SetUID(iSFEntity.GetObject(), _newUID);
		}
		else
		{

			/// 1-2) fixedUID가 있다면,
			auto* pIEntity = _mapEntities.Find(_newUID);

			if (pIEntity) {
				UE_LOG(LogTemp, Error, TEXT("the Entity(%d) already exist."), _newUID );
				return;
			}
		}

		/// 2) Pool에 등록
		_entities.Add(iSFEntity);
		_mapEntities.Add(_newUID, iSFEntity);

		UE_LOG(LogTemp, Warning, TEXT("Entity(%p) UID(%d) was registered"), iSFEntity.GetObject(), _newUID );
	}
}

void USFEntityManagerSubsystem::UnregisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity, bool& bResult)
{
	if (_entities.Find(iSFEntity))
	{
		int _uid = -1;
		ISFEntity::Execute_GetUID(iSFEntity.GetObject(), _uid);

		if (_uid > 0)
		{
			_mapEntities.Remove(_uid);
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Entity UID is less than 0. (%d)"), _uid);
			return;
		}

		_entities.Remove(iSFEntity);
	}
}

void USFEntityManagerSubsystem::FindRegisteredEntity_Implementation(int UID, TScriptInterface<ISFEntity>& iSFEntity, bool& bResult)
{
	auto* pIEntity = _mapEntities.Find(UID);

	if (pIEntity)
	{
		iSFEntity = *pIEntity;
		bResult = true;
		return;
	}

	bResult = false;
}

void USFEntityManagerSubsystem::GenerateUID_Implementation(int& UID)
{
	UID = _generatedUID++;
}


void USFEntityManagerSubsystem::SetExecutorName_Implementation(FName name)
{
	ExecutorName = name;
}

void USFEntityManagerSubsystem::GetExecutorName_Implementation(FName& name)
{
	name = ExecutorName;
}

void USFEntityManagerSubsystem::RegisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Emplace(_elem);
	}
}

void USFEntityManagerSubsystem::UnregisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Remove(_elem);
	}
}

int USFEntityManagerSubsystem::GetRegisteredTickets_Implementation(TArray<FName>& registerEvents)
{
	registerEvents = _processableTicketNames.Array();
	return registerEvents.Num();
}

void USFEntityManagerSubsystem::ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket)
{
	ensureMsgf(iTicket, TEXT("Ticket is null or none"));

	static FName _prevTicketName;
	// BP Event 호출하기
	FSFTicketStruct _rawTicket;
	ISFTicket::Execute_GetTicket(iTicket.GetObject(), _rawTicket);

	if(_rawTicket.TicketName != _prevTicketName ) {
		UE_LOG(LogTemp, Warning, TEXT("EntityMngSubsystem : GetTicket[%s]- OUID(%d)")
			   , *_rawTicket.TicketName.ToString()
			   , _rawTicket.OwnerUID);
	}
	_prevTicketName = _rawTicket.TicketName;

	// OwnerUID 에 따른 전달
	if (_rawTicket.OwnerUID > 0) 
	{
		bool bResult;
		TScriptInterface<ISFEntity> ownerEntity;

		ISFEntityManager::Execute_FindRegisteredEntity(this, _rawTicket.OwnerUID, ownerEntity, bResult);

		if ( bResult 
			&& ownerEntity.GetObject()->GetClass()->ImplementsInterface(USFTicketExecutor::StaticClass()) )
		{
			ISFTicketExecutor::Execute_ExecuteTicket(ownerEntity.GetObject(), iTicket);
		}
	}
	
	// BP 후처리
	if (Func_OnExecuteSFTicketForMng.IsBound())
		Func_OnExecuteSFTicketForMng.Broadcast(iTicket, _rawTicket.OwnerUID);
}
