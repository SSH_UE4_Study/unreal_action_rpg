﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SFCharacterMovementComponent.h"
#include "SFCCharacterSystem.h"
#include "GameFramework/Character.h"
#include "GameFramework/PlayerState.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Chaos/ChaosEngineInterface.h"
#include "GameFramework/PhysicsVolume.h"
#include "GameplayTagContainer.h"

#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"

#include "SFEntity.h"


FSFSwimProperties::FSFSwimProperties()
{
	LeftFootName = TEXT("Bip001-L-Foot");
	LeftKneeBoneName = TEXT("Bip001-L-Calf");
	ChestBoneName = TEXT("Bip001-Neck");

	fWaterTraceMaxHeight = 10000.0f;
	SwimState = EA02SwimState::Walk;
	bIsSwimCheck = false;
	fStopCheckTime = 0.0f;

	/// <summary>
	/// 초기 설정 필요한 프로퍼티들
	/// </summary>
	//fDistanceFootToKnee = 
	//fDistanceFootToChest
	//fSwimBuoyancy
}

void FSFSwimProperties::SetPropertiesByOwner(const TScriptInterface<ISFPawn>& IOwner)
{
	ACharacter* pOwnChar;

	if (IOwner && ISFPawn::Execute_GetCharacter(IOwner.GetObject(), pOwnChar))
	{
		FVector leftFootLoc = pOwnChar->GetMesh()->GetSocketLocation(LeftFootName);
		FVector leftKeenLoc = pOwnChar->GetMesh()->GetSocketLocation(LeftKneeBoneName);
		FVector chestLoc = pOwnChar->GetMesh()->GetSocketLocation(ChestBoneName);

		fDistanceFootToKnee = leftKeenLoc.Z - leftFootLoc.Z;
		fDistanceFootToChest = chestLoc.Z - leftFootLoc.Z;

		if( pOwnChar->GetCharacterMovement() )
			fSwimBuoyancy = pOwnChar->GetCharacterMovement()->Buoyancy;
	}
}




void USFCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	CreateMoveStatusPool(MaxTrajectoryCount);

	MovSensor = NewObject<USFMovSensor>(GetTransientPackage(),FName(TEXT("SFMovSensor")));
	if (MovSensor) {
		MovSensor->UseDebugDraw = DrawDebugForMoveStatus;
		MovSensor->CreatePredictionBuffer(MaxTrajectoryCount);
	}

	StatusUpdateTime = 0.f;
	m_CurStatus = 0;

	// 초기화
	auto& cur = GetStatus(m_CurStatus);

	cur.Location = GetOwner()->GetActorLocation();

	cur.Acceleration = Acceleration;
	cur.Velocity = LastUpdateVelocity;
	cur.InputVector = GetLastInputVector();
	cur.BodyForward = GetLastUpdateRotation().Vector();
	cur.SurfaceType =
		(MovementMode == EMovementMode::MOVE_Falling) 
		? ESurfaceType::EST_InAir : ESurfaceType::EST_OnGround;

	/// 기능 연결을 위한 초기화
	OwnerCapsuleHalfHeight =
		CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();

	SwimProperties.SetPropertiesByOwner(CharacterOwner.Get());

	/// 수영 데이터 관련 세팅
	APlayerState* pPlayerState = CharacterOwner->GetPlayerState();

	if( pPlayerState && pPlayerState->GetClass()->ImplementsInterface(USFEntity::StaticClass()))
	{
		ISFEntity::Execute_SetEntityLocalVariable(pPlayerState, FName(TEXT("SwimTurnWeight")), 3.f);
		ISFEntity::Execute_SetEntityLocalBoolean(pPlayerState, FName(TEXT("IsSwimCheck")), false);
		ISFEntity::Execute_SetEntityLocalVariable(pPlayerState, FName(TEXT("SwimBuoyancy")), Buoyancy);
		ISFEntity::Execute_SetEntityLocalName(pPlayerState, FName(TEXT("SwimState")), FName(TEXT("Walk")));
	}
}

void USFCharacterMovementComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (MovSensor)
	{
		MovSensor->ConditionalBeginDestroy();
		MovSensor = nullptr;
	}

	Super::EndPlay(EndPlayReason);
}

void USFCharacterMovementComponent::TickComponent(float delta_time, ELevelTick tick_type, FActorComponentTickFunction* tick_function)
{
	Super::TickComponent(delta_time, tick_type, tick_function);

	UpdateMovStatus(delta_time);

	if (MovSensor)
	{
		auto& cur = GetStatus(m_CurStatus);

		MovSensor->PullupSensorHeight = PullupSensorHeight;
		MovSensor->OnMovementTrace(CharacterOwner
			, cur.Acceleration
			, cur.Velocity
			, cur.InputVector
			, 1.f); // StatusUpdateCycleLimit);
	}
}

void USFCharacterMovementComponent::ProcessLanded(const FHitResult& Hit, float remainingTime, int32 Iterations)
{
	Super::ProcessLanded(Hit, remainingTime, Iterations);
}

void USFCharacterMovementComponent::Launch(FVector const& LaunchVel)
{
	Super::Launch(LaunchVel);
}

bool USFCharacterMovementComponent::CustomFindFloor(FFindFloorResult& out_floor_result, const FVector start, const FVector end)
{
	FCollisionQueryParams floor_check_collision_params;
	floor_check_collision_params.AddIgnoredActor(CharacterOwner);
	FCollisionShape capsule_shape = FCollisionShape::MakeCapsule(
		CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleRadius()
		, CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());

	FName profileName = CharacterOwner->GetCapsuleComponent()->GetCollisionProfileName();

	FHitResult floor_hit_result;
	bool is_blocking_hit = GetWorld()->SweepSingleByProfile(floor_hit_result
															, start
															, end
															, CharacterOwner->GetActorRotation().Quaternion()
															, (profileName == FName(TEXT("Custom"))) ? FName(TEXT("Pawn")) : profileName
															, capsule_shape, floor_check_collision_params);

	if (is_blocking_hit)
	{
		FindFloor(start, out_floor_result, false, &floor_hit_result);

		return (out_floor_result.IsWalkableFloor() && IsValidLandingSpot(start, out_floor_result.HitResult));
	}

	return false;
}



int USFCharacterMovementComponent::GetPrevStatusIdx()
{
	return (m_CurStatus - 1 >= 0) ? (m_CurStatus - 1) : m_StatusMax - 1;
}

int USFCharacterMovementComponent::GetLastStatusIdx()
{
	// return (m_CurState + 1 < 3 ) ? (m_CurState + 1) : 0;
	return (m_CurStatus - 2 < 0) ? (m_CurStatus + m_StatusMax - 2) : m_CurStatus - 2;
}

int USFCharacterMovementComponent::GetPrevStatusWithBase(int base)
{
	return (base - 1 >= 0) ? (base - 1) : m_StatusMax - 1;
}

void USFCharacterMovementComponent::CreateMoveStatusPool(int count)
{
	m_MoveStatusPool.Empty();

	m_MoveStatusPool.AddZeroed(count);
	for (int i = 0; i < count; ++i) {
		m_MoveStatusPool[i] = MakeShareable(new FSFMovementStatus());
	}

	/// 예측 풀도 동일하게. x -> 5개 일단 
	m_PredictionStatusPool.AddZeroed(MaxPredictionCount);
	for (int i = 0; i < MaxPredictionCount; ++i) {
		m_PredictionStatusPool[i] = MakeShareable(new FSFMovementStatus());
	}

	m_CurStatus = 0;
	m_StatusMax = count;
}

void USFCharacterMovementComponent::UpdateStatusIdx()
{
	m_CurStatus++;
	if (m_CurStatus >= m_StatusMax)
		m_CurStatus = 0;
}

FSFMovementStatus& USFCharacterMovementComponent::GetStatus(int cursor)
{
	return *m_MoveStatusPool[cursor];
}

const FSFMovementStatus& USFCharacterMovementComponent::GetCurMoveStatus()
{
	return GetStatus(m_CurStatus);
}

const FSFMovementStatus& USFCharacterMovementComponent::GetPrevMoveStatus()
{
	return GetStatus(GetPrevStatusIdx());
}

const FSFMovementStatus& USFCharacterMovementComponent::GetLastMoveStatus()
{
	return GetStatus(GetLastStatusIdx());
}


void USFCharacterMovementComponent::UpdateMovStatus(float delta_time)
{
	StatusUpdateTime += delta_time;

	if (StatusUpdateTime >= StatusUpdateCycleLimit)
	{
		StatusUpdateTime -= StatusUpdateCycleLimit;

		StoreCurrentStatus();

		// 예측 포지션
		MakePredictionStatus();
	}

	// Water 체크
	/*
	if (SwimProperties.fStopCheckTime <= 0.f) {
		CheckWaterSurface(delta_time);
		ControlMovementModeBySwimState();
	}
	else {
		// 체크 갭이 생기면. 타이머 동작
		SwimProperties.fStopCheckTime =
			FMath::Max(SwimProperties.fStopCheckTime - delta_time, 0.f);
	}
	*/

	DrawDebugInfo();
}


// SFSwimComponent 기능을 일부 이주
void USFCharacterMovementComponent::CheckWaterSurface(float delta_time)
{
	//if (!(MovSensor && MovSensor->IsSwimCheck))
	//	return;
	if(!CharacterOwner || !CharacterOwner->GetPlayerState())
		return;

	//TODO: @안석 - 캐릭터 상태 체크 비용이 클 수 있어, 대안 찾기
	if( !CharacterOwner->GetClass()->ImplementsInterface(USFPawn::StaticClass()) )
		return;

	if( ISFEntity::Execute_GetEntityLocalBoolean(CharacterOwner->GetPlayerState()
			, FName(TEXT("IsSwimCheck"))) == false )
		return;

	// Swim 체크 중
	//if(_logUpdateTime <= 0.f) {
		UE_LOG(LogTemp, Warning, TEXT("In CheckWaterSurface"));
	//	_logUpdateTime = 0.5f;
	//}
	//else {
	//	_logUpdateTime -= delta_time;
	//}

	bool isClimbing = ISFPawn::Execute_IsPawnState(CharacterOwner, ESFPawnState::Climbing );

	FVector ownerUp = CharacterOwner->GetActorUpVector();
	FVector TraceStart = CharacterOwner->GetActorLocation()
		+ ( OwnerCapsuleHalfHeight * ownerUp);
	FVector TraceEnd = TraceStart + ( -ownerUp * SwimProperties.fWaterTraceMaxHeight );

	TArray<FHitResult> hitResults;
	TArray<AActor*> ignoreActors;

	float distanceToGround = 0.f;
	float distanceToWater = 0.f;

	if (!UKismetSystemLibrary::LineTraceMulti(this, TraceStart, TraceEnd
											  , SwimProperties.SwimTraceChannel
											  , false, ignoreActors
											  , SwimProperties.ShowDebugTrace ? EDrawDebugTrace::ForDuration : EDrawDebugTrace::None
											  , hitResults, true
											  , FLinearColor::Red
											  , FLinearColor::Green
											  , 0.f))
	{
		// Stop Swim Chesk 후 종료
		DoStateCommand_Implementation(FName(TEXT("StopSwimCheck")),TEXT(""));
		return;
	}

	// 물 표현, 수면 아래 지형 체크
	for (auto hitResult : hitResults)
	{
		if( hitResult.PhysMaterial->SurfaceType == SwimProperties.GroundSurfaceType )
			distanceToGround = hitResult.Distance;
		else if( hitResult.PhysMaterial->SurfaceType == SwimProperties.WaterSurfaceType )
			distanceToWater = hitResult.Distance;
	}

	float PawnHeight = OwnerCapsuleHalfHeight * 2.f;
	float _distance = 0.f;

	/// 점프. 낙하 상태에서 물에 닿는 경우 체크.
	/// 물과 땅 거리를 기준으로 거리 설정
	if( MovementMode == EMovementMode::MOVE_Falling )
		_distance = distanceToGround - distanceToWater;
	else
		_distance = PawnHeight - distanceToWater;

	/// 물속 걷는 상태인지, 수영 상태인지 구분
	if (_distance > SwimProperties.fDistanceFootToChest
		|| distanceToGround >= 0.f)	// 0 - 충돌이 없음, 수면 아래 지형이 없는 것으로 판단
	{
		SwimProperties.SwimState = EA02SwimState::Swim;

		ISFEntity::Execute_SetEntityLocalName(CharacterOwner->GetPlayerState()
											  , FName(TEXT("SwimState"))
											  , FName(TEXT("Swim")));
	}
	else if (_distance > SwimProperties.fDistanceFootToKnee)
	{
		// 수중, 등반 가능한 상태가 아니면 
		if( !isClimbing) {
			SwimProperties.SwimState = EA02SwimState::WaterWalking;

			ISFEntity::Execute_SetEntityLocalName(CharacterOwner->GetPlayerState()
												  , FName(TEXT("SwimState"))
												  , FName(TEXT("WaterWalking")));
		}
	}
	else
	{
		// 수면 등반 가능한 상태가 아니면 일반 걷기
		if( !isClimbing) {
			SwimProperties.SwimState = EA02SwimState::Walk;

			ISFEntity::Execute_SetEntityLocalName(CharacterOwner->GetPlayerState()
												  , FName(TEXT("SwimState"))
												  , FName(TEXT("Walk")));
		}
	}

	// 낙하 상태, 수영 중?
	if (MovementMode == EMovementMode::MOVE_Falling 
		&& SwimProperties.SwimState == EA02SwimState::Swim)
	{
		// 수영 상태 체크 시간 1초에 한 번씩 체크
		SwimProperties.fStopCheckTime = 1.f;
	}

}


// 무브먼트 모드 제어
void USFCharacterMovementComponent::ControlMovementModeBySwimState()
{
	//if( !SwimProperties.bIsSwimCheck)
	//	return;
	if( !CharacterOwner || !CharacterOwner->GetPlayerState())
		return;

	if ( !ISFEntity::Execute_GetEntityLocalBoolean(CharacterOwner->GetPlayerState()
												  , FName(TEXT("IsSwimCheck"))))
		return;

	if (!CharacterOwner->GetClass()->ImplementsInterface(USFPawn::StaticClass()))
		return;

	

	/// 1) 등반 중 Swim으로 바꿔야 할 때 외, 강제 모드 변경은 하지 않음 <- 옛 코드 주석
	if (ISFPawn::Execute_IsPawnState(CharacterOwner, ESFPawnState::Climbing))
	{
		if (SwimProperties.SwimState == EA02SwimState::Swim)
		{
			Buoyancy = SwimProperties.fSwimBuoyancy;
			ISFPawn::Execute_CallSendActionEvent(CharacterOwner,
				FGameplayTag::RequestGameplayTag(FName(TEXT("Event.Input.Action.Swim"))));
		}
		return;	// 그 외 상태 변경 없이 종료
	}

	if (SwimProperties.SwimState == EA02SwimState::Walk ||
		SwimProperties.SwimState == EA02SwimState::WaterWalking)
	{
		Buoyancy = 0.f;
		if( IsSwimming() )
			SetMovementMode(EMovementMode::MOVE_Walking);
	}
	else if (SwimProperties.SwimState == EA02SwimState::Swim)
	{
		Buoyancy = SwimProperties.fSwimBuoyancy;
		SetMovementMode(EMovementMode::MOVE_Swimming);

		bool isSwimming = false;

		/// 인터페이스로 Hero, Runners 체크 통합
		/// SFSwimComponent.cpp 209~221 라인 참고

		if (!ISFPawn::Execute_IsPawnState(CharacterOwner, ESFPawnState::Swimming))
		{
			ISFPawn::Execute_CallSendActionEvent(CharacterOwner,
				FGameplayTag::RequestGameplayTag(FName(TEXT("Event.Input.Action.Swim"))));
		}
	}
}





void USFCharacterMovementComponent::StoreCurrentStatus()
{
	UpdateStatusIdx();
	auto& newCur = GetStatus(m_CurStatus);

	newCur.Location = LastUpdateLocation;
	newCur.Acceleration = Acceleration;
	newCur.Velocity = LastUpdateVelocity;
	newCur.InputVector = GetLastInputVector();
	newCur.BodyForward = GetLastUpdateRotation().Vector();
	newCur.SurfaceType =
		(MovementMode == EMovementMode::MOVE_Falling)
		? ESurfaceType::EST_InAir : ESurfaceType::EST_OnGround;
}

void USFCharacterMovementComponent::MakePredictionStatus()
{
	auto& curStatus = GetStatus(m_CurStatus);			// GetCurMoveStatus();
	auto& prevStatus = GetStatus(GetPrevStatusIdx());	 // GetPrevMoveStatus();


	FVector vAccelDir = curStatus.Acceleration.GetSafeNormal2D();
	FVector vVel = curStatus.Velocity;
	float vVelLength = vVel.Size2D() * StatusUpdateCycleLimit;

	FVector vNextLoc;
	FVector vPrevLoc = curStatus.Location -
		FVector(0.f,0.f, CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());

	// 회전각 얻기
	float sn, cs;
	FVector vBaseDir = vVel.GetSafeNormal2D();	// 마지막 속도 값
	FVector vNewBaseDir = GetOwner()->GetActorForwardVector();	// 시작 액터 방향값

	GetDirToSinCos(vBaseDir, sn, cs);
	FVector vLocalVel = GetLocalVel(sn, cs, vAccelDir);
	FVector vWorldVelDir;

	for (int i = 0; i < MaxPredictionCount; ++i)
	{
		GetDirToSinCos(vNewBaseDir, sn, cs);
		vWorldVelDir = GetWorldVel(sn, cs, vLocalVel);

		vNextLoc = vPrevLoc + (vWorldVelDir * vVelLength);

		FindPredictionFloor( vNextLoc, vNextLoc );

		m_PredictionStatusPool[i]->InputVector = vPrevLoc;
		m_PredictionStatusPool[i]->Location = vNextLoc;
		m_PredictionStatusPool[i]->BodyForward = vWorldVelDir; // vInvVel;
		m_PredictionStatusPool[i]->PredictionLength = vVelLength;

		vNewBaseDir = (vNextLoc - vPrevLoc).GetSafeNormal2D();
		vPrevLoc = vNextLoc;
	}

	if (MovSensor)
	{
		for (int i = 0; i < MaxPredictionCount; ++i)
			MovSensor->SetPredictedLoc(i, m_PredictionStatusPool[i]->Location);
	}
}

bool USFCharacterMovementComponent::FindPredictionFloor(const FVector BaseLoc, FVector& HitImpactPoint)
{
	FCollisionQueryParams collision_params;
	collision_params.AddIgnoredActor(CharacterOwner);
	FHitResult hitResult;

	FName profileName = CharacterOwner->GetCapsuleComponent()->GetCollisionProfileName();

	bool bRet = GetWorld()->LineTraceSingleByProfile(hitResult
		, BaseLoc + FVector(0.f, 0.f, 1000.f)
		, BaseLoc - FVector(0.f, 0.f, 1000.f)
		, (profileName == FName(TEXT("Custom"))) ? FName(TEXT("Pawn")) : profileName
		, collision_params);

	HitImpactPoint = (bRet) ? hitResult.ImpactPoint : BaseLoc;
	return bRet;
}

// 벡터, 월드 기준 sin, cos 얻그
void USFCharacterMovementComponent::GetDirToSinCos(FVector vWorldDir, float& sn, float& cs)
{
	float rad = FMath::Atan2(vWorldDir.Y, vWorldDir.X);

	sn = FMath::Sin(rad);
	cs = FMath::Cos(rad);
}

FVector USFCharacterMovementComponent::GetLocalVel(float sn, float cs, FVector WorldVel)
{
	FVector localVel;

	localVel.X = cs * WorldVel.X + sn * WorldVel.Y;
	localVel.Y = cs * WorldVel.Y - sn * WorldVel.X;
	localVel.Z = 0.f;

	return localVel;
}

FVector USFCharacterMovementComponent::GetWorldVel(float sn, float cs, FVector LocalVel)
{
	FVector WorldVel;

	WorldVel.X = cs * LocalVel.X - sn * LocalVel.Y;
	WorldVel.Y = sn * LocalVel.X + cs * LocalVel.Y;
	WorldVel.Z = 0.f;

	return WorldVel;
}


void USFCharacterMovementComponent::DrawDebugInfo()
{
	if (DrawDebugForMoveStatus)
	{
		float rcpMaxAccel = (100.f * 1.f) / GetMaxAcceleration();
		float rcpMaxVel = (100.f * 1.f) / MaxWalkSpeed;
		float zHeight = 0.f;

		FVector _unitLoc, _unitLoc2;
		FVector _dirLoc;

		int _curIdx;

		//UE_LOG(LogTemp, Warning, TEXT("== Start : %d"), m_CurState);

		/// Start
		DrawOneFrame(m_CurStatus, 1.75f, rcpMaxAccel, rcpMaxVel, true);

		//TArray<int, TInlineAllocator<16>> _tempBuff;
		int _tempBuff[16];
		int _stackTop = 0;

		for (int i = m_CurStatus + 1; i < m_CurStatus + m_StatusMax ; ++i)
		{
			_curIdx = (i < m_StatusMax) ? i : i - m_StatusMax;
			_tempBuff[_stackTop] = _curIdx;
			_stackTop++;
		}

		float _clr_weight = 1.2f;
		for (int n = _stackTop - 1; n >= 0; --n)
		{
			_curIdx = _tempBuff[n];
			DrawOneFrame(_curIdx, _clr_weight, rcpMaxAccel, rcpMaxVel, false);
			_clr_weight -= 0.25f;
		}

		/// 예측 방향
		for (int k = 0; k < m_PredictionStatusPool.Num(); ++k)
		{
			DrawOnePredictionFrame(k);
		}

		// 개별
		_dirLoc = GetActorLocation() + FVector(0.f, 0.f, 15.f);
		// Input
		UKismetSystemLibrary::DrawDebugArrow(GetOwner()
											 , _dirLoc
											 , _dirLoc + (GetLastInputVector() * 100.f)
											 , 150.f
											 , FLinearColor(FColor::Green)
											 , 0.f
											 , 5.f);
	}
}

void USFCharacterMovementComponent::DrawOneFrame(int cursor, float ClrWeight, float rcpMaxAccel, float rcpMaxVel, bool IsFrist)
{
	auto& _cur = GetStatus(cursor);

	FVector _unitLoc = _cur.Location - FVector(0.f, 0.f, 75.f);
	FVector _unitVelLoc = _cur.Location - FVector(0.f, 0.f, 65.f);
	FVector _unitDirLoc = _cur.Location + FVector(0.f, 0.f, 10.f);

	FVector _unitLoc2 = _cur.Location - FVector(0.f, 0.f, 80.f);
	FVector _dirLoc = _cur.Location + FVector(0.f, 0.f, 60.f);

	// LookAt ( Unit Rotation )
	if (DrawDebugRotate)
	{
		UKismetSystemLibrary::DrawDebugArrow(GetOwner()
											 , _unitLoc2
											 , _unitLoc2 + (_cur.BodyForward * 100.f)
											 , 150.f
											 , (IsFrist)
											 ? FLinearColor(0.8f, 0.8f, 0.8f)
											 : FLinearColor(1.f * ClrWeight, 0.3f * ClrWeight, 0.06f * ClrWeight)
											 , 0.f
											 , 5.0f);
	}

	if (DrawDebugAccelVelociyStatus)
	{
		// Accel
		UKismetSystemLibrary::DrawDebugArrow(GetOwner()
											 , _unitLoc
											 , _unitLoc + (_cur.Acceleration * rcpMaxAccel)
											 , 150.f
											 , FLinearColor(1.f * ClrWeight, 0.f, 0.f)
											 , 0.f
											 , 5.f);

		FLinearColor _clr = (_cur.SurfaceType == ESurfaceType::EST_InAir)
			? (_cur.Velocity.Z < -0.f)
			? FLinearColor(FColor::Purple) * ClrWeight : FLinearColor(FColor::Cyan) * ClrWeight
			: FLinearColor(0.f, 0.f, 1.f * ClrWeight);

		// Velocity
		UKismetSystemLibrary::DrawDebugArrow(GetOwner()
											 , _unitVelLoc
											 , _unitVelLoc + (_cur.Velocity * rcpMaxVel)
											 , 150.f
											 , _clr
											 , 0.f
											 , 5.f);
	}

	if (DrawDebugInputDirStatus)
	{
		// Input
		UKismetSystemLibrary::DrawDebugArrow(GetOwner()
											 , _unitDirLoc
											 , _unitDirLoc + (_cur.InputVector * 100.f)
											 , 150.f
											 , FLinearColor(FColor::Black)
											 , 0.f
											 , 5.f);
	}
}

void USFCharacterMovementComponent::DrawOnePredictionFrame(int idx)
{
	auto& _cur = *m_PredictionStatusPool[idx];

	if (DrawDebugRotate)
	{
		FVector _start = _cur.InputVector + FVector(0.f, 0.f, 20.f);
		FVector _end = _cur.Location + FVector(0.f, 0.f, 20.f);

		UKismetSystemLibrary::DrawDebugArrow(GetOwner()
											 , _start
											 , _end
											 , 150.f
											 , FLinearColor(0.25f, 0.8f, 0.8f)
											 , 0.f
											 , 5.0f);
	}
}



bool USFCharacterMovementComponent::DoJump(bool bReplayingMoves)
{
	// 탑 포지션 강제 발동
	bNotifyApex = true;

	bool bRet = Super::DoJump(bReplayingMoves);

	// 점프 시작 MovSensor에 알림
	if (CharacterOwner && MovSensor)
	{
		MovSensor->DoJump(CharacterOwner);
	}

	return bRet;
}

void USFCharacterMovementComponent::NotifyJumpApex()
{
	Super::NotifyJumpApex();

	if (CharacterOwner && MovSensor)
	{
		MovSensor->UpatePredictedLand(CharacterOwner);
	}
}


///
///
/// 인터페이스 메소드 구현부
///
///
/// 
/// [9/25/2022 안석] 임시용. 컴포넌트로 분리된 기능들 통합 과정에서 사용을 위한 메소드
void USFCharacterMovementComponent::SetStateFlag_Implementation(FName StateName, bool flag)
{
	if(!MovSensor)
		return;

	if (StateName == FName(TEXT("IsSwimCheck"))) {
		MovSensor->IsSwimCheck = flag;
		SwimProperties.bIsSwimCheck = flag;
	}
	else if (StateName == FName(TEXT("IsClimbing"))) {
		MovSensor->IsClimbing = flag;
	}
	else if (StateName == FName(TEXT("IsSwimming"))) {
		MovSensor->IsSwimming = flag;
	}
}

/// [9/25/2022 안석] State 이름으로 상태값 알아오기
bool USFCharacterMovementComponent::GetStateFlag_Implementation(FName StateName, bool& StateFlag)
{
	if (!MovSensor)
		return false;

	if (StateName == FName(TEXT("IsSwimCheck"))) {
		StateFlag = MovSensor->IsSwimCheck;
		return true;
	}
	else if (StateName == FName(TEXT("IsClimbing"))) {
		StateFlag = MovSensor->IsClimbing;
		return true;
	}
	else if (StateName == FName(TEXT("IsSwimming"))) {
		StateFlag = MovSensor->IsSwimming;
		return true;
	}

	return false;
}

/// [9/25/2022 안석] State 통합 명령. 임시. argument 는 string parsing 필요
void USFCharacterMovementComponent::DoStateCommand_Implementation(FName StateCommand, const FString& StrArgs)
{
	if( !MovSensor)
		return;

	if (StateCommand == FName(TEXT("StartSwimCheck"))) {
		MovSensor->IsSwimCheck = true;

		ISFEntity::Execute_SetEntityLocalBoolean(CharacterOwner->GetPlayerState()
												 , FName(TEXT("IsSwimCheck")), true);
	}
	else if (StateCommand == FName(TEXT("StopSwimCheck"))) {
		MovSensor->IsSwimCheck = false;
		SwimProperties.SwimState = EA02SwimState::Walk;

		ISFEntity::Execute_SetEntityLocalBoolean(CharacterOwner->GetPlayerState()
												 , FName(TEXT("IsSwimCheck")), false);
		ISFEntity::Execute_SetEntityLocalName(CharacterOwner->GetPlayerState()
												, FName(TEXT("SwimState"))
												, FName(TEXT("Walk")));
	}
	else if (StateCommand == FName(TEXT("UpdateSwimState"))) {
		CheckWaterSurface(0.016f);	// 임시 dt
	}
}