﻿#include "SFMovSensor.h"
#include "SFCCharacterSystem.h"

#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"

#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "CollisionQueryParams.h"

#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFOperandPoolSubsystem.h"



bool FSFSensorSource::DoTrace(const TObjectPtr<ACharacter>& CharOwner, FVector& HitImpactPoint)
{
	FCollisionQueryParams collision_params;
	collision_params.AddIgnoredActor(Cast<AActor>(CharOwner));
	FHitResult hitResult;

	FName profileName = CharOwner->GetCapsuleComponent()->GetCollisionProfileName();

	bool bRet = false;

	if (profileName != FName(TEXT("Custom"))) {
		// 라이딩 중,
		bRet = CharOwner->GetWorld()->LineTraceSingleByProfile(hitResult
															, SensorStart
															, SensorEnd
															, profileName
															, collision_params);
	}

	HitImpactPoint = ( bRet ) ? hitResult.ImpactPoint : SensorEnd;

	HitPoint = HitImpactPoint;
	IsHit = bRet;

	return bRet;
}

ESFReachSensorHitType FSFSensorSource::DoSphereTrace(const TObjectPtr<ACharacter>& CharOwner, float fRadius, FVector& HitImpactPoint)
{
	FCollisionQueryParams collision_params;
	collision_params.AddIgnoredActor(CharOwner);
	FHitResult hitResult;

	bool bRet = false;
	FName profileName = CharOwner->GetCapsuleComponent()->GetCollisionProfileName();

	if( profileName != FName(TEXT("Custom")))
	{
		bRet = CharOwner->GetWorld()->SweepSingleByProfile(hitResult
			, SensorStart, SensorEnd, FQuat::Identity
			, profileName
			, FCollisionShape::MakeSphere(fRadius)
			, collision_params );

		if (bRet)
		{
			if( hitResult.ImpactPoint.Equals(SensorStart, 0.05f) )	// 등반으로
			{
				HitImpactPoint = SensorStart;
				HitPoint = HitImpactPoint;
				ReachHitType = ESFReachSensorHitType::ERSHIT_Climb;
				return ESFReachSensorHitType::ERSHIT_Climb;
			}
			else
			{
				HitImpactPoint = hitResult.ImpactPoint;
				HitPoint = HitImpactPoint;
				ReachHitType = ESFReachSensorHitType::ERSHIT_Pullup;
				return ESFReachSensorHitType::ERSHIT_Pullup;
			}
		}
	}

	HitImpactPoint = SensorStart;
	HitPoint = HitImpactPoint;
	ReachHitType = ESFReachSensorHitType::ERSHIT_None;
	return ReachHitType;
}

ESFLandSensorHitType FSFSensorSource::DoDownSphereTrace(const TObjectPtr<ACharacter>& CharOwner, float fRadius, FVector& HitImpactPoint)
{
	FCollisionQueryParams collision_params;
	collision_params.AddIgnoredActor(CharOwner);
	FHitResult hitResult;

	bool bRet = false;
	FName profileName = CharOwner->GetCapsuleComponent()->GetCollisionProfileName();

	if (profileName != FName(TEXT("Custom")))
	{
		bRet = CharOwner->GetWorld()->SweepSingleByProfile(hitResult
														, SensorStart, SensorEnd, FQuat::Identity
														, profileName
														, FCollisionShape::MakeSphere(fRadius)
														, collision_params);
	}

	if (bRet)
	{
		HitImpactPoint = hitResult.ImpactPoint;
		HitPoint = HitImpactPoint;
		IsHit = bRet;
		LandHitType = ESFLandSensorHitType::ELSHIT_Surface;	// 속성에 따라 분류하기.
	}
	else
	{
		HitImpactPoint = SensorEnd;
		HitPoint = SensorEnd;
		IsHit = false;
		LandHitType = ESFLandSensorHitType::ELSHIT_None;
	}

	return LandHitType;
}


USFMovSensor::USFMovSensor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, bJumpUpside(false)
	, bJumpInput(false)
	, bJumpDownside(false)
	, bFallDown(false)
{
}

void USFMovSensor::PostInitProperties()
{
	Super::PostInitProperties();
	SF_LOG(Warning,TEXT("MovSensor PostInitProperties"));
}

void USFMovSensor::BeginDestroy()
{
	Super::BeginDestroy();
	OnSelfDisconnectFromGOP();
}


void USFMovSensor::OnSelfConnectToGOP(AActor* Owner)
{
	// Game Instance 얻어서 처리하기
	UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(Owner);

	if (GameInstance)
	{
		USFOperandPoolSubsystem* pOperandPoolSubSystem =
			GameInstance->GetSubsystem<USFOperandPoolSubsystem>();

		if (pOperandPoolSubSystem)
		{
			FSFOpVariant _opVar;
			_opVar.objectData = this;
			_opVar.operandType = ESFOpHandleType::EOHT_Object;

			pOperandPoolSubSystem->PushVariant_Implementation(
				FName(TEXT("MovSensor")), _opVar);

			SF_LOG(Warning, TEXT("GOP 'MovSensor' Created"));

			OwnActor = Owner;
		}
	}
}

void USFMovSensor::OnSelfDisconnectFromGOP()
{
	UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(OwnActor);

	if (GameInstance)
	{
		USFOperandPoolSubsystem* pOperandPoolSubSystem =
			GameInstance->GetSubsystem<USFOperandPoolSubsystem>();

		if (pOperandPoolSubSystem)
		{
			pOperandPoolSubSystem->ReleaseHandle_Implementation(FName(TEXT("MovSensor")));
			SF_LOG(Warning,TEXT("GOP 'MovSensor' Release"));
		}
	}
}


void USFMovSensor::CreatePredictionBuffer(int count)
{
	PredictedLocations.Empty();
	PredictedLocations.AddZeroed(count);
}

void USFMovSensor::SetPredictedLoc(int idx, const FVector vLoc)
{
	if( idx < PredictedLocations.Num() )
		PredictedLocations[idx] = vLoc;
}

void USFMovSensor::SetPredictedLandLoc(const FVector& vActorLoc, const FVector& vLoc)
{
	bDirtyFlagForPredictedLand = true;
	JumpStartLocation = vActorLoc;
	PredictedLandLocation = vLoc;
}

void USFMovSensor::DoJump(const TObjectPtr<ACharacter>& CharOwner)
{
	if (CharOwner && !bJumpInput)
	{
		bJumpInput = true;
		bJumpUpside = true;
		JumpStartLocation = CharOwner->GetActorLocation();

		/// LastMovState = ESFMovState::EMovState_Jump;
		SF_LOG(Warning,TEXT("SFMovState : Jump"));

		if (UseDebugDraw)
		{
			UKismetSystemLibrary::DrawDebugSphere(CharOwner
												  , JumpStartLocation, 20.f, 12
												  , FLinearColor(1.f, 0.17f, 0.10f), 3.f, 2.f);
		}
	}
}

void USFMovSensor::UpatePredictedLand(const TObjectPtr<ACharacter>& CharOwner)
{
	if (CharOwner)
	{
		bJumpInput = false;
		bJumpDownside = true;
		bFallDown = true;
		JumpApexLocation = CharOwner->GetActorLocation();

		// 1차 떨어지는 형태로 판단.
		// TODO: @안석 - PredictedLandLocation 에서 아래로 체크해서 Ground, Water 구분하기
		//LastMovState = ESFMovState::EMovState_Fall;
		SF_LOG(Warning, TEXT("SFMovState : Fall"));

		FVector _temp = JumpApexLocation - JumpStartLocation;
		PredictedLandLocation = JumpStartLocation + FVector( _temp.X * 2.f, _temp.Y * 2.f , 0.f);

		//bDirtyFlagForPredictedLand = true;

		if (UseDebugDraw)
		{
			// 탑 포지션 
			UKismetSystemLibrary::DrawDebugSphere(CharOwner
												  , JumpApexLocation, 20.f, 12
												  , FLinearColor(1.f, 0.17f, 0.10f), 3.f, 2.f);

			// 랜딩 포지션
			UKismetSystemLibrary::DrawDebugSphere(CharOwner
												  , PredictedLandLocation, 20.f, 12
												  , FLinearColor(1.f, 0.17f, 0.10f), 3.f, 2.f);
		}
	}
}

void USFMovSensor::OnLand(const TObjectPtr<ACharacter>& CharOwner)
{
	bJumpDownside = false;
	bFallDown = false;
}


void USFMovSensor::AdjustState(const FVector& Velocity)
{
	float fCurVelSqrt = Velocity.SizeSquared2D();

	if (FMath::Abs(Velocity.Z) > 0.01f) {

		// 이전 상태 정리
		if (JumpState == ESFJumpState::EJmpState_Start)
			JumpState = ESFJumpState::EJmpState_Upward;
		else if (JumpState == ESFJumpState::EJmpState_Apex)
			JumpState = ESFJumpState::EJmpState_Downward;

		// 점프 동작 정리
		if (bJumpUpside) {
			LastMovState = ESFMovState::EMovState_Jump;
			JumpState = ESFJumpState::EJmpState_Start;
			bJumpUpside = false;
		}
		else if (bJumpDownside) {
			LastMovState = ESFMovState::EMovState_JumpApex;
			JumpState = ESFJumpState::EJmpState_Apex;
			bJumpDownside = false;
		}
		else {
			LastMovState = ESFMovState::EMovState_Fall;
		}
	}
	else if (fCurVelSqrt > 0.f)
	{
		// 2way 선택이므로, 반대는 무조건 Jump none.
		JumpState = ESFJumpState::EJmpState_None;

		if (fCurVelSqrt > (400.f * 400.f))
			LastMovState = ESFMovState::EMovState_Run;
		else
			LastMovState = ESFMovState::EMovState_Walk;
	}
}

bool USFMovSensor::WallSensorTrace(const TObjectPtr<ACharacter>& CharOwner
								   , const FVector& vOwnerLoc
								   , const FVector& vOwnerFwd
								   , float fScale
								   , FVector& WallHitImpact)
{
	float fFwdScaleMax = 100.f * fScale;
		//FMath::Min(CharOwner->GetVelocity().Size2D() * fScale, 100.f);

	WallSensor.SensorStart = vOwnerLoc;
	WallSensor.SensorEnd = vOwnerLoc + vOwnerFwd * fFwdScaleMax;

	bool bWallHitRet = WallSensor.DoTrace(CharOwner, WallHitImpact);

	if (UseDebugDraw)
	{
		FLinearColor lineColor = (bWallHitRet) 
			? FLinearColor::Red : FLinearColor(FVector3f(1.f, 0.388f, 0.f));

		// 라인 
		UKismetSystemLibrary::DrawDebugLine(CharOwner
											, WallSensor.SensorStart
											, WallHitImpact
											, lineColor, 0.f, 1.f);

		// 쉐입
		UKismetSystemLibrary::DrawDebugSphere(CharOwner
											  , WallHitImpact
											  , CapsuleRadius * 0.5f
											  , 8, lineColor, 0.f, 1.f);
	}

	return bWallHitRet;
}

bool USFMovSensor::FootSensorTrace(const TObjectPtr<ACharacter>& CharOwner
								   , const FVector& vOwnerLoc
								   , const FVector& vOwnerFwd
								   , float fScale
								   , FVector& FootHitImpact)
{
	FootSensor.SensorStart = vOwnerLoc + (vOwnerFwd * CapsuleRadius)
		- FVector(0.f, 0.f, CapsuleHalfHeight - 50.f);
	FootSensor.SensorEnd = FootSensor.SensorStart - FVector(0.f, 0.f, 100.f);

	// Foot Sensor
	bool bFootHitRet = FootSensor.DoTrace(CharOwner, FootHitImpact);

	if (UseDebugDraw)
	{
		FLinearColor lineColor = (bFootHitRet) ? FLinearColor::Blue : FLinearColor::Red;

		UKismetSystemLibrary::DrawDebugSphere(CharOwner
											  , FootHitImpact, CapsuleRadius * 0.5f
											  , 8, lineColor, 0.f, 1.f);
	}

	return bFootHitRet;
}

void USFMovSensor::AdjustWallState(const TObjectPtr<ACharacter>& CharOwner
								   , bool WallHitRet
								   , const FVector& WallHitImpact)
{
	// 벽이 있다면?
	if (WallHitRet)
	{
		ReachSensor.SensorStart = WallHitImpact + FVector(0.f, 0.f, PullupSensorHeight);
		ReachSensor.SensorEnd = WallHitImpact;

		FVector vReachHitImpact;
		LastReachHitType = //ReachSensor.DoTrace(CharOwner, vReachHitImpact);
			ReachSensor.DoSphereTrace(CharOwner, CapsuleRadius * 0.5f, vReachHitImpact);

		// 벽 상태
		LastMovState = ESFMovState::EMovState_Climb;
		/// SF_LOG(Warning, TEXT("SFMovState : Climb"));

		if (LastReachHitType == ESFReachSensorHitType::ERSHIT_Climb)
		{
			if (UseDebugDraw)
			{
				UKismetSystemLibrary::DrawDebugSphere(CharOwner
													  , WallHitImpact + FVector(0.f, 0.f, 100.f) // vReachHitImpact
													  , CapsuleRadius * 0.5f
													  , 8
													  , FLinearColor(FVector3f(1.f, 0.f, 1.f))
													  , 0.f, 1.f);
			}
		}
		else /// if(ReachHitRet == ESFReachSensorHitType::ERSHIT_Pullup)
		{
			if (UseDebugDraw)
			{
				UKismetSystemLibrary::DrawDebugSphere(CharOwner
													  //, vWallHitImpact + FVector(0.f, 0.f, 100.f)
													  , vReachHitImpact
													  , CapsuleRadius * 0.5f
													  , 8
													  , FLinearColor::Black, 0.f, 1.f);
			}
		}
	}
	else
	{
		LastReachHitType = ESFReachSensorHitType::ERSHIT_None;
	}
}


void USFMovSensor::AdjustLandingState(const TObjectPtr<ACharacter>& CharOwner
									  , const FVector& vOwnerLoc
									  , bool FootHitRet
									  , FVector& LandHitImpact)
{
	// 날기 체크 기본
	CanGliding = false;

	// 바닥이 없다면.
	if (!FootHitRet)
	{
		int MaxCnt = FMath::Min(PredictedLocations.Num(), 5);
		float vCurHeight = vOwnerLoc.Z + CapsuleHalfHeight; // vFootHitImpact.Z;

		// 바닥 찾기
		FVector vStartLoc = FVector(PredictedLocations[0].X, PredictedLocations[0].Y, vCurHeight);
		LandSensor.SensorStart = vStartLoc;
		LandSensor.SensorEnd = vStartLoc - FVector(0.f, 0.f, 1000.f);

		// 1차 예측 지점 바닥 충돌 체크
		if (LandSensor.DoTrace(CharOwner, LandHitImpact))
		{
			if ((vCurHeight - LandHitImpact.Z) > MinimumGlidingHeight)
			{
				CanGliding = true;

				if (UseDebugDraw)
				{
					UKismetSystemLibrary::DrawDebugSphere(CharOwner
														  , vStartLoc, 20.f, 12
														  , FLinearColor::Black, 3.f, 2.f);

				}
			}
		}
		else
		{
			// 한 발치 앞 깊이가 깊은 상태.
			CanGliding = true;

			if (UseDebugDraw)
			{
				UKismetSystemLibrary::DrawDebugSphere(CharOwner
													  , vStartLoc, 20.f, 12
													  , FLinearColor::Black, 3.f, 2.f);

			}
		}

		/*
		/// 반복
		for (int i = 0; i < MaxCnt; ++i)
		{
			vStartLoc = FVector( PredictedLocations[i].X, PredictedLocations[i].Y, vCurHeight);
			LandSensor.SensorStart = vStartLoc;
			LandSensor.SensorEnd = vStartLoc - FVector(0.f, 0.f, 1000.f);

			FVector vLandHitImpact;
			ESFLandSensorHitType LandHitRet =
				LandSensor.DoDownSphereTrace(CharOwner, fDebugSphereRadius, vLandHitImpact);

			if (UseDebugDraw)
			{
				if (LandHitRet == ESFLandSensorHitType::ELSHIT_Surface)
				{

					UKismetSystemLibrary::DrawDebugLine(CharOwner
						, vStartLoc, vLandHitImpact, FLinearColor::Red, 0.f, 1.f);

					UKismetSystemLibrary::DrawDebugSphere(CharOwner
						, vLandHitImpact, fDebugSphereRadius, 8
						, FLinearColor::Red, 0.f, 1.f);
				}
				else
				{
					UKismetSystemLibrary::DrawDebugSphere(CharOwner
														  , vLandHitImpact, fDebugSphereRadius, 8
														  , FLinearColor(FVector3f(1.f,0.f,1.f))
														  , 0.f, 1.f);
				}
			}
		}
		*/
	}
}


bool USFMovSensor::OnMovementTrace(const TObjectPtr<ACharacter>& CharOwner
								   , const FVector Accel
								   , const FVector Velocity
								   , const FVector LastInput
								   , float fScale)
{
	FVector vOwnerLoc = CharOwner->GetActorLocation();
	FVector vOwnerFwd = CharOwner->GetActorForwardVector();

	// 기본 시작 상태 설정
	LastMovState = ESFMovState::EMovState_None;

	// 상태 설정
	AdjustState(Velocity);

	// 벽 상태 체크, 발 아래 상태 체크, 랜딩 체크 용
	bool bWallHitRet, bFootHitRet;
	FVector vWallHitImpact, vFootHitImpact, vLandHitImpact;

	// 벽 상태 체크
	bWallHitRet = WallSensorTrace(CharOwner, vOwnerLoc, vOwnerFwd, fScale, vWallHitImpact);
	// 바닥 상태 체크
	bFootHitRet = FootSensorTrace(CharOwner, vOwnerLoc, vOwnerFwd, fScale, vFootHitImpact);
	// 벽이 있다면?
	AdjustWallState(CharOwner, bWallHitRet, vWallHitImpact);
	// 랜딩, 날기 체크
	AdjustLandingState(CharOwner, vOwnerLoc, bFootHitRet, vLandHitImpact);


	// 점프 예측
	if (bDirtyFlagForPredictedLand)
	{
		bDirtyFlagForPredictedLand = false;

		UKismetSystemLibrary::DrawDebugLine(CharOwner
											, FVector(PredictedLandLocation.X
												, PredictedLandLocation.Y
												, JumpStartLocation.Z)
											, PredictedLandLocation, FLinearColor::Red, 2.f, 2.f);

		UKismetSystemLibrary::DrawDebugSphere(CharOwner
											  , PredictedLandLocation
											  , CapsuleRadius * 0.5f
											  , 8
											  , FLinearColor::Red, 2.f, 2.f);


	}


	return bWallHitRet;
}
