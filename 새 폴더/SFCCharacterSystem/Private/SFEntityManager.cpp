﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SFEntityManager.h"

// Add default functionality here for any ISFEntityManager functions that are not pure virtual.

USFEntityManagerComponent::USFEntityManagerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void USFEntityManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void USFEntityManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}


void USFEntityManagerComponent::RegisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity
															  , int fixedUID
															  , bool& bResult)
{
	if (!_entities.Find(iSFEntity))
	{
		/// 1) fixedUID -1인 경우 UID Generated
		int _newUID = fixedUID;

		if (fixedUID < 0)
		{
			GenerateUID_Implementation(_newUID);

			/// 1-1) Entity 에 Generated UID 설정
			ISFEntity::Execute_SetUID(iSFEntity.GetObject(), _newUID);
		}
		else
		{

			/// 1-2) fixedUID가 있다면,
			auto* pIEntity = _mapEntities.Find(_newUID);

			if (pIEntity) {
				UE_LOG(LogTemp, Error, TEXT("the Entity(%d) already exist."), _newUID);
				return;
			}
		}

		/// 2) Pool에 등록
		_entities.Add(iSFEntity);
		_mapEntities.Add(_newUID, iSFEntity);

		UE_LOG(LogTemp, Warning, TEXT("Entity(%p) UID(%d) was registered"), iSFEntity.GetObject(), _newUID);
	}
}


void USFEntityManagerComponent::UnregisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity
																, bool& bResult)
{
	if (_entities.Find(iSFEntity))
	{
		int _uid = -1;
		ISFEntity::Execute_GetUID( iSFEntity.GetObject(), _uid );

		if (_uid > 0)
		{
			_mapEntities.Remove(_uid);
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Entity UID is less than 0. (%d)"), _uid);
			return;
		}

		_entities.Remove(iSFEntity);
	}
}

void USFEntityManagerComponent::FindRegisteredEntity_Implementation(int UID
																	, TScriptInterface<ISFEntity>& iSFEntity
																	, bool& bResult)
{
	auto* pIEntity = _mapEntities.Find(UID);

	if (pIEntity)
	{
		iSFEntity = *pIEntity;
		bResult = true;
		return;
	}

	bResult = false;
}


void USFEntityManagerComponent::GenerateUID_Implementation(int& UID)
{
	UID = _generatedUID++;
}
