﻿#include "SFHFSMCondition.h"
#include "SFHFSM.h"

#include "SFCCharacterSystem.h"
#include "SFTicketProcessorSubsystem.h"

#include "Kismet/GameplayStatics.h"


/*
void USFHFSMConditionData::OnPostInit()
{
	//Super::PostInitProperties();

	//UGameInstance* GameInstance = GetGameInstance();
	UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(this);

	USFOperandPoolSubsystem* pOperandPoolSubSystem =
		GameInstance->GetSubsystem<USFOperandPoolSubsystem>();

	m_GOPSubsystem = pOperandPoolSubSystem;
}
*/

bool USFHFSMConditionCmpPlayerState::TestCondition(const TScriptInterface<ISFOperandPool>& iGOP
	, const FSFDataForTransition& dataSrc)
{
	SF_LOG(Warning, TEXT("Test Con (1)"));

	if (iGOP && iGOP.GetObject())
	{
		FSFOpVariant var;
		if ( ISFOperandPool::Execute_PeekVariant(iGOP.GetObject(), StatName, var))
		{
			if(Operator == FName(TEXT("=="))) {
				return (var.vecData.X == Value);
			}
		}
	}

	return false;
}




bool USFHFSMConditionCmpWorldState::TestCondition(const TScriptInterface<ISFOperandPool>& iGOP
	, const FSFDataForTransition& dataSrc)
{
	SF_LOG(Warning, TEXT("Test Con (2)"));

	if (iGOP && iGOP.GetObject())
	{
		FSFOpVariant var;
		if (ISFOperandPool::Execute_PeekVariant(iGOP.GetObject(), WorldStatName, var))
		{
			if (Operator == FName(TEXT("=="))) {
				return (var.vecData.X == Value);
			}
		}
	}

	return false;
}
