﻿#include "SFCCharacterSystem.h"

DEFINE_LOG_CATEGORY(LogSFCCharacterSystem);

#define LOCTEXT_NAMESPACE "FSFCCharacterSystem"

void FSFCCharacterSystem::StartupModule()
{
	SF_LOG(Warning, TEXT("SFCCharacterSystem module has been loaded"));
}

void FSFCCharacterSystem::ShutdownModule()
{
	SF_LOG(Warning, TEXT("SFCCharacterSystem module has been unloaded"));
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFCCharacterSystem, SFCCharacterSystem)
