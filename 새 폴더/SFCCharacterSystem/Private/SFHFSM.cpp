﻿
#include "SFHFSM.h"
#include "SFHFSMCondition.h"
#include "SFCCharacterSystem.h"

#include "Kismet/GameplayStatics.h"


USFHFSMState::USFHFSMState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void USFHFSMState::OnEntryByInpState_Implementation(const FSFInputStatus& inpState)
{
	SF_LOG(Warning, TEXT("SFHFSM State : OnEnterByInpState_Impl : %s "), *inpState.ToString());

	if (ChildState) 
	{
		UFunction* const func = ChildState->FindFunction(TEXT("OnEntryByInpState"));
		
		if (func)
		{
			void* buf = FMemory_Alloca(func->ParmsSize);
			FMemory::Memzero(buf, func->ParmsSize);

			for (TFieldIterator<FProperty> It(func);
				 It && It->HasAnyPropertyFlags(CPF_Parm); ++It)
			{
				FProperty* Prop = *It;
				FString strType = Prop->GetCPPType();

				if (strType == TEXT("FSFInputStatus"))
				{
					FSFInputStatus* pArg =
						Prop->ContainerPtrToValuePtr<FSFInputStatus>(buf);
					*pArg = inpState;
				}

				SF_LOG(Warning,TEXT("Func Param Type :'%s'"), *strType);
			}

			ChildState->ProcessEvent(func, buf);
		}
	}
}

void USFHFSMState::OnEntryByPlayerState_Implementation(const FSFPlayerStatus& playerState)
{

}


void USFHFSMState::OnExit_Implementation(const FSFInputStatus& inpState
											, const FSFPlayerStatus& playerState)
{

}

void USFHFSMState::OnStateUpdate_Implementation(const FSFInputStatus& inpState
											, const FSFPlayerStatus& playerState)
{
}


bool USFHFSMState::CanTransition_Implementation(const TScriptInterface<ISFOperandPool>& iGOP
			, const FSFDataForTransition& dataSrc) const
{
	bool bResult = false;

	if (Condition.Num())
	{
		bResult = true;

		for (auto* pCon : Condition)
		{
			bResult = ( pCon ) ? (bResult && pCon->TestCondition(iGOP, dataSrc)) : false;
		}
	}

	return bResult;
}


void USFHFSMState::SetNewCondition(const UClass* conClass)
{
	//Condition = NewObject<USFHFSMConditionData>(this, conClass);
}




USFHierarchicalFSMComponent::USFHierarchicalFSMComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void USFHierarchicalFSMComponent::BeginPlay()
{
	Super::BeginPlay();

	UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(this);

	USFOperandPoolSubsystem* pOperandPoolSubSystem =
		GameInstance->GetSubsystem<USFOperandPoolSubsystem>();

	m_IGOP= pOperandPoolSubSystem;

	SrcTransitionPtr = MakeShareable(new FSFDataForTransition);
}

void USFHierarchicalFSMComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	SrcTransitionPtr = nullptr;
}

// Called every frame
void USFHierarchicalFSMComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	/// 스택 구조로 쌓아서 플레이 하기
	//UpdateState(DeltaTime);
}


void USFHierarchicalFSMComponent::UpdateState(float DeltaTime)
{
	if (RootState)
	{
		FSFInputStatus _temp;
		RootState->OnEntryByInpState_Implementation(_temp);
	}
}

void USFHierarchicalFSMComponent::TestTransition()
{
	if (RootState)
	{
		bool bResult = RootState->CanTransition_Implementation(m_IGOP
			, *SrcTransitionPtr );

		SF_LOG(Warning, TEXT("Test Transition : %c"), bResult ? 'O' : 'X');
	}
}
