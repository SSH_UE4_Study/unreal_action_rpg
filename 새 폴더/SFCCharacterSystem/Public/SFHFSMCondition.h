﻿#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"

#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFOperandPoolSubsystem.h"


#include "SFMovementStatus.h"
#include "SFHFSMCondition.generated.h"


USTRUCT(BlueprintType)
struct FSFInputStatus
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFInputStatus Struct")
	bool PressMove = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFInputStatus Struct")
	bool PressJump = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFInputStatus Struct")
	bool PressDash = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFInputStatus Struct")
	bool PressInteraction = false;


	bool IsPressMove()
	{
		return PressMove;
	}

	bool IsPressAnyAction()
	{
		return (PressJump || PressDash || PressInteraction);
	}

	FString ToString() const
	{
		FString out;
		out.Append("InpStatus:(");
		out.Append(PressMove ? "Move: o, " : "Move: x, ");
		out.Append(PressJump ? "Jump: o, " : "Jump: x, ");
		out.Append(PressDash ? "Dash: o, " : "Dash: x, ");
		out.Append(PressInteraction ? "Irac: o" : "Irac: x");
		out.Append(")");
		return out;
	}
};

USTRUCT(BlueprintType)
struct FSFPlayerStatus
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFPlayerStatus Struct")
	ESurfaceType SurfaceType = ESurfaceType::EST_OnGround;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFPlayerStatus Struct")
	FVector Acceleration = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFPlayerStatus Struct")
	FVector Velocity = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFPlayerStatus Struct")
	FVector BodyForward = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFPlayerStatus Struct")
	float Slope = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFPlayerStatus Struct")
	FVector Location = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFPlayerStatus Struct")
	FVector InputVector = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFPlayerStatus Struct")
	float CurrentStamina = 0.f;
};


USTRUCT(BlueprintType)
struct FSFDataForTransition
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSFInputStatus InputStatus;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSFPlayerStatus EntityStatus;
};


DECLARE_DELEGATE_RetVal_TwoParams(bool, FTransitionChecker, FSFInputStatus&, FSFPlayerStatus&);



UCLASS(Blueprintable, BlueprintType, EditInlineNew, CollapseCategories)
class SFCCHARACTERSYSTEM_API USFHFSMConditionData : public UObject
{
	GENERATED_BODY()

public:

	UFUNCTION()
		TArray<FName> GetStatOptions() const
	{
		return { FName(TEXT("Move")), FName(TEXT("Speed")), FName(TEXT("Slop")), FName(TEXT("Height")) };
	}

	UFUNCTION()
		TArray<FName> GetOperatorOptions() const
	{
		return { FName(TEXT("==")), FName(TEXT(">")), FName(TEXT(">=")), FName(TEXT("<"))
			,FName(TEXT("<=")) ,FName(TEXT("!=")), FName(TEXT("AND")), FName(TEXT("OR"))
		};
	}

	virtual bool TestCondition(const TScriptInterface<ISFOperandPool>& iGOP, const FSFDataForTransition& dataSrc ) { return false; }
};

UCLASS(Blueprintable, BlueprintType, EditInlineNew, CollapseCategories)
class SFCCHARACTERSYSTEM_API USFHFSMConditionCmpPlayerState : public USFHFSMConditionData
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, meta = (GetOptions = "GetStatOptions"))
		FName StatName;

	UPROPERTY(EditDefaultsOnly, meta = (GetOptions = "GetOperatorOptions"))
		FName Operator;

	UPROPERTY(EditDefaultsOnly)
		float Value;

	virtual bool TestCondition(const TScriptInterface<ISFOperandPool>& iGOP, const FSFDataForTransition& dataSrc) override;
};


UCLASS(Blueprintable, BlueprintType, EditInlineNew, CollapseCategories)
class SFCCHARACTERSYSTEM_API USFHFSMConditionCmpWorldState : public USFHFSMConditionData
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, meta = (GetOptions = "GetWorldStatOptions"))
		FName WorldStatName;

	UPROPERTY(EditDefaultsOnly, meta = (GetOptions = "GetOperatorOptions"))
		FName Operator;

	UPROPERTY(EditDefaultsOnly)
		float Value;

	UFUNCTION()
		TArray<FName> GetWorldStatOptions() const
	{
		return { FName(TEXT("Time")), FName(TEXT("Weather")), FName(TEXT("Height")), FName(TEXT("Depth")) };
	}

	virtual bool TestCondition(const TScriptInterface<ISFOperandPool>& iGOP, const FSFDataForTransition& dataSrc) override;
};
