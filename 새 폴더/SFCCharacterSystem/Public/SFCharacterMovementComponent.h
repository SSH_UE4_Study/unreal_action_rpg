﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "SFMovementStatus.h"
#include "SFMovSensor.h"
#include "SFEntity.h"

#include "SFCharacterMovementComponent.generated.h"

class USFCharacterMovementComponent;

UENUM(BlueprintType)
enum EA02SwimState
{
	// 무릎 아래까지 찬 물 위를 걷는 상태. Idle과의 차이점 없음 [6/24/2022 eunji.you]
	Walk,
	// 가슴께 아래까지 찬 물 위를 걷는 상태. 질주, 앉기가 불가능 [6/23/2022 eunji.you]
	WaterWalking,
	// Swim And SwimSprint [6/24/2022 eunji.you]
	Swim,
};


USTRUCT(BlueprintType)
struct FSFSwimProperties
{
	GENERATED_BODY()
public:

	FSFSwimProperties();

	// 발 위치의 Bone. 수심 체크 로직에 사용합니다. [6/23/2022 eunji.you]
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SFSwimProperties")
	FName LeftFootName;

	// 무릎 위치의 Bone. 무릎 위치와 비슷한 Calf Bone으로 지정한 상황입니다. [6/23/2022 eunji.you]
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SFSwimProperties")
	FName LeftKneeBoneName;

	// 가슴께 위치의 Bone. Neck Bone으로 지정하는 상황입니다. [6/23/2022 eunji.you]
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SFSwimProperties")
	FName ChestBoneName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SFSwimProperties")
	TEnumAsByte<EPhysicalSurface> WaterSurfaceType;

	// GroundSurfaceType. 현재는 Default [6/23/2022 eunji.you]
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SFSwimProperties")
	TEnumAsByte<EPhysicalSurface> GroundSurfaceType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SFSwimProperties")
	TEnumAsByte<ETraceTypeQuery> SwimTraceChannel;

	// MovementMode 강제 변환 시 어색한 부분 처리하기 위해 부력 조절하기 때문에 기존 부력수치 저장 [6/23/2022 eunji.you]
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFSwimProperties")
	float fSwimBuoyancy;

	// 높은 곳에서 떨어지는 경우에 잠시 수심 체크를 멈추는 역할 [7/8/2022 eunji.you]
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFSwimProperties")
	float fStopCheckTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFSwimProperties")
	float TurnWeight = 3.0f;

	// 수심을 체크하는 범위(높이) 입니다. 이 이상 차이나는 수면은 감지할 수 없습니다. [6/23/2022 eunji.you]
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFSwimProperties")
	float fWaterTraceMaxHeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFSwimProperties")
	bool IsSwimToClimb;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFSwimProperties")
	bool bIsSwimCheck;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFSwimProperties")
	bool ShowDebugTrace;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFSwimProperties")
	TEnumAsByte<EA02SwimState> SwimState;

	UPROPERTY(BlueprintReadOnly)
	float fDistanceFootToKnee;

	UPROPERTY(BlueprintReadOnly)
	float fDistanceFootToChest;


	void SetPropertiesByOwner(const TScriptInterface<ISFPawn>& IOwner);
};


// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFPawnMovementAdapter : public UInterface
{
	GENERATED_BODY()
};


class SFCCHARACTERSYSTEM_API ISFPawnMovementAdapter
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SF PawnMovementAdapter")
	bool GetMovement(USFCharacterMovementComponent*& RetMovement);
};


/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SFCCHARACTERSYSTEM_API USFCharacterMovementComponent 
	: public UCharacterMovementComponent
	, public ISFPawnMovementAdapter
	, public ISFMovSensorAdapter
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void TickComponent(float delta_time, ELevelTick tick_type, FActorComponentTickFunction* tick_function) override;
	virtual bool DoJump(bool bReplayingMoves) override;
	virtual void NotifyJumpApex() override;
	//virtual bool IsFalling() const override;

	virtual void ProcessLanded(const FHitResult& Hit, float remainingTime, int32 Iterations) override;
	virtual void Launch(FVector const& LaunchVel) override;

	float StatusUpdateTime = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement")
	float PullupSensorHeight = 150.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement")
	int MaxTrajectoryCount = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement")
	int MaxPredictionCount = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement", meta = (DisplayName = "Status record Update Cycle"))
	float StatusUpdateCycleLimit = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement|DebugDraw")
	bool DrawDebugForMoveStatus = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement|DebugDraw")
	bool DrawDebugAccelVelociyStatus = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement|DebugDraw")
	bool DrawDebugRotate = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement|DebugDraw")
	bool DrawDebugPrediction = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement|DebugDraw")
	bool DrawDebugInputDirStatus = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement|DebugDraw")
	TObjectPtr<USFMovSensor> MovSensor;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement|수영")
	float OwnerCapsuleHalfHeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFCharacter Movement|수영")
	FSFSwimProperties	SwimProperties;
	




	TArray<TSharedPtr<FSFMovementStatus>> m_MoveStatusPool;

	// 전방 예측 센서
	TArray<TSharedPtr<FSFMovementStatus>> m_PredictionStatusPool;

	int m_CurStatus;
	int m_StatusMax;

	FORCEINLINE int GetPrevStatusIdx();
	FORCEINLINE int GetLastStatusIdx();
	FORCEINLINE int GetPrevStatusWithBase(int base);
	FORCEINLINE void UpdateStatusIdx();
	FORCEINLINE FSFMovementStatus& GetStatus(int cursor);

	void CreateMoveStatusPool(int count);
	void UpdateMovStatus(float delta_time);

	void CheckWaterSurface(float delta_time);
	void ControlMovementModeBySwimState();


	void StoreCurrentStatus();
	void MakePredictionStatus();

	FORCEINLINE void GetDirToSinCos(FVector vWorldDir, float& sn, float& cs);
	FORCEINLINE FVector GetLocalVel(float sn, float cs, FVector WorldVel);
	FORCEINLINE FVector GetWorldVel(float sn, float cs, FVector LocalVel);


	void DrawDebugInfo();
	void DrawOneFrame(int cursor, float ClrWeight, float rcpMaxAccel, float rcpMaxVel, bool IsFrist);

	void DrawOnePredictionFrame(int idx);

private:
	bool CustomFindFloor(FFindFloorResult& out_floor_result, const FVector start, const FVector end);

	bool FindPredictionFloor(const FVector BaseLoc, FVector& HitImpactPoint);

	float _logUpdateTime = 0.f;

public:
	UFUNCTION(BlueprintCallable, Category = "SFCharacter Movement")
	const FSFMovementStatus& GetCurMoveStatus();

	UFUNCTION(BlueprintCallable, Category = "SFCharacter Movement")
	const FSFMovementStatus& GetPrevMoveStatus();

	UFUNCTION(BlueprintCallable, Category = "SFCharacter Movement")
	const FSFMovementStatus& GetLastMoveStatus();

	bool GetMovSensor_Implementation(USFMovSensor*& RetMovSensor)
	{
		RetMovSensor = MovSensor.Get();
		return true;
	};

	bool GetMovement_Implementation(USFCharacterMovementComponent*& RetMovement)
	{
		RetMovement = this;
		return true;
	}

	/// [9/25/2022 안석]
	/// MovSensorAdapter interface implementations
	void SetStateFlag_Implementation(FName StateName, bool flag);
	bool GetStateFlag_Implementation(FName StateName, bool& StateFlag);
	void DoStateCommand_Implementation(FName StateCommand, const FString& StrArgs);

};
