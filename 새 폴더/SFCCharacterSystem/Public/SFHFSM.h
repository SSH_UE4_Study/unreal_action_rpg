﻿#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"

#include "SFMovementStatus.h"
#include "SFHFSMCondition.h"

#include "SFOperand.h"
#include "SFOperandPool.h"
#include "SFOperandPoolSubsystem.h"


#include "SFHFSM.generated.h"

class USFHFSMConditionData;
class USFHFSMState;


//UCLASS()
//class SFCCHARACTERSYSTEM_API USFSMTransitionPair :

//USTRUCT(Atomic, BlueprintType)
//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FSFStateTransitionPair Struct")

// public UDataAsset

UCLASS(BlueprintType)
class SFCCHARACTERSYSTEM_API USFHFSMState : public UDataAsset
{
	GENERATED_BODY()

public:
	USFHFSMState(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFHFSM State")
	FGameplayTag CurrentStateTag;

	//, meta = (GetOptions = "GetConOptions")

	UPROPERTY(EditAnywhere, Instanced, Category = "SFHFSM State")
	TArray<class USFHFSMConditionData*> Condition;

	// class USFHFSMConditionData* Condition;

	UFUNCTION()
	TArray<class USFHFSMConditionData*> GetConOptions() const
	{
		TArray<USFHFSMConditionData*> temp;
		temp.Add(NewObject<USFHFSMConditionData>());
		return temp;
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFHFSM State")
	USFHFSMState* ChildState;
	
	UFUNCTION(BlueprintNativeEvent, Category = "SFHFSM State")
	void OnEntryByInpState(const FSFInputStatus& inpState);
	virtual void OnEntryByInpState_Implementation(const FSFInputStatus& inpState);

	UFUNCTION(BlueprintNativeEvent, Category = "SFHFSM State")
	void OnEntryByPlayerState(const FSFPlayerStatus& playerState);
	virtual void OnEntryByPlayerState_Implementation(const FSFPlayerStatus& playerState);

	UFUNCTION(BlueprintNativeEvent, Category = "SFHFSM State")
	void OnExit(const FSFInputStatus& inpState, const FSFPlayerStatus& playerState);
	virtual void OnExit_Implementation(const FSFInputStatus& inpState, const FSFPlayerStatus& playerState);

	UFUNCTION(BlueprintNativeEvent, Category = "SFHFSM State")
	void OnStateUpdate(const FSFInputStatus& inpState, const FSFPlayerStatus& playerState);
	virtual void OnStateUpdate_Implementation(const FSFInputStatus& inpState, const FSFPlayerStatus& playerState);

	UFUNCTION(BlueprintNativeEvent, Category = "SFHFSM State")
	bool CanTransition(const TScriptInterface<ISFOperandPool>& iGOP, const FSFDataForTransition& dataSrc) const;
	virtual bool CanTransition_Implementation(const TScriptInterface<ISFOperandPool>& iGOP, const FSFDataForTransition& dataSrc) const;

	/// Exit 조건

	/// 생성 관련
	void SetNewCondition(const UClass* conClass);
};


struct FSFStateTransitionPair
{
	FTransitionChecker TransitionOperatior;
	USFHFSMState NextState;
};



UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SFCCHARACTERSYSTEM_API USFHierarchicalFSMComponent
	: public UActorComponent

{
	GENERATED_BODY()

protected:

	TScriptInterface<ISFOperandPool> m_IGOP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "SFHierarchicalFSM Component")
	USFHFSMState* RootState;

	//UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "SFHierarchicalFSM Component")
	//FSFDataForTransition SrcTransition;

	//UPROPERTY()
	TSharedPtr<FSFDataForTransition> SrcTransitionPtr;

public:
	USFHierarchicalFSMComponent();

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "SFHierarchicalFSM Component")
	void UpdateState(float DeltaTime);

	UFUNCTION(BlueprintCallable, Category = "SFHierarchicalFSM Component")
	void TestTransition();
};
