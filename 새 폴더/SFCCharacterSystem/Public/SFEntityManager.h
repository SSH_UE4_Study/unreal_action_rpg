﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "SFEntity.h"

#include "SFEntityManager.generated.h"

#define SFENTITY_UID_BEGIN	1000

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class USFEntityManager : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SFCCHARACTERSYSTEM_API ISFEntityManager
{
	GENERATED_BODY()

	//TSet<TScriptInterface<ISFEntity>> _entities;
	//TMap<int, TScriptInterface<ISFEntity>> _mapEntities;

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	/// <summary>
	/// 엔티티 등록하기, fixedUID - 고정 UID, 외부에서 고정 UID를 사용하지 않을 경우 GenerateUID로 생성 후 등록.
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01 ISFEntity Manager")
	void RegisterEntity(const TScriptInterface<ISFEntity>& iSFEntity, int fixedUID, bool& bResult);
	//virtual void RegisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity, int fixedUID, bool& bResult) = 0;

	/// <summary>
	/// iSFEntity와 동일한 엔티티 등록 해제하기
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01 ISFEntity Manager")
	void UnregisterEntity(const TScriptInterface<ISFEntity>& iSFEntity, bool& bResult);
	//virtual void UnregisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity, bool& bResult) = 0;

	/// <summary>
	/// UID로 등록된 SFEntity 찾기
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01 ISFEntity Manager")
	void FindRegisteredEntity(int UID, TScriptInterface<ISFEntity>& iSFEntity, bool& bResult);
	//virtual void FindRegisteredEntity_Implementation(int UID, TScriptInterface<ISFEntity>& iSFEntity, bool& bResult) = 0;

	/// <summary>
	/// 내부 설정에 의한 Unique ID 생성하기
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01 ISFEntity Manager")
	void GenerateUID(int& UID);
	//virtual void GenerateUID_Implementation(int& UID) = 0;
};



UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SFCCHARACTERSYSTEM_API USFEntityManagerComponent : public UActorComponent, public ISFEntityManager
{
	GENERATED_BODY()

protected:

	/// <summary>
	/// 자동으로 생성되는 UID 의 시작점. 1000 이하는 직접 고정된 값을 설정
	/// </summary>
	int _generatedUID = SFENTITY_UID_BEGIN;

	TSet<TScriptInterface<ISFEntity>> _entities;

	UPROPERTY(Transient, BlueprintReadOnly, Category = "ISFEntity Manager Interface|Properties")
	TMap<int, TScriptInterface<ISFEntity>> _mapEntities;

public:

	USFEntityManagerComponent();

protected:
	virtual void BeginPlay() override;

public:

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFEntity Manager Interface")
	//void RegisterEntity(const TScriptInterface<ISFEntity>& iSFEntity, int fixedUID, bool& bResult);
	virtual void RegisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity, int fixedUID, bool& bResult) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFEntity Manager Interface")
	//void UnregisterEntity(const TScriptInterface<ISFEntity>& iSFEntity, bool& bResult);
	virtual void UnregisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity, bool& bResult) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFEntity Manager Interface")
	//void FindRegisteredEntity(int UID, TScriptInterface<ISFEntity>& iSFEntity, bool& bResult);
	virtual void FindRegisteredEntity_Implementation(int UID, TScriptInterface<ISFEntity>& iSFEntity, bool& bResult) override;

	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ISFEntity Manager Interface")
	//void GenerateUID(int& UID);
	virtual void GenerateUID_Implementation(int& UID) override;
};
