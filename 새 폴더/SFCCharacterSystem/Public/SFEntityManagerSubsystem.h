﻿#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"

#include "SFEntity.h"
#include "SFEntityManager.h"

#include "SFTicketExecutor.h"

#include "SFEntityManagerSubsystem.generated.h"

UCLASS(DisplayName = "SFC-003.01 SFEntityManager")
class SFCCHARACTERSYSTEM_API USFEntityManagerSubsystem
	: public UGameInstanceSubsystem
	, public ISFEntityManager
	, public ISFTicketExecutor
{
	GENERATED_BODY()

protected:

	/// <summary>
	/// 자동으로 생성되는 UID 의 시작점. 1000 이하는 직접 고정된 값을 설정
	/// </summary>
	int _generatedUID = SFENTITY_UID_BEGIN;

	TSet<TScriptInterface<ISFEntity>> _entities;

	UPROPERTY()
	TMap<int, TScriptInterface<ISFEntity>> _mapEntities;


	UPROPERTY()
	FName ExecutorName;


	UPROPERTY()
	TSet<FName>	_processableTicketNames;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDele_OnExecuteSFTicketForMng, const TScriptInterface<ISFTicket>&, iTicket, int, OwnerUID);


	UPROPERTY(BlueprintAssignable, Category = "SFC-003.01 SFEntityManager")
	FDele_OnExecuteSFTicketForMng Func_OnExecuteSFTicketForMng;

public:

	// Begin
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;


	//UFUNCTION(BlueprintCallable, Category = "SFC-003.01 SFEntityManager")
	//void RegisterEntity(const TScriptInterface<ISFEntity>& iSFEntity, int fixedUID, bool& bResult);

	//UFUNCTION(BlueprintCallable, Category = "SFC-003.01 SFEntityManager")
	//void UnregisterEntity(const TScriptInterface<ISFEntity>& iSFEntity, bool& bResult);

	//UFUNCTION(BlueprintCallable, Category = "SFC-003.01 SFEntityManager")
	//void FindRegisteredEntity(int UID, TScriptInterface<ISFEntity>& iSFEntity, bool& bResult);

	//UFUNCTION(BlueprintCallable, Category = "SFC-003.01 SFEntityManager")
	//void GenerateUID(int& UID);

	virtual void RegisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity, int fixedUID, bool& bResult) override;
	virtual void UnregisterEntity_Implementation(const TScriptInterface<ISFEntity>& iSFEntity, bool& bResult) override;
	virtual void FindRegisteredEntity_Implementation(int UID, TScriptInterface<ISFEntity>& iSFEntity, bool& bResult) override;
	virtual void GenerateUID_Implementation(int& UID) override;


	// 실행기 이름 설정, 가져오기
	virtual void SetExecutorName_Implementation(FName name) override;
	virtual void GetExecutorName_Implementation(FName& name) override;


	// 실행기에 연결할 명령어(태그) 리스트 등록
	virtual void RegisterTickets_Implementation(const TArray<FName>& events) override;

	// 실행기 명령어 연결 해제
	virtual void UnregisterTickets_Implementation(const TArray<FName>& events) override;

	// 등록된 태그 리스트
	virtual int GetRegisteredTickets_Implementation(TArray<FName>& registerEvents) override;

	// 티겟 실 처리 부분
	virtual void ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket) override;
};
