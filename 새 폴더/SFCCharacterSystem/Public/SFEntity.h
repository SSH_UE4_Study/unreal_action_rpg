﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Math/Vector.h"
#include "UObject/Interface.h"
#include "GameplayTagContainer.h"
#include "SFEntity.generated.h"

//#define SFINTERFACE(MethodName) \
//	void ##MethodName##(); \
//	virtual void ##MethodName##_Implementation() = 0;
//
//#define SFINTERFACE_OneParam(MethodName, ... ) \
//	void MethodName(__VA_ARGS__); \
//	virtual void MethodName_Implementation(__VA_ARGS__) = 0;
class APlayerState;
class UCharacterMovementComponent;
class USFCharacterMovementComponent;
class USFMovSensor;


UINTERFACE(MinimalAPI, Blueprintable)
class USFResponse : public UInterface
{
	GENERATED_BODY()
};

class SFCCHARACTERSYSTEM_API ISFResponse
{
	GENERATED_BODY()
	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.05 ISFResponse")
	void Response(FName type, const FString& ResString);
};



// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFEntity : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SFCCHARACTERSYSTEM_API ISFEntity
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	void SetUID(int uid);
	//virtual void SetUID_Implementation(int uid) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	void GetUID(int& uid);
	//virtual void GetUID_Implemention(int& uid) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	void SetPartyID(int partyId);
	//virtual void SetPartyID_Implementation(int partyId) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	void GetPartyID(int& partyId);
	//virtual void GetPartyID_Implementation(int& partyId) = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	bool GetEntityObject(AActor*& RealActor);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	bool PerformStringCmd(FName Command, const FString& ArgString);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	bool PerformStringCmdWithResponse(FName Command, const FString& ArgString, const TScriptInterface<ISFResponse>& iResponser);

	/// <summary>
	/// A02용 추가 인터페이스
	/// </summary>
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	bool GetEntityMovement(UCharacterMovementComponent*& CharMovCom);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	bool GetEntityMovSensor(USFMovSensor*& MovSensor);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	bool GetEntityState(APlayerState*& PlayerState);

	//
	// [9/25/2022 안석] A01 상태 정리용.
	//
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	void SetEntityLocalVariable(FName StatName, float value);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	float GetEntityLocalVariable(FName StatName);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	void SetEntityLocalBoolean(FName StatName, bool flag);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	bool GetEntityLocalBoolean(FName StatName);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	void SetEntityLocalName(FName StatName, FName ConstName);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	FName GetEntityLocalName(FName StatName);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	void SetEntityLocalVector(FName StatName, FVector vector);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.01 ISFEntity")
	FVector GetEntityLocalVector(FName StatName);

};


// 무브먼트 변경을 위한 변환 과정용 임시 상태정보
UENUM(BlueprintType)
enum class ESFPawnState : uint8
{
	None = 0,
	Idle,
	Walk,
	Run,
	Jump,
	Climbing,
	Falling,
	Diving,
	Swimming,
	Gliding,
	Riding,
};


//
//
// SFHero를 가져오기 위한 공통 인터페이스
//
//
UINTERFACE(MinimalAPI, Blueprintable)
class USFPawn : public UInterface
{
	GENERATED_BODY()
};

class SFCCHARACTERSYSTEM_API ISFPawn
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.06 ISFPawn")
	ESFPawnState GetPawnState();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.06 ISFPawn")
	bool IsPawnState(ESFPawnState state);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.06 ISFPawn")
	bool GetCharacter(ACharacter*& OwnChar);

	/// 옛 방식 액션 호출
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.06 ISFPawn")
	void CallSendActionEvent(FGameplayTag EventTag);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-003.01.06 ISFPawn")
	void PerformStringPawnCmd(FName Command, const FString& ArgString);
};