﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UObject/Interface.h"
#include "Engine/DataAsset.h"

#include "SFMovSensor.generated.h"


UENUM(BlueprintType)
enum class ESFReachSensorHitType : uint8
{
	ERSHIT_None = 0			UMETA(DisplayName = "ERSHIT_None"),
	ERSHIT_Climb = 1		UMETA(DisplayName = "ERSHIT_Climb"),
	ERSHIT_Pullup = 2		UMETA(DisplayName = "ERSHIT_Pullup"),
};

UENUM(BlueprintType)
enum class ESFLandSensorHitType : uint8
{
	ELSHIT_None = 0			UMETA(DisplayName = "ELSHIT_None"),
	ELSHIT_Surface = 1		UMETA(DisplayName = "ELSHIT_Surface"),
	ELSHIT_Ground = 2		UMETA(DisplayName = "ELSHIT_Ground"),
	ELSHIT_Water = 3		UMETA(DisplayName = "ELSHIT_Water"),
};

UENUM(BlueprintType)
enum class ESFJumpState : uint8
{
	EJmpState_None = 0			UMETA(DisplayName = "EJmpState_None"),
	EJmpState_Start = 1			UMETA(DisplayName = "EJmpState_Start"),
	EJmpState_Upward = 2		UMETA(DisplayName = "EJmpState_Upward"),
	EJmpState_Apex = 3			UMETA(DisplayName = "EJmpState_Apex"),
	EJmpState_Downward = 4		UMETA(DisplayName = "EJmpState_Downward"),
};


UENUM(BlueprintType)
enum class ESFMovState : uint8
{
	EMovState_None = 0			UMETA(DisplayName = "EMovState_None"),
	EMovState_Idle = 1			UMETA(DisplayName = "EMovState_Idle"),
	EMovState_Walk = 2			UMETA(DisplayName = "EMovState_Walk"),
	EMovState_Run = 3			UMETA(DisplayName = "EMovState_Run"),
	EMovState_Jump = 4			UMETA(DisplayName = "EMovState_Jump"),
	EMovState_Fall = 5			UMETA(DisplayName = "EMovState_Fall"),
	EMovState_FallGround = 6	UMETA(DisplayName = "EMovState_FallGround"),
	EMovState_FallWater = 7		UMETA(DisplayName = "EMovState_FallWater"),
	EMovState_Climb = 8			UMETA(DisplayName = "EMovState_Climb"),

	EMovState_JumpApex = 9		UMETA(DisplayName = "EMovState_JumpApex"),
};


USTRUCT(BlueprintType)
struct FSFSensorSource
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FSFSensor Source")
	FVector SensorStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FSFSensor Source")
	FVector SensorEnd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FSFSensor Source")
	FVector HitPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FSFSensor Source")
	bool IsHit;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FSFSensor Source")
	ESFReachSensorHitType ReachHitType;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FSFSensor Source")
	ESFLandSensorHitType  LandHitType;

	bool DoTrace(const TObjectPtr<ACharacter>& CharOwner, FVector& HitImpactPoint);
	ESFReachSensorHitType DoSphereTrace(const TObjectPtr<ACharacter>& CharOwner, float fRadius, FVector& HitImpactPoint);
	ESFLandSensorHitType DoDownSphereTrace(const TObjectPtr<ACharacter>& CharOwner, float fRadius, FVector& HitImpactPoint);
};


UCLASS(BlueprintType)
class SFCCHARACTERSYSTEM_API USFMovSensor : public UDataAsset // public UObject
{
	GENERATED_BODY()

public:
	USFMovSensor(const FObjectInitializer& ObjectInitializer);

	virtual void PostInitProperties() override;
	virtual void BeginDestroy() override;

	void OnSelfConnectToGOP(AActor* Owner);
	void OnSelfDisconnectFromGOP();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	FSFSensorSource	WallSensor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	FSFSensorSource FootSensor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	FSFSensorSource ReachSensor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	FSFSensorSource LandSensor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	TArray<FVector> PredictedLocations;

	// 점프 착지 예측 과련
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	FVector PredictedLandLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	FVector JumpStartLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	FVector JumpApexLocation;

	UPROPERTY(Transient)
	bool	bDirtyFlagForPredictedLand = false;

	// 점프 단발 처리용
	UPROPERTY(Transient)
	uint32	bJumpUpside : 1;
	uint32  bJumpInput : 1;

	UPROPERTY(Transient)
	uint32	bJumpDownside : 1;

	UPROPERTY(Transient)
	uint32	bFallDown : 1;

	//UPROPERTY(Transient)
	//bool	bJumpOn = false;


	//UPROPERTY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFMovSensor Asset", meta = (Units = "cm"))
	float	CapsuleHalfHeight = 88.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFMovSensor Asset", meta = (Units = "cm"))
	float	CapsuleRadius = 34.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFMovSensor Asset", meta = (Units = "cm"))
	float	WallTraceCapsuleRadius = 12.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFMovSensor Asset", meta = (Units = "cm"))
	float	PullupSensorHeight = 150.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFMovSensor Asset", meta=(Units="cm"))
	float	MinimumGlidingHeight = 500.f;

	float	StateChgLimitTime = 0.f;

	//UPROPERTY(Transient)
	//FName CollisionProfileName;

	//UPROPERTY(Transient, DuplicateTransient)
	//TObjectPtr<ACharacter> CharOwner;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFMovSensor Asset")
	bool UseDebugDraw;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	ESFMovState LastMovState = ESFMovState::EMovState_None;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	ESFJumpState JumpState = ESFJumpState::EJmpState_None;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	ESFReachSensorHitType LastReachHitType = ESFReachSensorHitType::ERSHIT_None;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	ESFLandSensorHitType  LastLandHitType = ESFLandSensorHitType::ELSHIT_None;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset")
	bool CanGliding = false;

	// A01 기능 이전 중 사용할 임시 변수
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset|A01 Temp")
	bool IsSwimCheck;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset|A01 Temp")
	bool IsClimbing;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFMovSensor Asset|A01 Temp")
	bool IsSwimming;


	UPROPERTY()
	TObjectPtr<AActor>	OwnActor;



	void CreatePredictionBuffer(int count);

	void SetPredictedLoc(int idx,const FVector vLoc);

	void DoJump(const TObjectPtr<ACharacter>& CharOwner);
	void UpatePredictedLand(const TObjectPtr<ACharacter>& CharOwner);
	void OnLand(const TObjectPtr<ACharacter>& CharOwner);


	// vActorLoc : 점프 시작 시점의 액터의 월드 포지션
	// vLoc : 점프 도착 예상 위치
	void SetPredictedLandLoc(const FVector& vActorLoc, const FVector& vLoc);

	FORCEINLINE void AdjustState(const FVector& Velocity);

	FORCEINLINE bool WallSensorTrace(const TObjectPtr<ACharacter>& CharOwner, const FVector& vOwnerLoc, const FVector& vOwnerFwd, float fScale, FVector& WallHitImpact);
	FORCEINLINE bool FootSensorTrace(const TObjectPtr<ACharacter>& CharOwner, const FVector& vOwnerLoc, const FVector& vOwnerFwd, float fScale, FVector& FootHitImpact);

	FORCEINLINE void AdjustWallState(const TObjectPtr<ACharacter>& CharOwner, bool WallHitRet, const FVector& WallHitImpact);
	FORCEINLINE void AdjustLandingState(const TObjectPtr<ACharacter>& CharOwner, const FVector& vOwnerLoc, bool FootHitRet, FVector& LandHitImpact);

	//UFUNCTION(BlueprintCallable)
	bool OnMovementTrace(const TObjectPtr<ACharacter>& CharOwner, 
		const FVector Accel, const FVector Velocity, const FVector LastInput, float fScale);
};



// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFMovSensorAdapter : public UInterface
{
	GENERATED_BODY()
};


class SFCCHARACTERSYSTEM_API ISFMovSensorAdapter
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SF MovSensorAdapter")
	bool GetMovSensor(USFMovSensor*& RetMovSensor);

	// 임시 이벤트
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SF MovSensorAdapter")
	void SetStateFlag(FName StateName, bool flag);

	// 임시 상태 확인용
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SF MovSensorAdapter")
	bool GetStateFlag(FName StateName, bool& StateFlag);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SF MovSensorAdapter")
	void DoStateCommand(FName StateCommand,const FString& StrArgs);
};
