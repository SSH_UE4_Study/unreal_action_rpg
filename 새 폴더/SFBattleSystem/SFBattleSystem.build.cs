﻿using UnrealBuildTool;
 
public class SFBattleSystem : ModuleRules
{
	public SFBattleSystem(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"
			, "GameplayTags"
			, "SFCTicketSystem"
			, "SFCCharacterSystem"});
 
		PublicIncludePaths.AddRange(new string[] {"SFBattleSystem/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFBattleSystem/Private"});
	}
}
