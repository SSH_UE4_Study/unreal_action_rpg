﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UObject/Interface.h"
#include "Components/ActorComponent.h"

#include "SFTicket.h"
#include "SFTicketExecutor.h"

#include "SFHitEffectProxy.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class USFHitEffectProxy : public UInterface
{
	GENERATED_BODY()
};


class SFBATTLESYSTEM_API ISFHitEffectProxy
{
	GENERATED_BODY()
		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxy")
	void SetHandle(int handle);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxy")
	int GetHandle();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxy")
	void SetAlive(bool bAlive);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxy")
	bool IsAlive();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxy")
	bool PerformStringCmd(FName Command, const FString& ArgString);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxy")
	void Update(float DeltaTime);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxy")
	void PreRemove();
};


//
// 최소 단위 SFHitEffectProxy 오브젝트
//
UCLASS(Blueprintable)
class SFBATTLESYSTEM_API USFHitEffectProxyObject 
	: public UObject
	, public ISFHitEffectProxy
	, public ISFTicketExecutor
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFC-004.08 SFHitEffectProxy Object")
	int _handle = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFC-004.08 SFHitEffectProxy Object")
	bool _isAlive = false;

public:
	
	virtual void SetHandle_Implementation(int handle) override;
	virtual int GetHandle_Implementation() override;
	virtual void SetAlive_Implementation(bool bAlive) override;
	virtual bool IsAlive_Implementation() override;
	virtual bool PerformStringCmd_Implementation(FName Command, const FString& ArgString) override;
	virtual void Update_Implementation(float DeltaTime) override;
	virtual void PreRemove_Implementation() override;

	virtual void ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket) override;
};


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SFBATTLESYSTEM_API USFHitEffectProxyComponent
	: public UActorComponent
	, public ISFHitEffectProxy
	, public ISFTicketExecutor
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFC-004.08 SFHitEffectProxy Component")
	int _handle = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SFC-004.08 SFHitEffectProxy Component")
	bool _isAlive = false;


public:
	USFHitEffectProxyComponent();

protected:
	virtual void BeginPlay() override;
	virtual void InitializeComponent() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	virtual void SetHandle_Implementation(int handle) override;
	virtual int GetHandle_Implementation() override;
	virtual void SetAlive_Implementation(bool bAlive) override;
	virtual bool IsAlive_Implementation() override;
	virtual bool PerformStringCmd_Implementation(FName Command, const FString& ArgString) override;
	virtual void Update_Implementation(float DeltaTime) override;
	virtual void PreRemove_Implementation() override;


	virtual void ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket) override;
};
