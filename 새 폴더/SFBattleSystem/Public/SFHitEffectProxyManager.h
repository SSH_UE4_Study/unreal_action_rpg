﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Components/ActorComponent.h"

#include "SFHitEffectProxy.h"

#include "SFHitEffectProxyManager.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class USFHitEffectProxyManager : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class SFBATTLESYSTEM_API ISFHitEffectProxyManager
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxyManager")
	void CreateManagedPool(int count);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxyManager")
	void ReleaseManagedPool();

	// 히트 이펙트 프록시 생성
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxyManager")
	bool GetNewHitEffectProxy(TScriptInterface<ISFHitEffectProxy>& iNewHitProxy);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "SFC-004.08 ISFHitEffectProxyManager")
	void ReleaseHitEffectProxy(const TScriptInterface<ISFHitEffectProxy>& iHitProxy);
};
