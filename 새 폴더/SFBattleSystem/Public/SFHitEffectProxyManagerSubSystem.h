﻿#pragma once

#include "CoreMinimal.h"
#include "Subsystems/LocalPlayerSubsystem.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"

#include "SFHitEffectProxyManager.h"
#include "SFTicketExecutor.h"
#include "Tickable.h"

#include "SFHitEffectProxyManagerSubSystem.generated.h"


#define SF_HITPROXY_SEGMENT		(100000)


UCLASS(DisplayName = "SFC-004.08 SFHitEffectProxyManagerSubSystem")
class USFHitEffectProxyManagerSubSubsystem
	: public ULocalPlayerSubsystem
	, public FTickableGameObject
	, public ISFHitEffectProxyManager
	, public ISFTicketExecutor
{
	GENERATED_BODY()

	uint64 LastFrameNumberWeTicked = (uint64)-1;

	int _assigend_handle = 0;

protected:

	UPROPERTY()
	FName ExecutorName;

	UPROPERTY()
	TSet<FName>	_processableTicketNames;

	// BP에서 티켓 처리를 위한 delegate
	UPROPERTY(BlueprintAssignable, Category = "SFC-004.08 SFHitEffectProxyManagerSubSystem")
	FDele_OnExecuteSFTicketMng Func_OnExecuteSFTicketMng;

	UPROPERTY()
	TArray<TObjectPtr<USFHitEffectProxyObject>> _managedHitProxyObjects;

	TQueue<int> _readyHitProxyObjects;

	UPROPERTY()
	TMap<int,TScriptInterface<ISFHitEffectProxy>> _activeHitProxyMap;

	void Update(float dt);

public:

	virtual void Tick(float DeltaTime) override;

	virtual ETickableTickType GetTickableTickType() const override
	{
		return ETickableTickType::Always;
	}

	virtual TStatId GetStatId() const override
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FSFBattleSystem, STATGROUP_Tickables);
	}

	virtual bool IsTickableWhenPaused() const override
	{
		return true;
	}

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	// 실행기 이름 설정, 가져오기
	virtual void SetExecutorName_Implementation(FName name) override;
	virtual void GetExecutorName_Implementation(FName& name) override;


	// 실행기에 연결할 명령어(태그) 리스트 등록
	virtual void RegisterTickets_Implementation(const TArray<FName>& events) override;

	// 실행기 명령어 연결 해제
	virtual void UnregisterTickets_Implementation(const TArray<FName>& events) override;

	// 등록된 태그 리스트
	virtual int GetRegisteredTickets_Implementation(TArray<FName>& registerEvents) override;

	// 티겟 실 처리 부분
	virtual void ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket) override;

	/// ISFHitEffectProxyManager 부분
	virtual void CreateManagedPool_Implementation(int count) override;
	virtual void ReleaseManagedPool_Implementation() override;
	virtual bool GetNewHitEffectProxy_Implementation(TScriptInterface<ISFHitEffectProxy>& iNewHitProxy) override;
	virtual void ReleaseHitEffectProxy_Implementation(const TScriptInterface<ISFHitEffectProxy>& iHitProxy) override;

	UFUNCTION(BlueprintCallable, Category = "SFC-004.08 SFHitEffectProxyManagerSubSystem")
	void PushExternalHitProxyInterface(const TScriptInterface<ISFHitEffectProxy>& iExtHitProxy,int& AssignedHandle);

	UFUNCTION(BlueprintCallable, Category = "SFC-004.08 SFHitEffectProxyManagerSubSystem")
	bool FindActiveHitProxyInterface(int AssignedHandle, TScriptInterface<ISFHitEffectProxy>& iRetHitProxy);
};
