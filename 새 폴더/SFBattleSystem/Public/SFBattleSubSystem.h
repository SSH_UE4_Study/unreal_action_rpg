﻿#pragma once

#include "CoreMinimal.h"
//#include "Subsystems/GameInstanceSubsystem.h"
#include "Subsystems/LocalPlayerSubsystem.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"

#include "SFEntity.h"
#include "SFEntityManager.h"

#include "SFTicketExecutor.h"

#include "SFHitEffectProxyManager.h"

#include "SFBattleSubSystem.generated.h"

UCLASS(DisplayName = "SFC-004 SFBattleSystem")
class SFBATTLESYSTEM_API USFBattleSubSubsystem
	: public ULocalPlayerSubsystem // public UGameInstanceSubsystem
	, public ISFTicketExecutor
{
	GENERATED_BODY()

protected:


	UPROPERTY()
	TObjectPtr<UObject> EngageSystem;

	UPROPERTY()
	FName ExecutorName;

	UPROPERTY()
	TSet<FName>	_processableTicketNames;

public:

	// Begin
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;


	// 실행기 이름 설정, 가져오기
	virtual void SetExecutorName_Implementation(FName name) override;
	virtual void GetExecutorName_Implementation(FName& name) override;


	// 실행기에 연결할 명령어(태그) 리스트 등록
	virtual void RegisterTickets_Implementation(const TArray<FName>& events) override;

	// 실행기 명령어 연결 해제
	virtual void UnregisterTickets_Implementation(const TArray<FName>& events) override;

	// 등록된 태그 리스트
	virtual int GetRegisteredTickets_Implementation(TArray<FName>& registerEvents) override;

	// 티겟 실 처리 부분
	virtual void ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket) override;


	UFUNCTION(BlueprintCallable, Category = "SFC-004 SFBattleSystem")
	bool GetHitBoxManagerInterface(TScriptInterface< ISFHitEffectProxyManager>& iHitBoxProxyManagerInterface);

};
