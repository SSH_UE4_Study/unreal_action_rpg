﻿#include "SFHitEffectProxyManagerSubSystem.h"
#include "SFBattleSystem.h"

#include "Subsystems/SubsystemCollection.h"

#include "SFTicket.h"

void USFHitEffectProxyManagerSubSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	SF_LOG(Warning, TEXT("SFC-004.08 SFHitEffectProxyManagerSubSystem Init"));

	// 할당용 핸들 시작값 초기화
	_assigend_handle = 0;	
}

void USFHitEffectProxyManagerSubSubsystem::Deinitialize()
{
	SF_LOG(Warning, TEXT("SFC-004.08 SFHitEffectProxyManagerSubSystem Deinit"));
}

void USFHitEffectProxyManagerSubSubsystem::SetExecutorName_Implementation(FName name)
{
	ExecutorName = name;
}

void USFHitEffectProxyManagerSubSubsystem::GetExecutorName_Implementation(FName& name)
{
	name = ExecutorName;
}

void USFHitEffectProxyManagerSubSubsystem::RegisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Emplace(_elem);
	}
}

void USFHitEffectProxyManagerSubSubsystem::UnregisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Remove(_elem);
	}
}

int USFHitEffectProxyManagerSubSubsystem::GetRegisteredTickets_Implementation(TArray<FName>& registerEvents)
{
	registerEvents = _processableTicketNames.Array();
	return registerEvents.Num();
}

void USFHitEffectProxyManagerSubSubsystem::ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket)
{
	ensureMsgf(iTicket, TEXT("Ticket is null or none"));

	// BP Event 호출하기
	//FSFTicketStruct _rawTicket;
	//ISFTicket::Execute_GetTicket(iTicket.GetObject(), _rawTicket);

	int uid = ISFTicket::Execute_GetOwnerUID(iTicket.GetObject());

	if(Func_OnExecuteSFTicketMng.IsBound())
		Func_OnExecuteSFTicketMng.Broadcast(iTicket, uid);

	//UE_LOG(LogTemp, Warning, TEXT("EntityMngSubsystem : GetTicket[%s]- OUID(%d)")
	//	   , *_rawTicket.TicketName.ToString()
	//	   , _rawTicket.OwnerUID);
	//if (Func_OnExecuteSFTicketForMng.IsBound())
	//	Func_OnExecuteSFTicketForMng.Broadcast(iTicket, _rawTicket.OwnerUID);

	// OwnerUID 를 HitProxyUID로 사용
	FSFTicketStruct _rawTicket;
	ISFTicket::Execute_GetTicket(iTicket.GetObject(), _rawTicket);

	if (_rawTicket.OwnerUID > 0)
	{
		auto* pIHitEffectProxy = _activeHitProxyMap.Find(_rawTicket.OwnerUID);

		if (pIHitEffectProxy && *pIHitEffectProxy
			&& pIHitEffectProxy->GetObject()->GetClass()->ImplementsInterface(USFTicketExecutor::StaticClass()))
		{
			ISFTicketExecutor::Execute_ExecuteTicket(pIHitEffectProxy->GetObject(), iTicket);
		}
	}
}


void USFHitEffectProxyManagerSubSubsystem::CreateManagedPool_Implementation(int count)
{
	// 초기화
	_assigend_handle = 0;
	_managedHitProxyObjects.Empty();
	_readyHitProxyObjects.Empty();

	_managedHitProxyObjects.AddZeroed(count);

	/// 0번은 무시
	for (int i = 1; i < count; ++i)
	{
		_managedHitProxyObjects[i] = NewObject<USFHitEffectProxyObject>();
		_readyHitProxyObjects.Enqueue(i);
	}
}

void USFHitEffectProxyManagerSubSubsystem::ReleaseManagedPool_Implementation()
{
	_readyHitProxyObjects.Empty();
	_managedHitProxyObjects.Empty();
}

bool USFHitEffectProxyManagerSubSubsystem::GetNewHitEffectProxy_Implementation(TScriptInterface<ISFHitEffectProxy>& iNewHitProxy)
{
	if( _readyHitProxyObjects.IsEmpty())
		return false;

	int Index = -1;

	if (_readyHitProxyObjects.Peek(Index))
	{
		auto* pRawObj = _managedHitProxyObjects[Index].Get();

		// 핸들 설정. Pool Index 를 핸들값으로 사용.
		// Note: SF_HITPROXY_SEGMENT 이상 핸들값은 관리되는 핸들
		int handle = Index + SF_HITPROXY_SEGMENT;

		SF_LOG(Warning, TEXT("Queue peek:(%d) - handle:(%d)"), Index, handle);

		ISFHitEffectProxy::Execute_SetHandle(pRawObj, handle);
		ISFHitEffectProxy::Execute_SetAlive(pRawObj, true);

		iNewHitProxy = TScriptInterface<ISFHitEffectProxy>(pRawObj);

		_readyHitProxyObjects.Pop();


		// 업데이트 맵에 등록
		_activeHitProxyMap.Add(handle, iNewHitProxy);

		return true;
	}
	else {
		SF_LOG(Error, TEXT("풀에 있는 포인터가 이상함"));
	}
	return false;
}

void USFHitEffectProxyManagerSubSubsystem::ReleaseHitEffectProxy_Implementation(const TScriptInterface<ISFHitEffectProxy>& iHitProxy)
{
	if (iHitProxy)
	{
		ISFHitEffectProxy::Execute_SetAlive(iHitProxy.GetObject(), false);
	}
}

void USFHitEffectProxyManagerSubSubsystem::PushExternalHitProxyInterface(const TScriptInterface<ISFHitEffectProxy>& iExtHitProxy, int& AssignedHandle)
{
	int _handle = (_assigend_handle + 1 >= SF_HITPROXY_SEGMENT ) ? 0 : ++_assigend_handle;

	// iExtHitProxy
	ISFHitEffectProxy::Execute_SetHandle(iExtHitProxy.GetObject() , _handle);	// 외부 ProxyObj는 handle 값을 -1로
	ISFHitEffectProxy::Execute_SetAlive(iExtHitProxy.GetObject(), true);

	_activeHitProxyMap.Add(_handle, iExtHitProxy);

	AssignedHandle = _handle;
}

bool USFHitEffectProxyManagerSubSubsystem::FindActiveHitProxyInterface(int AssignedHandle, TScriptInterface<ISFHitEffectProxy>& iRetHitProxy)
{
	auto* pValue = _activeHitProxyMap.Find(AssignedHandle);

	if (pValue && pValue->GetObject())
	{
		iRetHitProxy = pValue->GetObject();
		return true;
	}

	return false;
}

void USFHitEffectProxyManagerSubSubsystem::Update(float dt)
{
	
	int _top = 0;
	int* _removeBuff = (int*)FMemory_Alloca(sizeof(int)*(_activeHitProxyMap.Num() + 10 ) );

	// map 이용
	for (auto& pair : _activeHitProxyMap)
	{
		bool bAlive = ISFHitEffectProxy::Execute_IsAlive(pair.Value.GetObject());

		if (!bAlive) {
			_removeBuff[_top++] = pair.Key;
		}
		else
		{
			// 티켓 Update
			ISFHitEffectProxy::Execute_Update(pair.Value.GetObject(), dt);
		}
	}

	// map 이용
	while (_top)
	{
		int _removeKey = _removeBuff[--_top];

		auto* pValue = _activeHitProxyMap.Find(_removeKey);

		if (pValue && pValue->GetObject())
		{
			int _handle = ISFHitEffectProxy::Execute_GetHandle(pValue->GetObject());

			if( _handle > SF_HITPROXY_SEGMENT ) {
				SF_LOG(Warning, TEXT("Restore Managed HitProxyObj : (%d) - RawIdx(%d)")
					, _handle, (_handle- SF_HITPROXY_SEGMENT));
				_readyHitProxyObjects.Enqueue(_handle- SF_HITPROXY_SEGMENT);	// pool index 로 복귀
			}

			ISFHitEffectProxy::Execute_PreRemove(pValue->GetObject());

			_activeHitProxyMap.Remove(_removeKey);
		}
	}
}




void USFHitEffectProxyManagerSubSubsystem::Tick(float DeltaTime)
{
	if(GetWorld() && GetWorld()->IsGameWorld() )
	{ 
		if (LastFrameNumberWeTicked == GFrameCounter)
			return;
		LastFrameNumberWeTicked = GFrameCounter;

		//SF_LOG(Warning, TEXT("G Frame : %lld"), LastFrameNumberWeTicked);

		Update(DeltaTime);
	}
}

