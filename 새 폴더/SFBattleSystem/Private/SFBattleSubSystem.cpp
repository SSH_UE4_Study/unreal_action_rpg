﻿#include "SFBattleSubSystem.h"
#include "SFHitEffectProxyManager.h"
#include "SFHitEffectProxyManagerSubSystem.h"
#include "Subsystems/SubsystemCollection.h"


void USFBattleSubSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	UE_LOG(LogTemp, Warning, TEXT("SFC-004 SFBattleSystem Init"));
}

void USFBattleSubSubsystem::Deinitialize()
{
	UE_LOG(LogTemp, Warning, TEXT("SFC-004 SFBattleSystem Deinit"));
}

void USFBattleSubSubsystem::SetExecutorName_Implementation(FName name)
{
	ExecutorName = name;
}

void USFBattleSubSubsystem::GetExecutorName_Implementation(FName& name)
{
	name = ExecutorName;
}

void USFBattleSubSubsystem::RegisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Emplace(_elem);
	}
}

void USFBattleSubSubsystem::UnregisterTickets_Implementation(const TArray<FName>& events)
{
	for (auto& _elem : events)
	{
		_processableTicketNames.Remove(_elem);
	}
}

int USFBattleSubSubsystem::GetRegisteredTickets_Implementation(TArray<FName>& registerEvents)
{
	registerEvents = _processableTicketNames.Array();
	return registerEvents.Num();
}

void USFBattleSubSubsystem::ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket)
{
	ensureMsgf(iTicket, TEXT("Ticket is null or none"));

	// BP Event 호출하기
	FSFTicketStruct _rawTicket;
	ISFTicket::Execute_GetTicket(iTicket.GetObject(), _rawTicket);

	//UE_LOG(LogTemp, Warning, TEXT("EntityMngSubsystem : GetTicket[%s]- OUID(%d)")
	//	   , *_rawTicket.TicketName.ToString()
	//	   , _rawTicket.OwnerUID);
	//if (Func_OnExecuteSFTicketForMng.IsBound())
	//	Func_OnExecuteSFTicketForMng.Broadcast(iTicket, _rawTicket.OwnerUID);
}


bool USFBattleSubSubsystem::GetHitBoxManagerInterface(
	TScriptInterface< ISFHitEffectProxyManager>& iHitBoxProxyManagerInterface)
{
	ULocalPlayer* pLocalPlayer = GetLocalPlayer();
	USFHitEffectProxyManagerSubSubsystem* pIt 
		= pLocalPlayer->GetSubsystem<USFHitEffectProxyManagerSubSubsystem>();

	if (pIt)
	{
		iHitBoxProxyManagerInterface = TScriptInterface<ISFHitEffectProxyManager>(pIt);
		return true;
	}

	return false;
}
