﻿#include "SFBattleSystem.h"

DEFINE_LOG_CATEGORY(LogSFBattleSystem);

#define LOCTEXT_NAMESPACE "FSFBattleSystem"

void FSFBattleSystem::StartupModule()
{
	SF_LOG(Warning, TEXT("SFBattleSystem module has been loaded"));
}

void FSFBattleSystem::ShutdownModule()
{
	SF_LOG(Warning, TEXT("SFBattleSystem module has been unloaded"));
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFBattleSystem, SFBattleSystem)
