﻿#include "SFHitEffectProxy.h"

#include "Kismet/GameplayStatics.h"


void USFHitEffectProxyObject::SetHandle_Implementation(int handle)
{
	_handle = handle;
}

int USFHitEffectProxyObject::GetHandle_Implementation()
{
	return _handle;
}

void USFHitEffectProxyObject::SetAlive_Implementation(bool bAlive)
{
	_isAlive = bAlive;
}

bool USFHitEffectProxyObject::IsAlive_Implementation()
{
	return _isAlive;
}

bool USFHitEffectProxyObject::PerformStringCmd_Implementation(FName Command, const FString& ArgString)
{
	return false;
}

void USFHitEffectProxyObject::Update_Implementation(float DeltaTime)
{

}

void USFHitEffectProxyObject::ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket)
{

}

void USFHitEffectProxyObject::PreRemove_Implementation()
{

}



//
//
// Component
//
//
USFHitEffectProxyComponent::USFHitEffectProxyComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
}


void USFHitEffectProxyComponent::BeginPlay()
{
	Super::BeginPlay();
}

void USFHitEffectProxyComponent::InitializeComponent()
{
	Super::InitializeComponent();
}

void USFHitEffectProxyComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void USFHitEffectProxyComponent::SetHandle_Implementation(int handle)
{
	_handle = handle;
}

int USFHitEffectProxyComponent::GetHandle_Implementation()
{
	return _handle;
}

void USFHitEffectProxyComponent::SetAlive_Implementation(bool bAlive)
{
	_isAlive = bAlive;
}

bool USFHitEffectProxyComponent::IsAlive_Implementation()
{
	return _isAlive;
}


bool USFHitEffectProxyComponent::PerformStringCmd_Implementation(FName Command, const FString& ArgString)
{
	return false;
}

void USFHitEffectProxyComponent::Update_Implementation(float DeltaTime)
{

}


void USFHitEffectProxyComponent::ExecuteTicket_Implementation(const TScriptInterface<ISFTicket>& iTicket)
{

}

void USFHitEffectProxyComponent::PreRemove_Implementation()
{

}
