using UnrealBuildTool;
 
public class SFSpawnableSystem : ModuleRules
{
	public SFSpawnableSystem(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"});
 
		PublicIncludePaths.AddRange(new string[] {"SFSpawnableSystem/Public"});
		PrivateIncludePaths.AddRange(new string[] {"SFSpawnableSystem/Private"});
	}
}