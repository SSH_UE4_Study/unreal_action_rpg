#include "SFSpawnableSystem.h"

DEFINE_LOG_CATEGORY(SFSpawnableSystem);

#define LOCTEXT_NAMESPACE "FSFSpawnableSystem"

void FSFSpawnableSystem::StartupModule()
{
	UE_LOG(SFSpawnableSystem, Warning, TEXT("SFSpawnableSystem module has been loaded"));
}

void FSFSpawnableSystem::ShutdownModule()
{
	UE_LOG(SFSpawnableSystem, Warning, TEXT("SFSpawnableSystem module has been unloaded"));
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSFSpawnableSystem, SFSpawnableSystem)